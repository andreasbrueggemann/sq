"""
Theme config handler for the client's GUI.

The config is defined as follows:
File `theme_default.ini` in folder `config` contains the default theme config.
File `theme.ini` in the main directory can contain more specific theme config.
Each key that is not given in the specific theme in the main directory will instead be queried from
the default theme which acts as a fallback.

To query key `y` of category `x`, simply call `x.y()`, e.g., `WindowSize.launcher()`.
"""

from configparser import ConfigParser
from typing import Optional, Tuple
import math

FILE = "theme.ini"
"""Optional theme config file"""
FILE_DEFAULT = "config/theme_default.ini"
"""Default theme config file"""

def _parse_size(size_string: str) -> Tuple[int, int]:
    """Parses a size `([width], [height])` from a string `'[width]x[height]'`"""
    parts = size_string.split("x")
    assert len(parts) == 2
    return (int(parts[0]), int(parts[1]))

def size_to_string(width: int, height: int) -> str:
    """Returns a string `'[width]x[height]'` for the given width and height"""
    return f"{width}x{height}"

def _parse_rgb(rgb_string: str) -> Tuple[int, int, int]:
    """Parses an rgb value `([r], [g], [b])` (all values between 0 and 255) from a string
        `[r],[g],[b]`"""
    parts = rgb_string.split(",")
    assert len(parts) == 3
    return (int(parts[0]), int(parts[1]), int(parts[2]))

def _parse_rgb_to_hex(rgb_string: str) -> str:
    """Parses an rgb value `([r], [g], [b])` (all values between 0 and 255) from a string
        `[r],[g],[b]` and returns a hex string encoding `#......` of it"""
    parts = _parse_rgb(rgb_string)
    return f"#{parts[0]:02X}{parts[1]:02X}{parts[2]:02X}"

def apply_rgb_washout(rgb: Tuple[int, int, int], washout: float) -> Tuple[int, int, int]:
    """Applies given washout (0.0-1.0) to given rgb color `([r], [g], [b])` (0-255) by multiplying
    component-wise"""
    return (math.floor(washout * rgb[0]),
            math.floor(washout * rgb[1]),
            math.floor(washout * rgb[2]))

class Theme:
    """Theme configuration singleton"""

    __instance: Optional['Theme'] = None

    def __init__(self):
        """Constructs the configuration singleton"""

        self.__parser = ConfigParser()

        # later config overwrites previous ones' keys, thus default first
        self.__parser.read([FILE_DEFAULT, FILE])

        Theme.__instance = self

    @staticmethod
    def get_config() -> 'Theme':
        """Returns the theme configuration and constructs it if necessary"""
        if Theme.__instance is None:
            Theme()
        return Theme.__instance

    @staticmethod
    def get_parser() -> ConfigParser:
        """Returns the used parser"""
        return Theme.get_config().__parser # pylint: disable=protected-access

class WindowSize:
    """Theme configuration regarding window sizes"""

    @staticmethod
    def game() -> Tuple[int, int]:
        """
        Returns default width and height of the game window.

        Note that this setting may be overwritten by the AutoConfig when a player manually resizes
        the window.
        """
        return _parse_size(Theme.get_parser()["window_size"].get("game"))

    @staticmethod
    def launcher() -> Tuple[int, int]:
        """Returns default width and height of the launcher"""
        return _parse_size(Theme.get_parser()["window_size"].get("launcher"))

    @staticmethod
    def proactive_dl() -> Tuple[int, int]:
        """Returns default width and height of the proactive download manager"""
        return _parse_size(Theme.get_parser()["window_size"].get("proactive_dl"))

    @staticmethod
    def crash_reporter() -> Tuple[int, int]:
        """Returns the default width and height of the crash reporter"""
        return _parse_size(Theme.get_parser()["window_size"].get("crash_reporter"))

    @staticmethod
    def info() -> Tuple[int, int]:
        """Returns the default width and height of an info window"""
        return _parse_size(Theme.get_parser()["window_size"].get("info"))

    @staticmethod
    def selftest() -> Tuple[int, int]:
        """Returns the default width and height of the selftest"""
        return _parse_size(Theme.get_parser()["window_size"].get("selftest"))

class GameWindow:
    """Theme configuration regarding the game window"""

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the background color"""
        return _parse_rgb(Theme.get_parser()["game_window"].get("color"))

class Text:
    """Theme configuration regarding text formatting"""

    @staticmethod
    def line_break_spacing() -> int:
        """Returns the spacing between textlines"""
        return Theme.get_parser()["text"].getint("line_break_spacing")

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the text color"""
        return _parse_rgb(Theme.get_parser()["text"].get("color"))

    @staticmethod
    def highlight_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the highlighted text's color"""
        return _parse_rgb(Theme.get_parser()["text"].get("highlight_color"))

    @staticmethod
    def big_size() -> int:
        """Returns the font size for big text"""
        return Theme.get_parser()["text"].getint("big_size")

    @staticmethod
    def small_size() -> int:
        """Returns the font size for small text"""
        return Theme.get_parser()["text"].getint("small_size")

    @staticmethod
    def font_name() -> str:
        """Returns the name of the font to use for standard text"""
        return Theme.get_parser()["text"].get("font_name")

    @staticmethod
    def disconnect_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the disconnected flag's color"""
        return _parse_rgb(Theme.get_parser()["text"].get("disconnect_color"))

    @staticmethod
    def reconnect_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the reconnecting flag's color"""
        return _parse_rgb(Theme.get_parser()["text"].get("reconnect_color"))

    @staticmethod
    def continue_status_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the ontinue status flag's color"""
        return _parse_rgb(Theme.get_parser()["text"].get("continue_status_color"))

class Scoreboard:
    """Theme configuration regarding the scoreboard formatting"""

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the standard text color of the scoreboard"""
        return _parse_rgb(Theme.get_parser()["scoreboard"].get("color"))

    @staticmethod
    def washout() -> float:
        """Returns washout (0.0-1.0) that is multiplied to colors when used in scoreboard context"""
        return Theme.get_parser()["scoreboard"].getfloat("washout")

class Table:
    """Theme configuration regarding table widgets"""

    @staticmethod
    def column_spacing() -> int:
        """Returns spacing between columns"""
        return Theme.get_parser()["table"].getint("column_spacing")

class Button:
    """Theme configuration regarding button widgets"""

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the background color of a button"""
        return _parse_rgb(Theme.get_parser()["button"].get("color"))

    @staticmethod
    def color_pressed() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the background color of a button if it is pressed"""
        return _parse_rgb(Theme.get_parser()["button"].get("color_pressed"))

    @staticmethod
    def text_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the text color of a button"""
        return _parse_rgb(Theme.get_parser()["button"].get("text_color"))

    @staticmethod
    def font_name() -> str:
        """Returns the name of the font to use for the text inside a button"""
        return Theme.get_parser()["button"].get("font_name")

    @staticmethod
    def font_size() -> int:
        """Returns the font size to use for the text inside a button"""
        return Theme.get_parser()["button"].getint("font_size")

class Buzzer:
    """Theme configuration regarding buzzer widgets"""

    @staticmethod
    def background_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the background color of the buzzer"""
        return _parse_rgb(Theme.get_parser()["buzzer"].get("background_color"))

    @staticmethod
    def background_color_active() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the background color of the buzzer if active"""
        return _parse_rgb(Theme.get_parser()["buzzer"].get("background_color_active"))

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the color of the buzzer"""
        return _parse_rgb(Theme.get_parser()["buzzer"].get("color"))

    @staticmethod
    def color_pressed() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the color of the buzzer if being pressed"""
        return _parse_rgb(Theme.get_parser()["buzzer"].get("color_pressed"))

class Progress:
    """Theme configuration regarding progress bars"""

    @staticmethod
    def border_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the border color of a progress bar"""
        return _parse_rgb(Theme.get_parser()["progress"].get("border_color"))

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the color of a progress bar's bar itself"""
        return _parse_rgb(Theme.get_parser()["progress"].get("color"))

    @staticmethod
    def ahead_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the color of a progress bar's ahead progress bar"""
        return _parse_rgb(Theme.get_parser()["progress"].get("ahead_color"))

class Slider:
    """Theme configuration regarding slider widgets"""

    @staticmethod
    def background_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the background color of a slider"""
        return _parse_rgb(Theme.get_parser()["slider"].get("background_color"))

    @staticmethod
    def border_color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the border color of a slider"""
        return _parse_rgb(Theme.get_parser()["slider"].get("border_color"))

    @staticmethod
    def color() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the color of a slider's knob itself"""
        return _parse_rgb(Theme.get_parser()["slider"].get("color"))

    @staticmethod
    def color_pressed() -> Tuple[int, int, int]:
        """Returns RGB values (0-255) for the color of a slider's knob itself when being pressed"""
        return _parse_rgb(Theme.get_parser()["slider"].get("color_pressed"))

class Tk:
    """
    Theme configuration regarding GUIs using Tk/Tkinter.

    This could be redesigned to be more flexible by allowing more finegrained configuration.
    Feel free to extend if necessary.
    """

    @staticmethod
    def title_font_name() -> str:
        """Returns the font name for the title label"""
        return Theme.get_parser()["tk"].get("title_font_name")

    @staticmethod
    def title_font_size() -> int:
        """Returns the font size for the title label"""
        return Theme.get_parser()["tk"].getint("title_font_size")

    @staticmethod
    def title_font_color() -> str:
        """Returns hex color string for the color of the title label's font"""
        return _parse_rgb_to_hex(Theme.get_parser()["tk"].get("title_font_color"))

    @staticmethod
    def title_background_color() -> str:
        """Returns hex color string for the background color of the title label"""
        return _parse_rgb_to_hex(Theme.get_parser()["tk"].get("title_background_color"))

    @staticmethod
    def button_font_color() -> str:
        """Returns hex color string for the font color of a button"""
        return _parse_rgb_to_hex(Theme.get_parser()["tk"].get("button_font_color"))

    @staticmethod
    def button_background_color() -> str:
        """Returns hex color string for the background color of a button"""
        return _parse_rgb_to_hex(Theme.get_parser()["tk"].get("button_background_color"))

    @staticmethod
    def i_pad_x() -> int:
        """Returns the internal x-axis padding used by various UI elements"""
        return Theme.get_parser()["tk"].getint("i_pad_x")

    @staticmethod
    def i_pad_y() -> int:
        """Returns the internal y-axis padding used by various UI elements"""
        return Theme.get_parser()["tk"].getint("i_pad_y")

    @staticmethod
    def pad_x() -> int:
        """Returns the external x-axis padding used by various UI elements"""
        return Theme.get_parser()["tk"].getint("pad_x")

    @staticmethod
    def pad_y() -> int:
        """Returns the external y-axis padding used by various UI elements"""
        return Theme.get_parser()["tk"].getint("pad_y")
