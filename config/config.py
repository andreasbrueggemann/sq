"""
Config handler for server and client.

The config is defined as follows:
Files `server_default.ini` and `client_default.ini` in folder `config` contain the default config.
Files `server.ini` and `client.ini` in the main directory can contain more specific config.
Examples for these files are given in folder `config`.
Each key that is not given in the specific config in the main directory will instead be queried
from the default config which acts as a fallback.
Mind that some specific config has no fallback as custom values are required in those cases.

To programmatically query the config, you have to do the following:
Before querying anything, the config handler has to be initialized once by calling `Config([TYPE])`
where `[TYPE]` is to be replaced by the type of the application, namely `Type.SERVER` or
`Type.CLIENT` depending on the current context.
No reference to the initialized config must be held manually.
To query key `y` of category `x`, simply call `x.y()`, e.g., `Download.lookahead()`.
Note that config which is only intended to be used for either `Type.SERVER` or `Type.CLIENT` will
lead to an exception if it is queried in the opposite context.
"""

# pylint: disable=cyclic-import

from configparser import ConfigParser
from pathlib import Path
from typing import Callable, Optional, List, Any, Tuple
from enum import Enum
import sys
import os
import shutil

from misc import logger
from client_mod.errors import CustomMessageError

FILE_SERVER = "server.ini"
"""Optional config file for the server"""
FILE_SERVER_DEFAULT = "config/server_default.ini"
"""Default config file for the server"""
FILE_CLIENT = "client.ini"
"""Optional config file for the client"""
FILE_CLIENT_DEFAULT = "config/client_default.ini"
"""Default config file of the client"""

class Type(Enum):
    """Type of the running application defining if it is the game server or a client"""
    SERVER = 0
    CLIENT = 1

def _server_only(func: Callable[[], Any]):
    """Restricts the decorated config getter to server config only"""
    def inner(*args):
        if Config.get_config().get_type() != Type.SERVER:
            raise AttributeError("Server-only config accessed by client")
        return func(*args)
    return inner

def _client_only(func: Callable[[], Any]):
    """Restricts the decorated config getter to client config only"""
    def inner(*args):
        if Config.get_config().get_type() != Type.CLIENT:
            raise AttributeError("Client-only config accessed by server")
        return func(*args)
    return inner

def _alternative_paths(func: Callable[[], str]):
    """Changes the decorated config getter for paths to append `_[index]` in the end if `index` not
        zero where index is as given on Config initialization."""
    def inner(*args):
        output = func(*args)
        if Config.get_config().get_index() == 0:
            return output
        return f"{output}_{Config.get_config().get_index()}"
    return inner

class Config:
    """Configuration singleton that has to be initialized manually"""

    __instance: Optional['Config'] = None

    def __init__(self, type_: Type, index: int = 0):
        """
        Constructs the configuration singleton for either `Type.SERVER` or `Type.CLIENT` depending
        on the corresponding type of the running application.

        Argument `index` is for debugging purposes only.
        Setting it to `0` results in default behaviour while other values lead to usage of
        alternative files when running multiple clients from the same folder.
        For instance, for value `1`, folder `temp_1` will be used instead of folder `temp`
        and file `playlist.txt_1` will be used instead of file `playlist.txt` so that no
        collisions between both clients occur.
        """

        self.__type = type_
        self.__index = index
        self.__parser = ConfigParser()

        # later config overwrites previous ones' keys, thus default first
        if type_ == Type.SERVER:
            self.__parser.read([FILE_SERVER_DEFAULT, FILE_SERVER])
        else:
            self.__parser.read([FILE_CLIENT_DEFAULT, FILE_CLIENT])

        Config.__instance = self

    def get_type(self) -> Type:
        """Returns the type of the configuration, i.e., if it is for a server or a client"""
        return self.__type

    def get_index(self) -> int:
        """Returns the index for debugging purposes as documented in the constructor"""
        return self.__index

    @staticmethod
    def get_config() -> 'Config':
        """Returns the configuration or raises an Error if it was never constructed"""
        if Config.__instance is None:
            raise AttributeError("Config was accessed before being initialized")
        else:
            return Config.__instance

    @staticmethod
    def get_parser() -> ConfigParser:
        """Returns the used parser or raises an Error if the corresponding `Configuration` was never
            constructed"""
        return Config.get_config().__parser # pylint: disable=protected-access

    def test(self): # pylint: disable=too-many-branches
        """Tests the config and terminates the program if required configuration is missing or
            malformed (only some malformatting cases tested for), also tests if required files
            and directories exist"""

        def handle_error(error_message: str):
            """Handles an error depending on whether it is for client or server"""
            if self.get_type() == Type.SERVER:
                logger.error(error_message)
                sys.exit()
            else:
                raise CustomMessageError(error_message)

        files: List[Tuple[str, int, str]] = []
        optional_files: List[Tuple[str, int, str]] = []
        paths: List[Tuple[str, int, str]] = []

        if Logging.protocol_message_length_limit() <= 3:
            handle_error("Logging protocol_message_length_limit must be larger than 3")
        if Logging.target() not in ["terminal", "file", "both"]:
            handle_error(f"Invalid logging target {Logging.target()}")
        if Logging.level().lower() not in ["error", "warning", "connectionchange", "info", "debug"]:
            handle_error(f"Invalid loglevel {Logging.level()}")
        optional_files.append((Logging.path(), os.R_OK | os.W_OK, "log file path"))

        if self.get_type() == Type.SERVER:
            if not Path(FILE_SERVER).is_file():
                handle_error(f"Missing config file {FILE_SERVER}, please create one before running "
                            + "the server. Example configs are given and explained in "
                            + "config/server.ini or config/server_min.ini (if you are only "
                            + "interested in the most important configuration)")
            if Database.url() is None or Database.username() is None or Database.password() is None:
                handle_error("Missing database config")

        else:
            if not Path(FILE_CLIENT).is_file():
                handle_error(f"Missing config file {FILE_CLIENT}, please create one before running "
                            + "the client. Example configs are given and explained in "
                            + "config/client.ini or config/client_min.ini (if you are only "
                            + "interested in the most important configuration)")
            if Download.path_ffmpeg() is None:
                handle_error("Missing entry 'path_ffmpeg' in download config")
            if Download.quality() not in ["worst", "best"]:
                handle_error(f"Invalid download quality {Download.quality()}")
            optional_files.append((Files.player_id(), os.R_OK | os.W_OK, "player id file"))
            optional_files.append((Files.failed_downloads(), os.R_OK | os.W_OK,
                                    "failed downloads file"))
            optional_files.append((Files.playlist(), os.R_OK | os.W_OK, "playlist file"))
            optional_files.append((Files.lobby_music(), os.R_OK | os.W_OK, "lobby music file"))
            optional_files.append((Files.version(), os.R_OK | os.W_OK, "version file"))
            paths.append((Download.download_path(), os.R_OK | os.W_OK, "download path"))
            paths.append((Download.permacache_path(), os.R_OK | os.W_OK, "permacache path"))
            if Download.path_ffmpeg():
                if shutil.which("ffmpeg") is None:
                    handle_error("ffmpeg not found on your system")
            else:
                files.append((Files.ffmpeg(), os.X_OK, "ffmpeg executable"))
            if Debug.local_temp():
                paths.append((Debug.local_temp_path(), os.R_OK, "local temp path"))

        for path, permissions, name in files + paths:
            if not os.access(path, permissions):
                handle_error(f"Cannot access {name} {path} (does not exist or insufficient "
                                + "permissions)")

        for path, permissions, name in optional_files:
            parent = str(Path(path).parent.absolute())
            if not os.access(parent, permissions):
                handle_error(f"Cannot access {name} parent {parent} (does not exist or "
                                + "insufficient permissions)")
            if Path(path).is_file() and not os.access(path, permissions):
                handle_error(f"Cannot access {name} {path} (insufficient permissions)")

class Network:
    """Network configuration, contains information relevant to connect to a game server"""

    @staticmethod
    def ip_address() -> Optional[str]:
        """
        Returns the IP address (IPv4 or IPv6) of the game server.

        For the client, this acts as a default value and can be changed in the launcher.
        """
        return Config.get_parser()["network"].get("ip_address")

    @staticmethod
    def port() -> int:
        """
        Returns the port of the game server.

        For the client, this acts as a default value and can be changed in the launcher.
        """
        return Config.get_parser()["network"].getint("port")

    @staticmethod
    @_server_only
    def ping_upper_bound() -> int:
        """
        Returns the upper bound for client pings (ms).

        If a client's ping is higher than this value, the ping compensation uses the upper bound
        instead of the actual ping to the client's disadvantage so that ping compensation cannot
        grow arbitrary large.
        """
        return Config.get_parser()["network"].getint("ping_upper_bound")

    @staticmethod
    @_server_only
    def ping_count() -> int:
        """Returns the number of ping messages to be sent to determine a client's ping (median of
            all measurements)"""
        return Config.get_parser()["network"].getint("ping_count")

class Gameplay:
    """Gameplay configuration"""

    @staticmethod
    @_server_only
    def score_to_reach() -> int:
        """Returns the score that a player has to reach to end the game"""
        return Config.get_parser()["gameplay"].getint("score_to_reach")

    @staticmethod
    @_server_only
    def maximum_score_per_song() -> int:
        """Returns the the maximum points that can be assigned to each player for a song."""
        return Config.get_parser()["gameplay"].getint("maximum_score_per_song")

    @staticmethod
    @_server_only
    def songs_to_fetch() -> int:
        """Returns the number of songs to fetch which is the maximal number of songs per game"""
        return Config.get_parser()["gameplay"].getint("songs_to_fetch")

    @staticmethod
    @_server_only
    def random_start() -> bool:
        """
        Returns whether songs should be played from a random starting point.

        If set to `False`, all songs will play from the start or the provided time stamp in the URL
        (e.g. if the song starts later in a music video).
        If set to `True`, all songs will play from a random start between the original start or
        provided time stamp and `latest_start` times the way from the original start or provided
        time stamp to the end of the complete audio.
        """
        return Config.get_parser()["gameplay"].getboolean("random_start")

    @staticmethod
    @_server_only
    def latest_start() -> float:
        """
        Returns the latest starting point for a song where `0.0` corresponds to the start (w.r.t.
        the provided time stamp in the URL if available) and `1.0` corresponds to the end.

        Only used if `random_start` is set to `True`.
        """
        return Config.get_parser()["gameplay"].getfloat("latest_start")

    @staticmethod
    @_server_only
    def buzzer_slack() -> float:
        """
        Returns the buzzer slack.

        The buzzer slack is a time interval that begins with the first player buzzering.
        Instead of immediately pausing the song, it continues for the number of seconds defined by
        the slack.
        Then, a random player who buzzered initially or inside this slack is selected to solve.
        If buzzer_slack_compensation_mode is enabled, the random choice is made only among such
        players with the currently lowest score.
        Set the slack to `0.0` if you just want the fastest player to solve.
        """
        return Config.get_parser()["gameplay"].getfloat("buzzer_slack")

    @staticmethod
    @_server_only
    def buzzer_slack_compensation_mode() -> bool:
        """
        Returns whether buzzer-slack compensation mode is enabled.

        If this mode is enabled, the choice between multiple players who buzzered when using
        buzzer-slack is random among all such players who have the lowest score instead of randomly
        among all.
        """
        return Config.get_parser()["gameplay"].getboolean("buzzer_slack_compensation_mode")

    @staticmethod
    @_server_only
    def listening_time_limit() -> Optional[float]:
        """
        Returns the time limit that a song is playing before it is skipped if nobody buzzers or
        `None` if no time limit is set.
        """
        time_limit = Config.get_parser()["gameplay"].getfloat("listening_time_limit")
        assert time_limit >= 0
        return None if time_limit == 0.0 else time_limit

    @staticmethod
    @_server_only
    def listening_time_limit_skip_threshold() -> float:
        """
        Returns the percentage of players need to skip in order to activate the listening time
        limit.

        This can be used to, e.g., enforce a time limit only if at least half of all players wish
        to skip or, e.g., 90% of the players.
        
        Special case: 1.0 represents that the time limit activates if all but one player wants to
        continue

        Only active if listening_time_limit is active, i.e., > 0.0.
        """
        return Config.get_parser()["gameplay"].getfloat("listening_time_limit_skip_threshold")

    @staticmethod
    @_server_only
    def guessing_time_limit() -> Optional[float]:
        """
        Returns the time limit for guessing a song after buzzering or `None` if no time limit is
        set.

        Depending on guessing_time_limit_hard, this either is a soft limit just displayed to the
        players, or a hard limit that automatically proceeds to solving after the time exceeded.
        """
        time_limit = Config.get_parser()["gameplay"].getfloat("guessing_time_limit")
        assert time_limit >= 0
        return None if time_limit == 0.0 else time_limit

    @staticmethod
    @_server_only
    def guessing_time_limit_hard() -> bool:
        """
        Returns whether the guessing_time_limit is a hard limit, i.e., causes the game to
        automatically proceed to solving after it has exceeded.

        Otherwise, the time limit is a soft limit meaning that (no) remaining time is shown to the
        players but no automatic action is taken should it be exceeded.

        Only active if guessing_time_limit is active, i.e., > 0.0.
        """
        return Config.get_parser()["gameplay"].getboolean("guessing_time_limit_hard")

    @staticmethod
    @_server_only
    def no_buzzers() -> bool:
        """
        Returns whether the 'No Buzzers' mode is enabled.

        In this mode, all songs play until everyone continues or the listening_time_limit is reached
        and solutions are only revealed after all songs in the end.
        """
        return Config.get_parser()["gameplay"].getboolean("no_buzzers")

    @staticmethod
    @_client_only
    def username() -> Optional[str]:
        """Returns the default name of the player if defined"""
        return Config.get_parser()["gameplay"].get("username")

    @staticmethod
    @_client_only
    def overwrite_lobby_music() -> bool:
        """Returns whether the lobby music shall be overwritten by the first song of each game"""
        return Config.get_parser()["gameplay"].getboolean("overwrite_lobby_music")

    @staticmethod
    @_client_only
    def allow_playlist_duplicates() -> bool:
        """Returns whether duplicate entries can be added to the playlist"""
        return Config.get_parser()["gameplay"].getboolean("allow_playlist_duplicates")

class GUI:
    """GUI configuration"""

    @staticmethod
    @_client_only
    def show_fps() -> bool:
        """Returns whether FPS should be displayed"""
        return Config.get_parser()["gui"].getboolean("show_fps")

    @staticmethod
    @_client_only
    def show_download_stats() -> bool:
        """Returns whether download statistics should be displayed"""
        return Config.get_parser()["gui"].getboolean("show_download_stats")

    @staticmethod
    @_client_only
    def volume_translation_order() -> float:
        """
        Returns the order of the volume translation.

        This is an experimental configuration.
        For order k, moving the volume bar to position x sets the volume to x^k.
        As x is in [0,1], the result also is in [0,1] for k>0 but the volume increases slower in for
        lower positions of the volume slider for higher k.
        """
        return Config.get_parser()["gui"].getfloat("volume_translation_order")

    @staticmethod
    @_client_only
    def allow_all_buttons() -> bool:
        """
        Returns whether all button actions can be used to click buttons and similar.

        Setting this to `True` basically reverts the fix of #13 as some users claim that using the
        scroll wheel or similar to buzzer is faster than clicking.
        """
        return Config.get_parser()["gui"].getboolean("allow_all_buttons")

class Download:
    """Configuration for the download and post-processing of audio files"""

    @staticmethod
    @_server_only
    def lookahead() -> int:
        """Returns the song download lookahead which is the number of songs that have to be
            downloaded before the quiz leaves initial downloading"""
        return Config.get_parser()["download"].getint("lookahead")

    @staticmethod
    @_client_only
    def path_ffmpeg() -> bool:
        """Returns whether FFMPEG shall be identified by PATH, otherwise local binary is used"""
        return Config.get_parser()["download"].getboolean("path_ffmpeg")

    @staticmethod
    @_client_only
    def normalize_audio_volume() -> bool:
        """Returns whether all downloaded audio shall be normalized to -0.0dB peak audio volume"""
        return Config.get_parser()["download"].getboolean("normalize_audio_volume")

    @staticmethod
    @_client_only
    @_alternative_paths
    def download_path() -> str:
        """Returns the path that songs are downloaded to during a match"""
        return Config.get_parser()["download"].get("download_path")

    @staticmethod
    @_client_only
    def permacache_downloads() -> bool:
        """
        Returns whether the audio files downloaded during running games should be permanently cached
        for future games.

        If set to `True`, all downloaded files will be copied to the path specified by
        `permacache_path`.
        Note that even if set to `False`, already cached files from previous games where it was set
        to `True` or cached files of proactive downloads that ignore `permacache_downloads` will be
        used.
        """
        return Config.get_parser()["download"].getboolean("permacache_downloads")

    @staticmethod
    @_client_only
    def permacache_path() -> str:
        """Returns the path where audio files shall be cached to if `permacache_downloads` is set to
            `True` of proactive downloads are being used"""
        return Config.get_parser()["download"].get("permacache_path")

    @staticmethod
    @_client_only
    def attempts_per_song() -> int:
        """Returns the number of attempts for downloading a single song using yt_dlp until
            the song is skipped"""
        return Config.get_parser()["download"].getint("attempts_per_song")

    @staticmethod
    @_client_only
    def quality() -> str:
        """Returns the preferred audio download quality which can be `'best'` or `'worst'`"""
        return Config.get_parser()["download"].get("quality")

    @staticmethod
    @_client_only
    def selftest_song() -> str:
        """Returns the URL used for the selftest song"""
        return Config.get_parser()["download"].get("selftest_song")

class Database:
    """
    Configuration regarding the database that songs are fetched from.

    This configuration defines how a server or optionally a client can connect and authenticate to
    the database to retrieve songs.
    The given username and password are used to access the database defined by `url`.
    If `post` is set to `True`, login parameters are transmitted as POST parameters, otherwise as
    standard HTTP authentication.
    """

    @staticmethod
    def url() -> Optional[str]:
        """
        Returns the URL of the database to fetch songs from.

        The used protocol should be https such that transmitted login data is encrypted.
        Optional for the client.
        """
        return Config.get_parser()["database"].get("url")

    @staticmethod
    def post() -> bool:
        """Returns whether login parameters shall be transmitted as POST parameters, otherwise HTTP
            auth is used"""
        return Config.get_parser()["database"].getboolean("post")

    @staticmethod
    def username() -> Optional[str]:
        """Returns the database username; optional for client"""
        return Config.get_parser()["database"].get("username")

    @staticmethod
    def password() -> Optional[str]:
        """Returns the database password; optional for client"""
        return Config.get_parser()["database"].get("password")

    @staticmethod
    def enabled_extractors() -> Optional[List[str]]:
        """Returns the names of the song extractors that may be used or `None` if all are
            allowed."""
        value = Config.get_parser()["database"].get("enabled_extractors")
        if value == "all":
            return None
        return value.split(",")

class Files:
    """File name configuration defining the paths of used input/output files"""

    @staticmethod
    @_client_only
    @_alternative_paths
    def player_id() -> str:
        """Returns the file where the last used player id is written to and read from"""
        return Config.get_parser()["files"].get("player_id")

    @staticmethod
    @_client_only
    def version() -> str:
        """Returns the file where the last used version is written to"""
        return Config.get_parser()["files"].get("version")

    @staticmethod
    @_client_only
    def ffmpeg() -> str:
        """Returns the location of the ffmpeg binary for case path_ffmpeg = False"""
        return Config.get_parser()["files"].get("ffmpeg")

    @staticmethod
    @_client_only
    def lobby_music() -> str:
        """Returns the file where the lobby music is saved; when loading it from pygame, use
            `downloader.TEMP_LOBBY_MUSIC_FILE` for the download folder instead"""
        return Config.get_parser()["files"].get("lobby_music")

    @staticmethod
    @_client_only
    @_alternative_paths
    def failed_downloads() -> str:
        """Returns "the file where failed downloads are logged to"""
        return Config.get_parser()["files"].get("failed_downloads")

    @staticmethod
    @_client_only
    @_alternative_paths
    def playlist() -> str:
        """Returns "the file where the playlist is written to"""
        return Config.get_parser()["files"].get("playlist")

class Logging:
    """Logging configuration defining which events shall be logged to the console and logfile
        handling"""

    @staticmethod
    def level() -> str:
        """
        Returns the loglevel which is the minimal loglevel of messages that shall be logged.

        Possible values:
        * `"error"`
        * `"warning"`
        * `"connectionchange"`
        * `"info"`
        * `"debug"`
        """
        return Config.get_parser()["logging"].get("level")

    @staticmethod
    def target() -> str:
        """
        Returns the target of logging messages.

        Possible values:
        * `"terminal"` - To only print all logging messages to the terminal
        * `"file"` - To only print all logging messages to a logging file
        * `"both"` - To print logging messages to terminal and logging file
        """
        return Config.get_parser()["logging"].get("target")

    @staticmethod
    def coloring() -> bool:
        """
        Returns whether colors should be used for log messages printed into the terminal.

        Corresponding config can be left empty, then coloring will be used on linux and not used
        on windows as windows seems not to have ansi coloring enabled by default.
        """
        value = Config.get_parser()["logging"].getboolean("coloring")
        if value is not None:
            return value
        else:
            if os.name == "nt":
                return False
            else:
                return True

    @staticmethod
    @_alternative_paths
    def path() -> str:
        """Returns the path of the file where logs are written to"""
        return Config.get_parser()["logging"].get("path")

    @staticmethod
    def max_file_size() -> int:
        """Returns the maximal size of a logfile in bytes"""
        return Config.get_parser()["logging"].getint("max_file_size")

    @staticmethod
    def backups() -> int:
        """
        Returns the maximal number of backup logfiles.

        If this value is e.g. 3, the logfile, e.g. sq_server.log, when reaching its size limit will
        be renamed to sq_server.log.1 and a new sq_server.log is opened. Further backups are renamed
        by changing the trailing index up to sq_server.log.3 which will be overwritten.
        Thus, you can have files sq_server.log, sq_server.log.1, sq_server.log.2 and sq_server.log.3
        """
        return Config.get_parser()["logging"].getint("backups")

    @staticmethod
    def tick_length_warning_threshold() -> float:
        """Returns the time that, if ticks were skipped, a tick has to take to a warning being
            logged (in seconds)."""
        return Config.get_parser()["logging"].getfloat("tick_length_warning_threshold")

    @staticmethod
    def ticks_skipped() -> bool:
        """Returns whether all cases where ticks were skipped should be logged on debuglevel"""
        return Config.get_parser()["logging"].getboolean("ticks_skipped")

    @staticmethod
    def incoming() -> bool:
        """Returns whether incoming messages should be logged on debuglevel"""
        return Config.get_parser()["logging"].getboolean("incoming")

    @staticmethod
    def outgoing() -> bool:
        """Returns whether outgoing messages should be logged on debuglevel"""
        return Config.get_parser()["logging"].getboolean("outgoing")

    @staticmethod
    def protocol_message_length_limit() -> int:
        """Returns the maximum length that protocol messages are truncated to when being logged"""
        return Config.get_parser()["logging"].getint("protocol_message_length_limit")

    @staticmethod
    def socket_events() -> bool:
        """Returns whether socket events (data to read available or socket writable) should be
            logged on debuglevel"""
        return Config.get_parser()["logging"].getboolean("socket_events")

class Timeouts:
    """Configuration regarding timeouts"""

    @staticmethod
    def base_tickrate() -> int:
        """
        Returns the base tickrate of the server or client.

        The base tickrate is used to determine selector timeouts such that at least, the given
        tickrate is reached.
        It only acts as a base as new messages immediately lead to a new tick being executed.

        For the client, the base tickrate corresponds to the base FPS as rendering and networking is
        executed in the same loop.
        """
        return Config.get_parser()["timeouts"].getint("base_tickrate")

    @staticmethod
    @_server_only
    def search_length_to_pause() -> float:
        """Returns the number of seconds that searching for the next song to play can take until a
            pause is triggered afterwards"""
        return Config.get_parser()["timeouts"].getfloat("search_length_to_pause")

    @staticmethod
    @_server_only
    def end_timeout() -> float:
        """Returns the number of seconds that have to pass when the game ended until the lobby may
            be re-entered"""
        return Config.get_parser()["timeouts"].getfloat("end_timeout")

    @staticmethod
    @_server_only
    def keepalive_interval() -> float:
        """
        Returns the number of seconds between each keepalive message being sent from server to all
        clients.

        Too low values increase traffic and load while too high values may not prevent timeouts.
        """
        return Config.get_parser()["timeouts"].getfloat("keepalive_interval")

    @staticmethod
    @_client_only
    def progress_sync_interval() -> float:
        """
        Returns the number of seconds between the messages where a client notifies the server
        of its own downloading progress.

        Lower values lead to a smoother moving progress bar while increasing server load.
        """
        return Config.get_parser()["timeouts"].getfloat("progress_sync_interval")

class Debug:
    """Debug configuration for testing purposes"""

    @staticmethod
    @_client_only
    def local_temp() -> bool:
        """
        Returns whether downloading shall just be emulated by copying files from a folder specified
        by `local_temp_path()`.

        Note that with this option being set to True song meta data still is being used from the
        current list instead of local files.
        Only use this for testing purposes.
        """
        return Config.get_parser()["debug"].getboolean("local_temp")

    @staticmethod
    @_client_only
    def local_temp_path() -> str:
        """Returns the folder used as song source if local_temp is being used"""
        return Config.get_parser()["debug"].get("local_temp_path")

    @staticmethod
    @_client_only
    def local_temp_download_delay() -> float:
        """Returns the simulated download delay for each song if local_temp is being used"""
        return Config.get_parser()["debug"].getfloat("local_temp_download_delay")
