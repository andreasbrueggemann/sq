"""
Handler for client's automated config which contains parameters that are set automatically by
the application and act as a persistent state over multiple games.

The auto config is saved in a file `auto_config.ini`.
"""

from configparser import ConfigParser
from typing import Optional

from config import theme

FILE = "auto_config.acfg"
"""Optional auto config file (does not exist before being written by the game)"""

class AutoConfig:
    """Auto configuration singleton"""

    __instance: Optional['AutoConfig'] = None

    def __init__(self):
        """Constructs the auto configuration singleton"""

        self.__parser = ConfigParser()
        self.__parser.read(FILE)
        if "auto_values" not in self.__parser:
            self.__parser["auto_values"] = {}

        AutoConfig.__instance = self

    @staticmethod
    def get_config() -> 'AutoConfig':
        """Returns the auto configuration and constructs it if necessary"""
        if AutoConfig.__instance is None:
            AutoConfig()
        return AutoConfig.__instance

    @staticmethod
    def get_parser() -> ConfigParser:
        """Returns the used parser"""
        return AutoConfig.get_config().__parser # pylint: disable=protected-access

def game_window_width() -> int:
    """
    Returns the width of the game window.

    If it was not previously written to the auto configuration, the one given by the theme is used.
    """
    return AutoConfig.get_parser()["auto_values"] \
                        .getint("game_window_width", theme.WindowSize.game()[0])

def game_window_height() -> int:
    """
    Returns the height of the game window.

    If it was not previously written to the auto configuration, the one given by the theme is used.
    """
    return AutoConfig.get_parser()["auto_values"] \
                        .getint("game_window_height", theme.WindowSize.game()[1])

def volume() -> float:
    """
    Returns the audio volume.

    If it was not previously written to the auto configuration, `0.5` is returned as default value.
    This is due to it not really making sense to also add a default value to the standard config
    as it can be changed immediately anyways.
    """
    return AutoConfig.get_parser()["auto_values"].getfloat("volume", 0.5)

def write_current_state(window_width: int, window_height: int, volume_: float):
    """
    Writes the given window width and height and volume to the auto config including its file.

    This should be called when terminating the program to save the current state.
    """

    parser = AutoConfig.get_parser()["auto_values"]
    parser["game_window_width"] = str(window_width)
    parser["game_window_height"] = str(window_height)
    parser["volume"] = str(volume_)
    with open(FILE, "w", encoding="utf-8") as configfile:
        AutoConfig.get_parser().write(configfile)
