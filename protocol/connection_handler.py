import selectors
import socket
import struct
import json
from typing import Any, Optional, Tuple, List

from misc import logger

BUFFER_SIZE = 2048
"""Maximum amount of data to be received at once"""
MSG_LEN_BYTE_LEN = 4 # 32 bit integer
"""Number of bytes used to encode payload length, limits maximum payload size"""
MSG_LEN_FORMAT = ">I" # Big endian unsigned integer
"""Format of the encoding of payload length"""
ENCODING = "utf-8"
"""Used message encoding"""

class ConnectionHandler:
    """
    Handler of a connection between client and server.

    This can be used both client- and server-side.
    It basically is a wrapper of a socket and corresponding selector and has to be set as `data` of
    the selector.
    """

    def __init__(self, selector: selectors.DefaultSelector, socket_: socket.socket,
                    address: Tuple[str, int]):
        """
        Constructs a new instance according to the given parameters.

        Parameters include the corresponding selector, used socket and connected address containing
        IP address and port.
        """

        self.__selector = selector
        self.__socket = socket_
        self.__address = address

        self.__linked_data: Optional[Any] = None
        """Optionally linked data to access from the connection handler"""

        self.__receive_buffer: bytes = b""
        """Buffer for bytes read from the socket"""
        self.__send_buffer: bytes = b""
        """Buffer for bytes to be written to the socket"""
        self.__message_length: Optional[int] = None
        """Length of the next expected message payload in bytes or `None` if length of next payload
            remains to be read"""

    def set_linked_data(self, linked_data: Any):
        """Sets the optional linked data to access from the connection handler"""
        self.__linked_data = linked_data

    def get_linked_data(self) -> Optional[Any]:
        """Returns the optional linked data to access from the connection handler or `None` if none
            set"""
        return self.__linked_data

    def get_address(self) -> Tuple[str, int]:
        """Returns the address consisting of IP and port"""
        return self.__address

    def get_incoming_messages(self) -> List[dict]:
        """
        Returns list of all complete incoming messages.

        This is to be called when `EVENT_READ` is given for the connection.
        """

        logger.event(*self.__address, "Read")
        self.__read_from_socket()

        # Construct list of all complete messages as no further `EVENT_READ` will be fired for them
        messages = []
        while True:
            if self.__message_length is None: # Read payload length
                if len(self.__receive_buffer) >= MSG_LEN_BYTE_LEN:
                    self.__message_length = struct.unpack(MSG_LEN_FORMAT,
                                                self.__pop_from_receive_buffer(MSG_LEN_BYTE_LEN))[0]

            if self.__message_length is not None and \
                    len(self.__receive_buffer) >= self.__message_length: # Read payload
                encoded_data = self.__pop_from_receive_buffer(self.__message_length)
                message = ConnectionHandler.__json_decode(encoded_data)
                logger.incoming(*self.__address, message, self.__message_length)
                self.__message_length = None
                messages.append(message)

            else: # No complete message left to read
                break

        return messages

    def add_outgoing_message(self, message: dict):
        """
        Registers a message to send.

        Note that the added message can only be sent by calling `write()` when the socket can send.
        Selector `EVENT_WRITE` is automatically enabled by calling this method.
        """

        payload = ConnectionHandler.__json_encode(message)
        length = len(payload)
        header = struct.pack(MSG_LEN_FORMAT, length)
        packet = header + payload
        self.__send_buffer += packet
        logger.outgoing(*self.__address, message, length)
        self.__selector.modify(
            self.__socket, selectors.EVENT_READ | selectors.EVENT_WRITE, data = self)

    def write(self):
        """
        Writes previously added outgoing messages to the socket.

        Call this if messages are to be sent and the socket can send.
        Selector `EVENT_WRITE` is automatically disabled if send buffer runs empty
        """
        logger.event(*self.__address, "Write")
        self.__write_to_socket()

    def close(self):
        """Closes the connection"""
        logger.disconnect(*self.__address)
        try:
            self.__selector.unregister(self.__socket)
        except KeyError as error:
            logger.error(f"Unable to unregister selector for {self.__address}: {repr(error)}")
        try:
            self.__socket.close()
        except OSError as error:
            logger.error(f"Unable to close socket for {self.__address}: {repr(error)}")
        finally:
            self.__socket = None

    def __pop_from_receive_buffer(self, n_bytes: int) -> bytes:
        """Pops the first `n_bytes` bytes from the receive buffer and returns them"""
        assert len(self.__receive_buffer) >= n_bytes
        output = self.__receive_buffer[:n_bytes]
        self.__receive_buffer = self.__receive_buffer[n_bytes:]
        return output

    def __pop_from_send_buffer(self, n_bytes: int) -> bytes:
        """Pops the first `n_bytes` bytes from the send buffer and returns them"""
        assert len(self.__send_buffer) >= n_bytes
        output = self.__send_buffer[:n_bytes]
        self.__send_buffer = self.__send_buffer[n_bytes:]
        return output

    def __read_from_socket(self):
        """Reads data from the socket transferring it to the receive buffer"""
        try:
            data = self.__socket.recv(BUFFER_SIZE)
        except BlockingIOError:
            pass
        else:
            if data:
                self.__receive_buffer += data
            else:
                raise ConnectionError("Peer closed")

    def __write_to_socket(self):
        """
        Writes data from the send buffer to the socket and removes it from the send buffer.

        Selector `EVENT_WRITE` is automatically disabled if send buffer runs empty.
        """

        if len(self.__send_buffer) > 0:
            try:
                n_bytes = self.__socket.send(self.__send_buffer)
            except BlockingIOError:
                pass
            else:
                self.__pop_from_send_buffer(n_bytes)

        if len(self.__send_buffer) == 0: # Nothing remains to be written
            self.__selector.modify(self.__socket, selectors.EVENT_READ, data = self)

    @staticmethod
    def __json_decode(encoded_json: bytes) -> dict:
        """Decodes the given JSON encoding of an object and returns the object"""
        return json.loads(encoded_json.decode(ENCODING))

    @staticmethod
    def __json_encode(object_: dict) -> bytes:
        """Encodes the given object as a JSON encoding"""
        return json.dumps(object_, ensure_ascii = False).encode(ENCODING)
