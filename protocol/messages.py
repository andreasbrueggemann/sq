"""
Collection of all used game network protocol messages.

Each message is a dict that contains an entry `"command"` identifying the specific message
and optionally additional data depending on the specific command.

For each message, `C_MESSAGENAME` contains the corresponding command content.
Function `MESSAGENAME(...)` returns a message according to the given arguments.
"""

# Use UPPER_CASE style for message generation functions too against convenction to highlight them
# pylint: disable=invalid-name

from typing import Dict, List, Optional

from misc.player_state import PlayerState
from misc.song import SongStatus

##########################
### General networking ###
##########################

C_JOIN_GAME = "join_game"
"""Message command for indicating that a player wants to join a game"""
def JOIN_GAME(username: str, protocol_version: int, player_id: int) -> dict:
    """
    Message indicating that a player wants to join a game.

    ### Client -> Server

    If the player wishes to connect normally, they give their desired username and used
    communication protocol version.
    In this case, `player_id` has to be `0` and another id will be assigned to the player later.

    If the player wishes to reconnect to a running game, `player_id` has to be set
    to the previously assigned id.
    In this case, `username` will be ignored.
    """

    return {
        "command": C_JOIN_GAME,
        "name": username,
        "protocol_version": protocol_version,
        "id": player_id
    }

C_REFUSE_CONNECTION = "refuse_connection"
"""Message command for indicating that the server is refusing a client connection to the game"""
REFUSE_CONNECTION_REASON_PROTOCOL_VERSION = "protocol_version"
"""Refuse reason for when client and server protocol versions are not compatible"""
def REFUSE_CONNECTION(reason: str, content: int) -> dict:
    """
    Message indicating that the server is refusing a client connection to the game
    due to incompatibility.

    ### Server -> Client

    ## Possible reasons:
    * `REFUSE_CONNECTION_REASON_PROTOCOL_VERSION` if the protocol versions are not compatible.
    In this case, `content` equals the protocol version that the server uses.
    """

    return {
        "command": C_REFUSE_CONNECTION,
        "reason": reason,
        "content": content
    }

C_SET_PLAYER_ID = "set_player_id"
"""Message command for setting the id of a player"""
def SET_PLAYER_ID(player_id: int) -> dict:
    """
    Message for setting the id of a player to the given value.

    ### Server -> Client
    """

    return {
        "command": C_SET_PLAYER_ID,
        "content": player_id
    }

C_PROVIDE_CONFIG = "provide_config"
"""Message command for sending server-side config to the client"""
def PROVIDE_CONFIG(lookahead: int, no_buzzers: bool, maximum_score_per_song: int) -> dict:
    """
    Message transmitting the relevant server-side config to the client.

    ### Server -> Client

    ## Transmitted config
    * Download lookahead
    * 'no buzzers' mode
    * Maximum score that can be assigned to a player for one song
    """

    return {
        "command": C_PROVIDE_CONFIG,
        "lookahead": lookahead,
        "no_buzzers": no_buzzers,
        "maximum_score_per_song": maximum_score_per_song
    }

C_PING = "ping"
"""Message command for initial message for a ping"""
def PING() -> dict:
    """
    Initial message for a ping.

    ### Server -> Client
    """

    return {
        "command": C_PING
    }

C_PING_REPLY = "ping_reply"
"""Message command for ping response messages"""
def PING_REPLY() -> dict:
    """
    Ping response message.

    ### Client -> Server
    """

    return {
        "command": C_PING_REPLY
    }

C_KEEPALIVE = "keepalive"
"""Message command for keepalive messages"""
def KEEPALIVE() -> dict:
    """
    Keepalive message leading to no reaction to keep the connection running.

    ### Server -> Client
    """

    return {
        "command": C_KEEPALIVE
    }

########################################
### Game messages sent by the server ###
########################################

C_SET_PLAYER_STATES = "set_player_states"
"""Message command for setting the player states of currently participating clients"""
def SET_PLAYER_STATES(content: List[PlayerState]) -> dict:
    """
    Message updating the states of all players (including score, continue status etc) for a client.

    ### Server -> Client

    `content` contains the state of each single player.
    """

    return {
        "command": C_SET_PLAYER_STATES,
        "content": [e.encode() for e in content]
    }

C_SET_GLOBAL_PROGRESS = "set_global_progress"
"""Message command for setting the current global download progress"""
def SET_GLOBAL_PROGRESS(progress: float) -> dict:
    """
    Message for setting the current global download progress which is the number
    of already finished songs (float for progress of the currently running download).

    ### Server -> Client
    """

    return {
        "command": C_SET_GLOBAL_PROGRESS,
        "progress": progress,
    }

C_ENTER_LOBBY = "enter_lobby"
"""Message command for notifying a client of the lobby being entered"""
def ENTER_LOBBY() -> dict:
    """
    Message notifying a client of the lobby being entered.

    ### Server -> Client
    """
    return {
        "command": C_ENTER_LOBBY
    }

C_WAIT_FOR_NEXT_GAME = "wait_for_next_game"
"""Message command for indicating that the server placed a client in the queue for the next game"""
def WAIT_FOR_NEXT_GAME() -> dict:
    """
    Message indicating that the server placed a client in the queue for the next game as there still
    is a game running. The client shall remain in this state until the server notifies it to enter a
    lobby.

    ### Server -> Client
    """
    return {
        "command": C_WAIT_FOR_NEXT_GAME
    }

C_SET_SONG_LIST = "set_song_list"
"""Message command for setting the list of songs for the current game"""
def SET_SONG_LIST(content: List[dict]) -> dict:
    """
    Message for setting the list of songs for the current game.

    ### Server -> Client

    `content` contains entries of form `{'title':...,'artist':...,'url':...,'user':...}`.
    """

    return {
        "command": C_SET_SONG_LIST,
        "content": content
    }

C_REQUEST_SONG_INDEX = "request_song_index"
"""Message command for requesting the status of the song at the given index"""
def REQUEST_SONG_INDEX(index: int) -> dict:
    """
    Message to request the download status (finished download, failed, still waiting) of the song
    at the given index.

    ### Server -> Client
    """

    return {
        "command": C_REQUEST_SONG_INDEX,
        "content": index
    }

C_ENTER_PAUSE = "enter_pause"
"""Message command for instructing a client to enter a pause"""
def ENTER_PAUSE() -> dict:
    """
    Message instructing a client to enter a pause.

    ### Server -> Client
    """

    return {
        "command": C_ENTER_PAUSE
    }

C_PLAY_SONG = "play_song_index"
"""Message command for instructing a client to play a song"""
def PLAY_SONG(index: int, timestamp: float) -> dict:
    """
    Message instructing a client to play the song at the given index
    starting at the given time in seconds.

    ### Server -> Client
    """

    return {
        "command": C_PLAY_SONG,
        "content": index,
        "timestamp": timestamp
    }

C_ENTER_GUESSING = "guess"
"""Message command for instructing a client to enter the guessing phase"""
def ENTER_GUESSING(player_name: str, player_id: int) -> dict:
    """
    Message instructing a client to enter the guessing phase after the player
    with given name and id buzzered.

    ### Server -> Client
    """

    return {
        "command": C_ENTER_GUESSING,
        "player_name": player_name,
        "player_id": player_id
    }

C_ENTER_NEUTRAL_GUESSING = "neutral_guess"
"""Message command for instructing a client to enter the 'neutral' guessing phase where there is no
    designated player who buzzered"""
def ENTER_NEUTRAL_GUESSING() -> dict:
    """
    Message instructing a client to enter the 'neutral' guessing phase where no player buzzered.

    ### Server -> Client
    """

    return {
        "command": C_ENTER_NEUTRAL_GUESSING,
    }

C_ENTER_SOLVING = "solve"
"""Message command for instructing a client to enter the solving phase"""
def ENTER_SOLVING(assigner_id: Optional[int], substitute: bool) -> dict:
    """
    Message instructing a client to enter the solving state for the last played song where the
    player with the given id has to assign points and may substitute the original assigner.
    The id may also be `None` if there is no assigner due to skipping of a song.
    In 'no buzzers' mode, assigner will always be a substitute.

    ### Server -> Client
    """

    return {
        "command": C_ENTER_SOLVING,
        "assigner_id": assigner_id,
        "substitute": substitute
    }

C_ENTER_SPECIFIC_SONG_SOLVING = "solve_specific_song"
"""Message command for instructing a client to enter the solving phase for a specific song index"""
def ENTER_SPECIFIC_SONG_SOLVING(index: int, assigner_id: Optional[int], substitute: bool) -> dict:
    """
    Message instructing a client to enter the solving state for the song with the given index where
    the player with the given id has to assign points and may substitute the original assigner.
    The id may also be `None` if there is no assigner due to skipping of a song.
    In 'no buzzers' mode, assigner will always be a substitute.

    ### Server -> Client
    """

    return {
        "command": C_ENTER_SPECIFIC_SONG_SOLVING,
        "index": index,
        "assigner_id": assigner_id,
        "substitute": substitute
    }

C_SET_POINT_ASSIGNMENT = "set_point_assignment"
"""Message command for setting the current point assignment concernign all players"""
def SET_POINT_ASSIGNMENT(assignment: Dict[int, int]) -> dict:
    """
    Message for setting the current point assignment concerning all players.

    ### Server -> Client

    `assignment` contains entries of form `player id => assigned points`.
    Keys for players with no assigned points do not necessarily have to be present.
    """

    return {
        "command": C_SET_POINT_ASSIGNMENT,
        "content": assignment
    }

C_END = "end"
"""Message command for instructing a client to enter the ending phase"""
def END() -> dict:
    """
    Message instructing a client to enter the ending state.

    ### Server -> Client
    """

    return {
        "command": C_END
    }

C_RESET = "reset"
"""Message command for instructing a client to reset their internal game state"""
def RESET() -> dict:
    """
    Message for instructing a client to reset their internal game state.

    ### Server -> Client
    """

    return {
        "command": C_RESET
    }

C_WAIT_FOR_REHABILITATION = "wait_for_rehabilitation"
"""Message command for instructing a client to wait to be rehabilitated"""
def WAIT_FOR_REHABILITATION(songs: List[dict]) -> dict:
    """
    Message instructing a client to wait to be rehabilitated and transmitting the songs
    of the current download so that the client can continue downloading.

    ### Server -> Client

    `songs` contains entries of form `{'title':...,'artist':...,'url':...,'user':...}`.
    """

    return {
        "command": C_WAIT_FOR_REHABILITATION,
        "songs": songs
    }

C_REHABILITATE_INTO_INIT_DOWNLOADING = "rehabilitate_into_init_downloading"
"""Message command for notifying a client of being rehabilitated into the initial download"""
def REHABILITATE_INTO_INIT_DOWNLOADING() -> dict:
    """
    Message notifying a client of being rehabilitated into the initial download.

    ### Server -> Client

    Note that other rehabilitation messages are not required as during a running game,
    clients can be rehabilitated by `ENTER_PAUSE()`.
    """

    return {
        "command": C_REHABILITATE_INTO_INIT_DOWNLOADING
    }

C_START_TIME_LIMIT = "start_time_limit"
"""Message command for notifying a client that a time limit has started"""
def START_TIME_LIMIT(time_limit: float) -> dict:
    """
    Message notifying a client that a time limit has started with the given length.

    ### Server -> Client
    """

    return {
        "command": C_START_TIME_LIMIT,
        "content": time_limit
    }

########################################
### Game messages sent by the client ###
########################################

C_SET_CONTINUE = "set_continue"
"""Message command for indicating whether a client wishes to continue"""
def SET_CONTINUE(continue_: bool, state: str) -> dict:
    """
    Message indicating whether a client wishes to continue.

    ### Client -> Server

    The current state is also transmitted to prevent late continues from previous
    states from influencing the current one.
    """

    return {
        "command": C_SET_CONTINUE,
        "content": continue_,
        "state": state
    }

C_UPDATE_LOCAL_PROGRESS = "update_local_progress"
"""Message command for setting the current local download progress"""
def UPDATE_LOCAL_PROGRESS(progress: float) -> dict:
    """
    Message for transmitting the current local download progress which is the number
    of already finished songs (float for progress of the currently running download).

    ### Client -> Server
    """

    return {
        "command": C_UPDATE_LOCAL_PROGRESS,
        "content": progress
    }

C_REPLY_SONG_INDEX = "reply_song_index"
"""Message command for song index reply messages containing the status of the requested song"""
def REPLY_SONG_INDEX(status: SongStatus, index: int, length: float) -> dict:
    """
    Reply message to a song index request message containing index of the requested song,
    its download state and its length in second which is set to an arbitrary value
    if the song has not already been downloaded successfully.

    ### Client -> Server
    """

    return {
        "command": C_REPLY_SONG_INDEX,
        "content": status,
        "index": index,
        "length": length
    }

C_BUZZER = "buzzer"
"""Message command for indicating that the buzzer was pressed"""
def BUZZER() -> dict:
    """
    Message indicating that the buzzer was pressed.

    ### Client -> Server
    """

    return {
        "command": C_BUZZER
    }

C_ASSIGN_POINTS = "assign_points"
"""Message command for assigning points to a player"""
def ASSIGN_POINTS(player_id: int, points: int) -> dict:
    """
    Message for assigning the given amount of points to the player with given id.

    ### Client -> Server
    """

    return {
        "command": C_ASSIGN_POINTS,
        "player": player_id,
        "points": points
    }
