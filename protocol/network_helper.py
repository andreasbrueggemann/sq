"""Collection of helper functions for networking"""

import socket
from ipaddress import ip_address, IPv4Address, IPv6Address

def get_socket_for_ip_address(ip_addr: str) -> socket.socket:
    """
    Returns a socket using the layer 3 protocol also used by the given ip address.

    This function DOES NOT connect or bind the socket.
    """

    ip_addr_obj = ip_address(ip_addr)
    if isinstance(ip_addr_obj, IPv4Address):
        return socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    elif isinstance(ip_addr_obj, IPv6Address):
        return socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    else:
        raise ValueError("Malformed server address protocol")
