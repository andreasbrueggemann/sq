# SQ - Multiplayer Song Quiz

Game to listen to songs and guessing them faster than your friends!
![The game while listening to a song](https://gitlab.com/andreasbrueggemann/sq/-/wikis/uploads/ef880ac00b2f0fa48e7387d797da93d3/listening.png "The game while listening to a song")

## Gameplay

With SQ, you and your friends can listen to the same song and try to recognize it.
The first one to buzzer stops the song and has to give their answer.
For each correct answer, you can earn points until one player has reached the point limit and wins the match.

* ***Which songs are in the quiz?*** You decide! All that you need is a collection of songs where for each song you only need Youtube-URL, title and artists. You do **not** need to have all of the songs as files or download them manually. SQ will automatically download all of them for you, only using the provided URL.
* ***How does SQ decide if your answer is correct?*** It doesn't. Play with your friends in a voice chat or video call and just say what you think that you just listened to. Then, you and your friends get to decide how much points your answer was worth. You are free to decide how points are assigned. Maybe one for the correct title, one for the artists? Wanna correct what your friend just answered? You may also get a point! Just use the rules that you like the most.
* ***Can I customize the gameplay?*** Yes, you can! There are multiple settings for the server to decide how many points are to be reached, if a song starts from its beginning or somewhere in the middle, if slower buzzering players also get a chance to answer and many more. We are extending the possibilities all the time.

### Course of a Match

* **Lobby:** All players connect to the lobby and only proceed as soon as everyone is ready to go.
* **Initial Downloading:** The game starts downloading the first few songs before starting so that you do not have to wait again during the running game. During later steps, the download proceeds in the background.
* **The Quiz:** There are multiple rounds where songs are played and everyone has to guess. The following steps repeat until someone reaches the score necessary to win the game. The  game also ends after some number of songs has been played even if no player reaches this score.
    * **Listening:** Everyone listens to the same song at the same time. This step ends as soon as one player uses their buzzer. If you do not recognize the song, you can decide to skip it. If everyone wants to skip the song, it ends and the game immediately continues to **Solving**.
    * **Guessing:** The music stops and the player who buzzered has to guess the song's title and artist(s) (or whatever you decide that the player should be able to guess). There is no need of entering your guess in the game itself. Instead, use a voice chat or similar. You may also decide to let other players correct the player who buzzered if this one got something wrong. The game continues as soon as every player clicks their *continue*-button.
    * **Solving:** The solution is shown to everyone. If the song was not skipped, the player who buzzered can now assign points to all players. Everyone has to agree to the point assignment in order to let the game continue. Here are some ideas for how to assign points, just use some of these ideas or make up your own to customize your experience:
        * You get one point for the correct song title and one point for the correct artists.
        * If the buzzering player got something wrong, other players can snatch their points for correct title or artist if they were able to correct the buzzering player during **Guessing**.
        * One more point may be assigned for additional information. The release year of the song? The album? The original version if the song was a cover? Decide what suits you best!
* **The End:** The game ends and the player with the highest score wins. Everyone can decide to leave or go back to the lobby.

![The game while assigning points in the Solving phase](https://gitlab.com/andreasbrueggemann/sq/-/wikis/uploads/ba8d1d6644cf811c7568121e98767165/solving.png "The game while assigning points in the Solving phase")

During the match, it may happen that the game pauses as the next song still needs some time to download, a player just reconnected or similar reasons. In such cases, it continues as soon as everyone decides that they are ready.

### Gameplay Customization

Besides many possibilities to customize your rules for guessing songs and assigning points, there are some options to configure the game for additional mechanics spicing things up.

#### Random Start

You don't want to listen to all songs from the beginning? Recognizing song intros is too boring for you? You should try out *Random Start* (even activated by default since Beta 1.1). Instead of playing all songs from the start, a random start point in the middle is chosen where every player starts to listen. You get to listen to different parts of your songs and even if the same song repeats in your next quiz, it may start from an entirely other position offering you much more variation.

#### Buzzer Slack

Some player is always buzzering a tenth of a second faster than you? *Buzzer Slack* may be just the right thing to increase your chances of being the one who gets to guess the song. You can activate this option and specify some slack, let's say 0.5 seconds just as an example. Now, when the first player buzzers, the song continues to play for these 0.5 seconds and everyone else still can buzzer. Then, the game randomly selects a player among the first one who buzzered and all other players who buzzered in the 0.5 seconds after to guess the song. Is this *fair*? Probably not completely, but it may just be the thing you are searching for if you want to give slower players slightly better chances to make your game more suspenseful.
Also, you can try out the compensation mode where the slack does not select a random player who buzzered, but the one with the lowest score.

#### No Buzzers

You don't like the competition about who buzzers first? Try the *No Buzzers* mode where a song is played until all players want to continue or a time limit exceeds. Then, all players can write down their guess. A solution is only provided in the end after all songs have been played. This is the perfect gamemode for you if you want a relaxed round.

### FAQ

* ***I lost connection to the server. Can I reconnect?*** Yes, you can. Just restart the game and connect to the same server (IP and port). Your username does not matter here, you will get your old one back when connecting. Then, you possibly have to wait until rehabilitation which happens right before the next song starts to play. So just tell your friends to wait for you restarting the game until they continue from **Solving**.
* ***Why is it that when I buzzer, there is some lag and the game does not continue immediately?*** There may be two reasons. First, your buzzering has to be transmitted to the server and then to all players. This may take a short time, depending on your ping. Also, **Buzzer Slack** exactly introduces such behaviour.
* ***Why is some other player selected for guessing even if I buzzered first?*** In a multiplayer game, it is normal that everything you do needs some time to be transmitted to the server and all other players. Likely, the other player buzzered first. If they did, their and your ping still add some time until your game knows that this player already buzzered. So, you may buzzer before your game knows that they already did but after they really did it. Also, the server does some ping-compensation. Another reason again may be **Buzzer Slack** which allows multiple players to buzzer and then randomly selects only one for guessing.
* ***Why is the game stuck at Pausing, Guessing or Solving?*** If there is a "Ready", "Continue" or "Solve" button on the bottom right, each connected player has to click it until the game proceeds. Regarding **Solving**, also have a look at the next question.
* ***Why does my "Continue"-button automatically deactivate again during Solving?*** In **Solving**, the player who buzzered assigns points to the players. By clicking "Continue", you agree to their point assignment. Therefore, the game automatically deactivates the button each time that the point assignment is changed. Let the responsible player assign the points and **then** click on continue.
* ***What can I do against downloads taking too much time?*** Unfortunately, not much. Make sure that your download quality is set to "worst" (which it is by default). You may speed up future downloads by locally caching each downloaded song. In this case, have a look at the section about the **Permacache**.

## Installing the Game

**If you want to host a server to play with your friends who use windows, your friends can likely skip the following steps and use a standalone version. In this case, go to section *Standalone Version***

### Requirements

SQ has the following requirements to run:

* Python 3.8 or higher
* Packages available via PIP (`pip install [package name]` or `pip3 install [package name]`)
    * requests
    * yt_dlp (Not required when only running the server)
    * pygame (Not required when only running the server)
    * pyinstaller (Only for building the windows standalone version)
* Tk, examples for installing it on different operating systems are:
    * Windows: When installing Python on Windows, this should be installed with it by default
    * Ubuntu: `sudo apt install python3-tk`
    * Manjaro: Package "tk" from the official Manjaro repositories
* ffmpeg, examples for installing it on different operation systems are:
    * Windows: You can download a release zip from [https://www.gyan.dev/ffmpeg/builds/](https://www.gyan.dev/ffmpeg/builds/) and copy `ffmpeg.exe` from the zip to the `bin` directory of SQ. These release builds are from a third party and we do not give any warranty about the safety of using them.
    * Ubuntu: `sudo apt install ffmpeg`
    * Manjaro: Package "ffmpeg" from the official Manjaro repositories

### Configuration Files

After installing all requirements, you have to add your own personalized configuration files to SQ.
The configuration files are `client.ini` and `server.ini` in the main directory of SQ that are yet to be placed there by you. Of course, you only need the configuration file for the client if you do not run the server yourself and vice versa.
You may use the configuration files `client.ini` and `server.ini` inside the `config` directory as templates, just copy them over to the SQ main directory. If you only want the most important configuration options, you may instead use `client_min.ini` and `server_min.ini` from the `config` directory. Then, besides copying them you also have to remove the "_min" from the names of their copies.

The configuration files are split into multiple sections each containing their own settings. Each setting that needs to be changed by you manually before running SQ is marked by a "TODO". In the following, the most important settings are documented. A documentation of each single setting is given inside the template `client.ini` and `server.ini` files.

#### Important Server Configuration

* `[network]`
    * `ip_address`: IP address used by the server, may be IPv4 or IPv6. Default value is `127.0.0.1` for local use.
    * `port`: Port used by the server. Default value is `65194`.
* `[gameplay]`
    * `score_to_reach`: Score to be reached by one player for the game to end. Default value is `30`.
    * `songs_to_fetch`: Maximal number of songs used for a single game. Default value is `100`.
    * `random_start`: Whether the gameplay customization option *Random Start* is to be used. Default value is `True`.
    * `buzzer_slack`: Time interval used by the gameplay customization option *Buzzer Slack*. *Buzzer Slack* is not being used if set to `0.0`. Default value is `0.0` (*Buzzer Slack* deactivated).
    * `buzzer_slack_compensation_mode`: Enable compensation mode for *Buzzer Slack* where players with lower score get an advantage. Default value is `False`.
    * `listening_time_limit`: Time limit for listening to a song before it is skipped. Default value is `0.0` representing no limit.
    * `listening_time_limit_skip_threshold`: Percentage of players that have to press 'continue' to start the `listening_time_limit`. Default value is `0.0` starting the timer immediately. Set to `1.0` to start time limit if all but one player want to continue.
    * `guessing_time_limit`: Time limit for guessing a song after it has been buzzered. Default value is `0.0` representing no limit.
    * `guessing_time_limit_hard`: Enable hard limit for `guessing_time_limit` that immediately proceeds to solution after time has passed. Default value is `False`.
    * `no_buzzers`: Enable *No Buzzers* gamemode. Default value is `False` corresponding to standard gamemode.
* `[database]`
    * `url`: URL of the website where the songs for each quiz are taken from. **Required to be set manually**
    * `post`: Whether accessing the website uses `POST` or `GET`. Default value is `True` (`POST` is used).
    * `username`: Username to authenticate to the website. **Required to be set manually**
    * `password`: Password to authenticate to the website. **Required to be set manually**

#### Important Client Configuration

* `[network]`
    * `ip_address`: Default IP address to connect to, used as a preset in the launcher where it can also be changed. Default value is `127.0.0.1` for local use.
    * `port`: Default port to connect to, used as a preset in the launcher where it can also be changed. Default value is `65194`.
* `[gameplay]`
    * `username`: Default username, used as a preset in the launcher where it can also be changed.
* `[download]`
    * `path_ffmpeg`: Set this to `True` if the path to `ffmpeg` is contained in your PATH variable (should usually be the case on Linux if installed using a package manager). Tet it to `False` if `ffmpeg.exe` is placed inside the `bin` directory of SQ (should usually be the case on Windows if ffmpeg was previously copied to the directory). **Required to be set manually**
    * `permacache_downloads`: Set this to `True` to permanently cache all songs that were downloaded once. See the section on the *Permacache*. Default value is `False`.
* `[database]`
    * `url`: URL of the website where the songs for the *Proactive Download* are taken from. Not set by default, only required if using *Proactive Download*.
    * `post`: Whether accessing the website uses `POST` or `GET`. Not set by default, only required if using *Proactive Download*.
    * `username`: Username to authenticate to the website. Not set by default, only required if using *Proactive Download*.
    * `password`: Password to authenticate to the website. Not set by default, only required if using *Proactive Download*.

#### Quick Tutorial for Working With Configuration Files.

This tutorial only gives you an example. Do not directly apply the changes shown here to your configuration files.
This is a part of `config/client.ini`:

    [download]
    # TODO uncomment one of the options depending on whether PATH contains a path to FFMPEG or FFMPEG
    # binaries are given in the binary folder specified by ffmpeg in [files] (default: bin/ffmpeg.exe).
    # If you are using Windows and have placed the binary in the folder, set the value to False.
    # If FFMPEG is installed on your system and included in your PATH variable (e.g. if installed using
    # apt or similar under Linux), set the value to True.
    # vvvvvvvvvvvvvvvvvvv
    # Binaries are in FFPEG binary folder:
    ; path_ffmpeg = False
    # FFMPEG installed, contained in PATH:
    ; path_ffmpeg = True
    
    # Uncomment if you do not want the audio volume to be normalized to a peak of -0.0dB.
    # Disabling normalization will increase the game's performance.
    # default: True
    ; normalize_audio_volume = False

    # Uncomment to change the path where songs are downloaded to during a match.
    # default: temp
    ; download_path = temp

In this example, you can see two settings inside the download section of the client configuration. Each line beginning with "`#`" only provides documentation to the given setting. Specific settings appear like this in the configuration files:

    some_setting = some_value

In the example from `config/client.ini`, you can see such settings with "`;`" and a whitespace in front. This means that these settings are not activated and the default settings are used. Now, assume that you want to set setting "`path_ffmpeg`" to "`True`", setting "`normalize_audio_volume`" to "`False`" instead of its default value "`True`" and setting "`download_path`" to "`other_temp`" instead of its default value "`temp`".
Then, you have to remove the corresponding occurences of "`;`" and whitespaces and make sure that all settings are set to the correct values, e.g.,  "`other_temp`" is the value for "`download_path`". The result should be like this:

    [download]
    # TODO uncomment one of the options depending on whether PATH contains a path to FFMPEG or FFMPEG
    # binaries are given in the binary folder specified by ffmpeg in [files] (default: bin/ffmpeg.exe).
    # If you are using Windows and have placed the binary in the folder, set the value to False.
    # If FFMPEG is installed on your system and included in your PATH variable (e.g. if installed using
    # apt or similar under Linux), set the value to True.
    # vvvvvvvvvvvvvvvvvvv
    # Binaries are in FFPEG binary folder:
    ; path_ffmpeg = False
    # FFMPEG installed, contained in PATH:
    path_ffmpeg = True
    
    # Uncomment if you do not want the audio volume to be normalized to a peak of -0.0dB.
    # Disabling normalization will increase the game's performance.
    # default: True
    normalize_audio_volume = False

    # Uncomment to change the path where songs are downloaded to during a match.
    # default: temp
    download_path = other_temp

### Standalone Version

Installing all requirements on a Windows system may be a tedious task.
If you have already set up a server and want to play with your friends who use Windows without having them also do the whole installing procedure, they can also use a standalone version of the client.
Note that **we do not provide such standalone client**.
But you can set up one by yourself and provide it to your friends as described in the following (we currently have no standalone version for Linux as it is rather easy to install all dependencies given a package manager):

* First, you have to be on a Windows system to compile a standalone version.
* Copy `ffmpeg.exe` (see **Requirements** for instructions how to obtain this file) into the folder `sq_builds`.
* Create a `client.ini` inside `sq_builds`. Use `sq_builds/client_example.ini` as reference that you can modify by, e.g., adding your server's IP address as default value already and fixing further default settings that you want to give to your friends. Mind that there are a few peculiarities regarding the settings. Some of the files, i.e., `version.ini`, `lobby_music.ogg`, `failed_downloads.txt`, `playlist.txt` are moved one folder to the top compared to default settings. If you want to use the *Permacache*, you should also do this for the corresponding folder by, e.g., using `permacache_path = ../song_database`. Also, the example config file already contains the correct settings for ffmpeg.
* Run `./windows_make_standalone.bat OUTPUTDIR` where `OUTPUTDIR` is the name of a folder that should be created inside `sq_builds` and contain the standalone version.
* Add the output files inside `OUTPUTDIR` to a ZIP archive and distribute it to your friends.
* Note that `OUTPUTDIR/sq` contains all relevant output files that are `dist`, `song_database` (for the *Permacache*), `client.bat` and `client.ini`. Your friends just have to run `client.bat`. The files that were previously moved to the top in the config files are the ones that may be of interest for your friends. The real client directory is `dist` and does not need to be accessed directly. This is so that you have a clean directory with only your important files while all DLLs and so on of the compiled version stay hidden.
* Now, if you want your friends to update their client, just distribute a new client and ask them to only replace the old `dist` folder by the new one. Of course, also ask them to update their config if you changed something there. This way, they keep their *Permacache*, playlist and so on when updating.
* Also note that while `dist` **may** contain a file `client.ini`, it is of no interest as it gets replaced by the top level `client.ini` when starting the client using `client.bat`. This is due to technical reasons: While some other files are moved to the top directory in the config, the config cannot be used to determine its own location. Thus, we copy it into the client directory for each game.

## Song Database

To select songs for each quiz, the server needs access to some kind of song database. We plan to add many different options to create and use different kinds of databases in the future. Currently, only the following method works:

Set up a webserver that can return lists of songs to the server. For each game, the server requests a song from the URL given as `url` in the database section of the configuration. The server uses a HTTP POST request using the following arguments:

* `username`: Username as given in the configuration file, used to authenticate to the webserver.
* `password`: Password as given in the configuration file, used to authenticate to the webserver.
* `limit`: The number of songs to return, as given by `songs_to_fetch` in the configuration file. If less songs exist, these songs should all be returned. If this value is set to `0`, all songs are returned.

The response of the webserver is expected to be a JSON list containing objects with the following keys each together with their values as strings:

* `title`: Title of the song
* `artist`: Artists of the song
* `url`: Youtube URL of the song. If a starting timestamp is given (e.g. `t=30` in the query), it is used as the start of the song and the part of the video's audio before this timestamp will never be played to the players during a quiz.
* `user`: Name of the user who uploaded the song to the database.

Example response (HTTP 200):

    [
        {
            "title": "Bohemian Rhapsody",
            "artist": "Queen",
            "url": "https://www.youtube.com/watch?v=fJ9rUzIMcZQ",
            "user": "Ikea Blahaj"
        },
        {
            "title": "Take On Me",
            "artist": "a-ha",
            "url": "https://youtu.be/djV11Xbc914?t=3",
            "user": ""
        }
    ]

Instead of using a HTTP POST request, you can also use HTTP GET requests. In this case, setting `post` has to be set to `False`.
Then, username and password are used for HTTP authentication while `limit` is given in the query.

All responses are assumed to be smaller than 4GiB due to technical reasons as messages handled by server and cient must not be longer.

## Versioning

The changelog can be found in [this file](CHANGELOG.md). There you can see that each version of SQ also has a specific *Protocol Version*. If the *Protocol Version* of client and server match, they are compatible, even if their overall SQ versions are different. Otherwise, the server will reject the client's connection.

If you run SQ for the first time at all or after an update, it will run a selftest where it essentially tries to download a song to ensure that the overall download handling is set up and working correctly on your device.

## Additional Client Functionality

### Playlist

In the **Solving** step, there is a button for adding the current song to your playlist. This button automatically adds title, artists and URL of the song to the file `playlist.txt` if not configured otherwise.

### Failed Downloads

If downloading a song fails, title, artists and URL are automatically written to the file `failed_downloads.txt` if not configured otherwise. You can use this file to test if the failed URLs are indeed not working anymore, e.g., due to copyright reasons. This can be useful to identify dead URLs and replacing them inside your database.

### Permacache

Downloading songs can take quite some time. If you enable the *Permacache* by setting `permacache_downloads` in your configuration file to `True`, each downloaded song will also be permanently saved inside the directory `song_database`. When you need this song again, it can just be taken from this directory saving you the time which would be required to download it again. Note that as the *Permacache* saves all songs and never deletes them, it can take much disk space, so keep an eye on the size of the directory.

Even if `permacache_downloads` is set to `False`, songs already cached can still be used by SQ. This means that you can enable the *Permacache* and then disable it if you do not want the directory to grow any larger. In this case, the already downloaded songs still help decreasing the time you need for downloading.

If you delete files from the directory `song_database`, please make sure to never remove `version.ini`. Otherwise, your *Permacache* could be corrupted.

### Proactive Download

If you have access to the database where the server gets its songs from, you can even fill your *Permacache* proactively without a running game. Just select the corresponding option in the launcher. If you do want to use this otpion, you need to set the corresponding database configuration for the client.

The proactive download can fill the *Permacache* even if it is disabled (`permacache_downloads` set to `False`).

## Contribute

If you want to contribute to the development of SQ, feel free to work on it and make a new merge request. If there are any problems or questions, feel free to open an issue. Some development information is given here. More information will follow as needed.

### Song Downloading

All songs are downloaded using yt_dlp. We need additional extractors to get starting timestamps and unique identifiers (e.g. Youtube video ids, needed for filenames in the *Permacache*) from each Youtube URL. As yt_dlp can also download from many other websites, SQ could be extended here. For each new website, a custom song data extractor needs to be added. Have a look at the existing one for youtube. There also are unittests for the extraction. Also have a look at `downloader.py`. Mind that each single download must eventually result in an ogg file.

### Testing

For testing purposes, have a look at the debug section of the client's configuration. With the correct configuration you can fill the folder specified by `local_temp_path` by files `song_0.ogg`, `song_1.ogg` etc. and instead of wasting much time downloading the correct songs, SQ will just simulate downloads by a short pauses and copying these files over using them instead of downloaded ones. If a file is missing, a failed download is simulated.

You may directly run `python client.py 0` to start the game using the default server information without opening the launcher first. Also, you can run `python client.py 1`, `python client.py 2` and so on to run multiple clients on the same machine from the same SQ directory. Instead of using file `failed_downloads.txt`, `python client.py 1` will use its exclusive file `failed_downloads.txt_1` then. Also, separated download directories are used in this case.

### Commands to Run

Run unittests (currently only for song data extraction):

    python -m unittest -v

Run pylint with the project configuration (ignoring fixme (TODOs) and duplicate-code (seems to be bugged sometimes) or not ignoring them):

    ./run_pylint.sh
    ./run_pylint.sh all

### Do's

* Use the previously mentioned commands frequently
* Search for TODOs
* Name your TODOs "# TODO [version that should do so]", e.g., "# TODO Beta 1.5"

### Don'ts

* Don't use `sys.exit()`, instead, raise Exceptions and let them be handled on a higher level, for instance such that the bug reporter can correctly output unexpected behaviour.
