import selectors
import socket
import time
from typing import List

from misc import consts
from misc.version import PROTOCOL_VERSION
from misc import logger
from misc.tickrate_manager import TickrateManager
from misc.song import SongStatus
from protocol import network_helper
from protocol.connection_handler import ConnectionHandler
from protocol.messages import * # pylint: disable=wildcard-import, unused-wildcard-import
from config import config
from .player import Player
from .state.game_context import GameContext

class Server:
    """SQ Server"""

    def __init__(self, host: str, port: int):
        """Constructs and runs a new server on the given host ip address and port"""

        logger.info(f"Starting server at {host}:{port}...")
        self.__host = host
        self.__port = port

        self.__last_keepalive = 0.0
        """Time when last keepalive message was sent to the connected clients"""

        self.__selector = selectors.DefaultSelector()
        """The server's selector for managing communication"""

        self.__players: List[Player] = []
        """List of all connected players, just used here to handle timed messages"""

        # Open nonblocking TCP socket
        self.__socket = network_helper.get_socket_for_ip_address(host)
        self.__socket.bind((self.__host, self.__port))
        self.__socket.listen()
        self.__socket.setblocking(False)
        self.__selector.register(self.__socket, selectors.EVENT_READ)

        # Start game
        self.__game_context = GameContext()
        logger.info("Server started.")
        self.__main_loop()

    def __main_loop(self): # pylint: disable=too-many-branches
        """Main loop of the SQ server"""
        try: # pylint: disable=too-many-nested-blocks
            tickrate_manager = TickrateManager(config.Timeouts.base_tickrate())
            while True:
                # Wait for next message for at most the idle time for the specified tickrate
                events = self.__selector.select(tickrate_manager.get_next_idle_time())

                # Begin to process next game tick
                tickrate_manager.start_tick()

                # Handle all socket events
                for key, mask in events:
                    if key.data is None: # No data set => server socket, new incoming connection
                        self.__accept_connection(key.fileobj)
                    else: # Communication with a client
                        connection_handler: ConnectionHandler = key.data
                        player: Player = connection_handler.get_linked_data()

                        try:
                            if mask & selectors.EVENT_READ: # New incoming data exists
                                messages = connection_handler.get_incoming_messages()
                                for message in messages:
                                    if player.is_connected():
                                        self.__handle_message(message, player)
                            if mask & selectors.EVENT_WRITE:# Output data can be written,
                                # `EVENT_WRITE` should only be used if such data exists
                                if player.is_connected():
                                    connection_handler.write()
                        except ConnectionError as error:
                            logger.warning(f"{str(player)}: Lost connection: {str(error)}")
                            player.disconnect()
                            self.__game_context.state().player_disconnect(player)
                            self.__players.remove(player)

                # Polling of timed player outputs
                for player in self.__players:
                    player.poll_timed_messages()

                # Server state polling for timed events
                self.__game_context.state().poll()

                # Keepalive messages
                elapsed = time.time() - self.__last_keepalive
                if elapsed > config.Timeouts.keepalive_interval():
                    self.__last_keepalive = time.time()
                    logger.info("Sending keepalive messages")
                    for player in self.__game_context.get_connected_players():
                        player.send_message(KEEPALIVE())

        except KeyboardInterrupt:
            logger.info("Closing server...")
        finally:
            self.__selector.close()

    def __accept_connection(self, connecting_socket: socket.socket):
        """Accepts and registers a new connection from the given socket"""
        new_socket, address = connecting_socket.accept()
        address = address[:2] # Cut away unneeded ipv6 parts
        logger.connect(*address)
        new_socket.setblocking(False)

        # Construct connection handler and register for selector
        connection_handler = ConnectionHandler(self.__selector, new_socket, address)
        self.__selector.register(new_socket, selectors.EVENT_READ, data = connection_handler)
        player = Player(connection_handler)
        connection_handler.set_linked_data(player)
        self.__players.append(player)

        # Finally, measure player ping
        player.ping()

    def __handle_message(self, message: dict, player: Player): # pylint: disable=too-many-branches
        """Handles the given received message from the given player"""
        try:
            command = message["command"]
            if command == C_JOIN_GAME:
                logger.info(f"{str(player)} tries to join the game...")
                if message["protocol_version"] == PROTOCOL_VERSION:
                    player.send_message(PROVIDE_CONFIG(config.Download.lookahead(),
                                                       config.Gameplay.no_buzzers(),
                                                       config.Gameplay.maximum_score_per_song()))

                    # Search for player id among list of disconnected players
                    # in case it is a reconnect
                    player_id = message["id"]
                    reconnect = False
                    for dc_player in self.__game_context.get_disconnected_players():
                        if dc_player.get_id() == player_id:
                            reconnect = True
                            player.send_message(SET_PLAYER_ID(player_id))
                            self.__game_context.state().player_reconnect(player, dc_player)
                            break
                    if not reconnect:
                        player.set_name(message["name"][:consts.MAX_PLAYERNAME_LENGTH])
                        player.send_message(SET_PLAYER_ID(player.get_id()))
                        self.__game_context.state().player_join(player)
                else:
                    player.send_message(REFUSE_CONNECTION(REFUSE_CONNECTION_REASON_PROTOCOL_VERSION,
                                                            PROTOCOL_VERSION))
                    logger.warning(f"{str(player)} (name: {message['name']}) tried to connect with "
                                    + f"protocol version {message['protocol_version']}, "
                                    + f"expected version {PROTOCOL_VERSION}")
            elif command == C_PING_REPLY:
                player.ping_reply()
            elif command == C_UPDATE_LOCAL_PROGRESS:
                self.__game_context.state().player_progress_update(player, message["content"])
            elif command == C_REPLY_SONG_INDEX:
                self.__game_context.state().player_song_index_reply(player,
                                                                    SongStatus(message["content"]),
                                                                    message["index"],
                                                                    message["length"])
            elif command == C_BUZZER:
                player.receive_timed_buzzer()
            elif command == C_ASSIGN_POINTS:
                self.__game_context.state().player_assign_points(player, message["player"],
                                                                    message["points"])
            elif command == C_SET_CONTINUE:
                self.__game_context.state().player_continue(player, message["content"],
                                                            message["state"])
            else:
                logger.error(
                    f"Malformed message from {str(player)}: Unknown command. Disconnect client...")
                player.disconnect()
                self.__game_context.state().player_disconnect(player)
                self.__players.remove(player)
        except KeyError as key_error: # Some message key was missing
            logger.error(f"Malformed message from {str(player)}: Missing key {str(key_error)}. "
                            + "Disconnect client...")
            player.disconnect()
            self.__game_context.state().player_disconnect(player)
            self.__players.remove(player)
