# pylint: disable=cyclic-import

from typing import List, Deque, Optional
from collections import deque

from server_mod.player import Player
from misc.song import Song

from .game_state import GameState

class GameContext:
    """
    Context of a `GameState` that holds such state and keeps track of variables which are to be
    preserved when state transitions appear.
    """

    def __init__(self):
        """Constructs a new instance that enters the `Waiting` state"""

        self.__next_game_queue: List['Player'] = []
        """List of players that are waiting for the server to enter its `Waiting` state again"""
        self.__current_game_players: List['Player'] = []
        """List of players that are in the current game or lobby"""
        self.__current_game_disconnected_players: List['Player'] = []
        """List of players that have been in the current game but disconnected / lost connection"""
        self.__current_game_reconnecting_players: List['Player'] = []
        """List of players that have been disconnected but are connecting again and wait to be
            rehabilitated into the running game"""

        self.__song_list: List[Song] = []
        """List of songs used by the current game"""
        self.__next_song_index: int = 0
        """Index of the next song in `__song_list` to play"""
        self.__played_song_indices: Deque[int] = deque()
        """Deque of indices (in `__song_list`) of songs that have already been played in the current
        game that can later be popped again to, e.g., repeat these songs."""

        self.__transition_stack_depth: int = 0
        """
        Depth of the current 'transition stack'.

        This should normally be `1` if a transition started by constructing a new state (this is
        handled by `@_game_state_constructor`) and then be reset to `0` if it is executed when
        `transition()` returns.
        The problem appears for chained transitions:
        ```
        self.context().transition(A())
        class A:
            def __init__(self):
                ...
                self.context().transition(B())
        ```
        This would construct `A` and then set is as the new state, but constructing `A` itself
        constructs `B` and sets it as new state. While this itself may be intended, it will lead to
        the game going to state `A` because the transition to `B` happens first, then the transition
        to `A` in this execution order.

        This issue is resolved by tracking in `__transition_stack_depth` the number of currently
        chained transitions. Only if it decreases after it was increased, the transition will be
        registered. In the example above, `A()` will set `__transition_stack_depth=1`, `B()` will
        set `__transition_stack_depth=2`, then calling `transition()` for `B` will set
        `__transition_stack_depth=1` and transition to `B` while calling `transition()` for `A` will
        set `__transition_stack_depth=0` which is the second decrease in a row that hence will not
        set `A` as the new state (`__state`).

        More elaborate example:
        ```
        self.context().transition(A())
        class A:
            @_game_state_constructor
            def __init__(self):
                ...
                self.context().transition(B())
                self.context().transition(C())
                self.context().transition(E())
        class C:
            @_game_state_constructor
            def __init__(self):
                ...
                self.context().transition(D())
        ...
        ```
        will lead to the following sequence of relevant function calls in this order:
        ```
        A()                     # __transition_stack_depth = 1
            B()                 # __transition_stack_depth = 2
            transition(B)       # __transition_stack_depth = 1 => set `__state = B`
            C()                 # __transition_stack_depth = 2
                D()             # __transition_stack_depth = 3
                transition(D)   # __transition_stack_depth = 2 => set `__state = D`
            transition(C)       # __transition_stack_depth = 1 => nothing because decreased before
            E()                 # __transition_stack_depth = 2
            transition(E)       # __transition_stack_depth = 1 => set `__state = E`
        transition(A)           # __transition_stack_depth = 0 => nothing because decreased before
        ```
        Note that this ensures that the last constructed state also is the one written to `__state`.

        TODO Beta 1.3 check if it is a better option to let the constructor of the new state
        immediately call the transition function instead of the former state. This could resolve
        this issue here in a more elegant fashion.
        """
        self.__transition_stack_returning: bool = False
        """Set to `True` if last relevant transition stack action was a return from `transition()`,
        i.e., `__transition_stack_depth` decreased (see doc of `__transition_stack_depth`)."""

        self.__state: GameState
        """Current game state"""
        # Import here to prevent circular imports
        from .waiting import Waiting
        self.transition(Waiting(self))

    def state(self) -> GameState:
        """Returns the current state of the game"""
        return self.__state

    def transition(self, state: GameState):
        """
        Transitions to the given state if the construction of said state did not itself transition
        to some new state already (see documentation of `__transition_stack_depth` for explanation).
        """

        assert self.__transition_stack_depth > 0
        if not self.__transition_stack_returning:
            self.__state = state
            self.__transition_stack_returning = True
        self.__transition_stack_depth -= 1

    def start_transition(self):
        """
        Registers the start of a state transition before it is executed.

        This must always be called if a new game state is constructed (done by
        `@_game_state_constructor`).
        See documentation of `__transition_stack_depth` for explanation.
        """

        assert self.__transition_stack_depth >= 0
        self.__transition_stack_depth += 1
        self.__transition_stack_returning = False

    def get_ingame_players(self) -> List[Player]:
        """
        Returns the list of all players connected to the current game.

        Items may be appended to or removed from the list.
        """
        return self.__current_game_players

    def get_reconnecting_players(self) -> List[Player]:
        """
        Returns the list of all players currently reconnecting to the current game.

        Items may be appended to or removed from the list.
        """
        return self.__current_game_reconnecting_players

    def get_disconnected_players(self) -> List[Player]:
        """
        Returns the list of all players that have been in the current game but disconnected / lost
        connection.

        Items may be appended to or removed from the list.
        """
        return self.__current_game_disconnected_players

    def get_connected_players(self) -> List[Player]:
        """Returns a list of all players that currently are connected to the server being ingame,
            reconnecting to a game, or in the queue for the next game."""
        return self.__current_game_players + self.__current_game_reconnecting_players \
                + self.__next_game_queue

    def get_all_players(self) -> List[Player]:
        """Returns a list of all players (ingame, reconnecting and disconnected, NOT queued)"""
        return self.__current_game_players + self.__current_game_reconnecting_players \
                + self.__current_game_disconnected_players

    def get_queued_players(self) -> List[Player]:
        """Returns a list of all players queued for the next game"""
        return self.__next_game_queue

    def get_song_list(self) -> List[Song]:
        """
        Returns the list of all songs used for the current game.

        Items may be appended to or removed from the list.
        """
        return self.__song_list

    def get_next_song_index(self) -> int:
        """Returns the index of the next song in `__song_list` to play"""
        return self.__next_song_index

    def increment_next_song_index(self):
        """Increments the index of the next song in `__song_list` to play by one"""
        self.__next_song_index += 1

    def reset_next_song_index(self):
        """Resets the index of the next song in `__song_list` to play to `0`"""
        self.__next_song_index = 0

    def add_song_index_as_played(self, index: int):
        """Adds the current song index to the list of already played songs"""
        assert index >= 0
        assert index < len(self.__song_list)
        self.__played_song_indices.append(index)

    def pop_played_song_index(self) -> Optional[int]:
        """Returns the next index of an already played song or `None` of none left"""
        if len(self.__played_song_indices) == 0:
            return None
        else:
            return self.__played_song_indices.popleft()
