# pylint: disable=cyclic-import

from typing import TYPE_CHECKING
import sys

from protocol.messages import REHABILITATE_INTO_INIT_DOWNLOADING, SET_SONG_LIST
from server_mod.player import Player
from config import config
from misc.song import Song, SongStatus
from misc import logger, song_importer

from .game_state import GameState, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext

class InitDownloading(GameState): # pylint: disable=abstract-method
    """
    Initial downloading state of the server.

    The game remains in this state until some lookahead of songs to be downloaded has been handled
    by each participating player.
    Thus, pauses in the remaining game caused by still running downloads should be avoided by giving
    the downloads some advance.
    The state is responsible for sampling the songs that the quiz round is using.

    ## Behaviour
    State `Searching` is entered effectively starting the game as soon as each player satisfies the
    chosen lookahead.
    Disconnected players may reconnect at any time.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext'):
        super().__init__(context)
        logger.info("Starting game...")

        # Select songs to use
        try:
            songs = song_importer.fetch_songs(config.Gameplay.songs_to_fetch())
        except ConnectionError as error:
            logger.error(str(error))
            sys.exit()
        for song in songs:
            song_object = Song.from_dict(song)
            if song_object.status == SongStatus.REJECTED:
                logger.error(f"Got url {song_object.url} which is not supported or allowed. "
                                + "This could be due to malicious database entries. "
                                + "Skipping the song...")
            else:
                self.context().get_song_list().append(song_object)

        self._send_to_ingame_players(
            SET_SONG_LIST([s.to_dict() for s in self.context().get_song_list()]))

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "InitDownloading"

    # No continue possible
    # No continue possible

    def poll(self):
        # Players may reconnect at any time
        self._try_rehabilitation()

    def _progress_update(self, progress: float):
        if progress >= 0.999 * config.Download.lookahead() or \
                        progress >= config.Gameplay.songs_to_fetch():
            # Evade problems due to floating point arithmetic => 0.999
            # Import here to prevent circular imports
            from .searching import Searching
            self.context().transition(Searching(self.context(), force_pause = True))

    # No special reaction to a player disconnecting required

    def _rehabilitate(self, player: Player):
        player.send_message(REHABILITATE_INTO_INIT_DOWNLOADING())

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected
    # No point assigning expected
    # Updates in continuing players not relevant
