# pylint: disable=cyclic-import

from typing import TYPE_CHECKING

from protocol.messages import ENTER_PAUSE
from server_mod.player import Player
from misc import logger

from .game_state import GameState, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext

class Pausing(GameState):
    """
    Pausing state of the server.

    This state represents a pause that is entered after the initial download finished, a player
    reconnected or the search for the next song to play took a time over some threshold.
    The game only continues if all players want so.

    ## Behaviour
    State `Pausing` is entered if a pause is triggered.
    It can only be left if all players wish to continue transitioning into state `Listening`.
    Players may reconnect at any time.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext', song_index: int, song_length: float):
        """
        Constructs a new instance for the given context.

        The parameters declare the index of the song to play after the pause and its length.
        """

        super().__init__(context)
        logger.info("Entering pause...")

        self.__song_index = song_index
        self.__song_length = song_length
        self._send_to_ingame_players(ENTER_PAUSE())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Pausing"

    @staticmethod
    def _state_can_continue() -> bool:
        return True

    def _continue(self):
        logger.info("Continuing from pause...")
        # Import here to prevent circular imports
        from .listening import Listening
        self.context().transition(Listening(self.context(), self.__song_index, self.__song_length))

    def poll(self):
        self._try_rehabilitation()

    # No handling of progress updates required
    # No special reaction to a player disconnecting required

    def _rehabilitate(self, player: Player):
        player.send_message(ENTER_PAUSE())

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected
    # No point assigning expected
    # Updates in continuing players not relevant
    