# pylint: disable=cyclic-import

from typing import TYPE_CHECKING
import time

from protocol.messages import END
from server_mod.player import Player
from config import config
from misc import logger

from .game_state import GameState, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext

class Ending(GameState): # pylint: disable=abstract-method
    """
    Ending state of the server.

    In this state, all players can view the final scoreboard and the server returns to its `Waiting`
    state after some timeout.

    ## Behaviour
    State `Ending` is entered from state `Searching` or state `Solving` if the score limit is
    reached or no songs to play remain.
    It is left after some timeout where the state transitions to state `Waiting`.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext'):
        super().__init__(context)
        logger.info("Ending game...")

        self.__end_time = time.time()
        self._send_to_ingame_players(END())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Ending"

    # No continue possible
    # No continue possible

    def poll(self):
        elapsed = time.time() - self.__end_time
        if elapsed > config.Timeouts.end_timeout():
            self._reset()

    # No handling of progress updates required
    # No special reaction to a player disconnecting required
    # Players are not rehabilitated in this state

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected
    # No point assigning expected
    # Updates in continuing players not relevant

    ### Overridden methods ###

    # override, disconnecting players do not have to be remembered due to having no relevant state
    def _remove_player_from_game(self, player: Player):
        self.context().get_ingame_players().remove(player)
        # No further action needed in end state, but remove to set reset only to still connected
