# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Optional
import time

from protocol.messages import ENTER_GUESSING, ENTER_NEUTRAL_GUESSING, START_TIME_LIMIT
from config import config
from server_mod.player import Player
from misc import logger

from .game_state import GameState, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext

class Guessing(GameState): # pylint: disable=abstract-method
    """
    Guessing state of the server.

    In this state, the player(s) have time to guess after someone buzzered.

    ## Behaviour
    State `Guessing` is entered when some player uses the buzzer.
    It transitions to state `Solving` if all players want to continue or if guessing_time_limit
    passes with guessing_time_limit_hard being enabled.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext', player: Optional[Player]):
        """
        Constructs a new instance for the given context.

        The player who buzzered is given by `player`.
        It is `None` in 'no buzzers' mode.
        """

        super().__init__(context)
        assert player is not None or config.Gameplay.no_buzzers()
        if player is not None:
            logger.info(f"{str(player)} is guessing...")
        else:
            logger.info("Giving all players time to guess...")

        self.__guesser: Optional[Player] = player
        if player is not None:
            self._send_to_ingame_players(ENTER_GUESSING(player.get_name(), player.get_id()))
        else:
            self._send_to_ingame_players(ENTER_NEUTRAL_GUESSING())
        self.__time_limit_start: Optional[float] = None
        if config.Gameplay.guessing_time_limit() is not None:
            logger.info(f"Starting time limit of {config.Gameplay.guessing_time_limit()}s")
            self._send_to_ingame_players(START_TIME_LIMIT(config.Gameplay.guessing_time_limit()))
            self.__time_limit_start = time.time()

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Guessing"

    @staticmethod
    def _state_can_continue() -> bool:
        return True

    def _continue(self):
        logger.info("Finished guessing...")
        if config.Gameplay.no_buzzers():
            # Import here to prevent circular imports
            from .searching import Searching
            self.context().transition(Searching(self.context()))
        else:
            # Import here to prevent circular imports
            from .solving import Solving
            self.context().transition(Solving(self.context(), self.__guesser))

    def poll(self):
        # Poll time limit
        if self.__time_limit_start is not None and config.Gameplay.guessing_time_limit_hard():
            if time.time() - self.__time_limit_start > config.Gameplay.guessing_time_limit():
                logger.info("Continuing due to time limit...")
                if config.Gameplay.no_buzzers():
                    # Import here to prevent circular imports
                    from .searching import Searching
                    self.context().transition(Searching(self.context()))
                else:
                    # Import here to prevent circular imports
                    from .solving import Solving
                    self.context().transition(Solving(self.context(), self.__guesser))

    # No handling of progress updates required
    # No special reaction to a player disconnecting required
    # Players are not rehabilitated in this state

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected
    # No point assigning expected
    # Updates in continuing players not relevant
