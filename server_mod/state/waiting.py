# pylint: disable=cyclic-import

from typing import TYPE_CHECKING

from protocol.messages import ENTER_LOBBY
from server_mod.player import Player
from misc import logger

from .game_state import GameState, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext

class Waiting(GameState): # pylint: disable=abstract-method
    """
    Waiting state of the server.

    In this state, the server waits for players to connect and starts a game with these players if
    all players declare to be ready. It is the default state that the server will initially go into
    and return to if a game ends or is stopped due to all players disconnecting.

    ## Behaviour
    Players can join immediately.
    They can also disconnect without being remembered as they hold no relevant state when no game is
    running.
    State `InitDownloading` is entered as soon as at least one player is connected and all players
    wish to start the game.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext'):
        super().__init__(context)
        logger.info("Entering waiting state...")

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Waiting"

    @staticmethod
    def _state_can_continue() -> bool:
        return True

    def _continue(self):
        # Import here to prevent circular imports
        from .init_downloading import InitDownloading
        self.context().transition(InitDownloading(self.context()))

    # No polling required
    # No reaction on progress updates required
    # No reaction to a player disconnecting required
    # No player rehabilitation required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected
    # No point assigning expected
    # Updates in continuing players not relevant

    ### Overridden methods ###

    # override as this is the only state where players really join the game and not the queue
    def player_join(self, player: Player):
        if player not in self.context().get_ingame_players():
            logger.info(f"{str(player)} enters lobby")
            self.context().get_ingame_players().append(player)
            player.send_message(ENTER_LOBBY())
            self._send_player_states()
        else:
            logger.warning(f"{str(player)} tried to enter lobby but already is in lobby")

    # override, disconnecting players do not have to be remembered due to having no relevant state
    def _remove_player_from_game(self, player: Player):
        logger.info("Player removed from waiting players")
        self.context().get_ingame_players().remove(player)
        self._send_player_states()
        self._check_for_continue()
