# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Callable, Any, List

from protocol.messages import SET_GLOBAL_PROGRESS, SET_PLAYER_STATES, WAIT_FOR_REHABILITATION, \
                                RESET, ENTER_LOBBY, WAIT_FOR_NEXT_GAME
from server_mod.player import Player
from misc import logger
from misc.player_state import PlayerState, ConnectionState
from misc.song import SongStatus

if TYPE_CHECKING:
    from .game_context import GameContext

def _current_players_only(func: Callable[['GameState', Player, Any], None]):
    """
    Decorator that ensures that a function is only executed for players that are in the current
    game.

    The decorated function must be a method of `GameState` or a subclass and take an instance of
    `Player` as its first argument.
    If the player is not in the current game, the decorated function is not called and a warning is
    printed instead.
    The decorated function must not have any return value
    """

    def inner(self: 'GameState', player: Player, *args):
        assert (isinstance(self, GameState) and isinstance(player, Player))
        if player in self.context().get_ingame_players():
            func(self, player, *args)
        else:
            logger.warning(f"{str(player)} does not participate in the current game but tried to "
                            + "access it")
    return inner

def _game_state_constructor(func: Callable[['GameState', 'GameContext', Any], Any]):
    """
    Decorator for all server-side game state constructors (excluding constructor of abstract super-
    class) that ensures correct chaining of state transitions.

    The decorated function must take an instance of `GameContext` as its first argument.

    Note that naively calling a transition inside a constructor would cause problems:
    ```
    self.context().transition(A())
    class A:
        def __init__(self):
            ...
            self.context().transition(B())
    ```
    would construct `A` and then set is as the new state, but constructing `A` itself constructs `B`
    and sets it as new state. While this itself may be intended, it will lead to the game going
    to state `A` because the transition to `B` happens first, then the transition to `A` in this
    execution order.

    This is fixed by keeping track of the current 'transition stack' size so that if a transition to
    `B` is called inside of another transition to `A`, only a transition to `B` is executed while
    the construction of `A` will only apply its internal affects but never be registered as new
    state.
    """

    def inner(self: 'GameState', context: 'GameContext', *args, **kwargs) -> Any:
        assert isinstance(self, GameState)
        from .game_context import GameContext
        assert isinstance(context, GameContext)
        context.start_transition()
        return func(self, context, *args, **kwargs)
    return inner

# pylint: disable=pointless-string-statement
"""
### Default state transition diagram ###

                                        ___________
                                        | Waiting | --------+ p_join, p_continue
                                        |_________| <-------+
                                             |
                                             | [all continue]
                                             v
                                        ___________________
                                        | InitDownloading | ------------+ p_progress_update
                                        |_________________| <-----------+
                                             |
                                             | [all progressed to lookahead or further]
                                             v
                                  +-->  _____________  
                                  |     | Searching | ------------+ p_song_index_reply
                                  |     |___________| <-----------+
                                  |          |
                                  |          + -----------------------------------------------------+----------------------------------------------+
                                  |          |                                                      |                                              |
                                  |          | [all ready for current index, first or long search]  |                                              |
                                  |          v                                                      |                                              |
                                  |     ___________                                                 |                                              |
                                  |     | Pausing | -----------+ p_continue                         | [all ready for current index, short search]  |
                                  |     |_________| <----------+                                    |                                              |
                                  |          |                                                      |                                              |
                                  |          | [all continue]                                       |                                              |
                                  |          v                                                      |                                              |
                                  |     _____________  <--------------------------------------------+                                              | 
                                  |     | Listening | ---------+ p_continue                                                                        |
                                  |     |___________| <--------+                                                                                   |
[all continue, limit not reached] |          |                                                                                                     |
                                  |          +------------------------+                                                                            |
                                  |          |                        |                                                                            |
                                  |          | [buzzer via polling]   |                                                                            | [no more songs]
                                  |          v                        | [all continue or time limit]                                               |
                                  |     ____________                  |                                                                            |
                                  |     | Guessing | ---+ p_continue  |                                                                            |
                                  |     |__________| <--+             |                                                                            |
                                  |          |                        |                                                                            |
                                  |          | [all continue or time] |                                                                            |
                                  |          v                        |                                                                            |
                                  |     ___________  <----------------+                                                                            |
                                  |     | Solving | ---------+ p_continue, p_assign_points                                                         |
                                  |     |_________| <--------+                                                                                     |
                                  |          |                                                                                                     |
                                  +----------+                                                                                                     |
                                             |                                                                                                     |
                                             | [all continue, limit reached]                                                                       |
                                             v                                                                                                     |
                                        __________  <----------------------------------------------------------------------------------------------+
                                        | Ending |
                                        |________|



### State transition diagram for 'no_buzzers' mode ###

                             ___________
                             | Waiting | --------+ p_join, p_continue
                             |_________| <-------+
                                  |
                                  | [all continue]
                                  v
                             ___________________
                             | InitDownloading | ------------+ p_progress_update
                             |_________________| <-----------+
                                  |
                                  | [all progressed to lookahead or further]
                                  v
                       +-->  _____________  
                       |     | Searching | ------------+ p_song_index_reply
                       |     |___________| <-----------+
                       |          |
                       |          + -----------------------------------------------------+----------------------------------------------+
                       |          |                                                      |                                              |
                       |          | [all ready for current index, first or long search]  |                                              |
                       |          v                                                      |                                              |
                       |     ___________                                                 |                                              |
                       |     | Pausing | -----------+ p_continue                         | [all ready for current index, short search]  |
                       |     |_________| <----------+                                    |                                              |
                       |          |                                                      |                                              |
[all continue or time] |          | [all continue]                                       |                                              |
                       |          v                                                      |                                              |
                       |     _____________  <--------------------------------------------+                                              |
                       |     | Listening | ---------+ p_continue                                                                        |
                       |     |___________| <--------+                                                                                   | [no more songs]
                       |          |                                                                                                     |
                       |          | [all continue or time limit]                                                                        |
                       |          v                                                                                                     |
                       |     ____________                                                                                               |
                       |     | Guessing | ---+ p_continue                                                                               |
                       |     |__________| <--+                                                                                          |
                       |          |                                                                                                     |
                       +----------+                                                                                                     |
                                                                                                                                        |
                                                                                                                                        |
                                                                                                                                        |
                             ___________  <---------------------------------------------------------------------------------------------+
                             | Solving | ---------+ p_continue, p_assign_points, all continue
                             |_________| <--------+
                                  |
                                  | [all continue,  limit reached]
                                  v
                             __________
                             | Ending |
                             |________|
"""

class GameState:
    """Superclass of all states of the server"""

    def __init__(self, context: 'GameContext'):
        """
        Constructs a new instance for the given context.

        This constructor must be called by the constructors of all subclasses.
        """

        self.__context = context
        self._reset_continuing_players()
        # as e.g. buzzering can transition to next state without all players continuing

    ### Abstract methods (some with default behaviour) ###

    @staticmethod
    def get_state_name() -> str:
        """Returns the name of the state"""
        raise NotImplementedError()

    @staticmethod
    def _state_can_continue() -> bool:
        """Returns whether the state allows continuing through continue commands by all players"""
        return False

    def _continue(self):
        """Method to call if all players continue, only to be implemented if `state_can_continue()`
            returns `True`"""
        raise NotImplementedError()

    def poll(self):
        """Polling handler for e.g. timed events"""

    def _progress_update(self, progress: float):
        """Handle a change of the current download progress"""

    def _handle_player_disconnect(self):
        """Handle a change of the current players where one player disconnects from the current
            game"""

    def _rehabilitate(self, player: Player):
        """
        Rehabilitate the given player into the currently running game.

        Only required for subclasses that can call `_try_rehabilitation()`.
        """
        raise NotImplementedError()

    ### Further methods ###

    def context(self) -> 'GameContext':
        """Returns the context of the state"""
        return self.__context

    def _reset(self):
        """
        Resets the internal state after a game finished.
        Automatically re-enters the `Waiting` state.
        Use this method instead of manually transitioning to the `Waiting` state.

        All currently active players are notified so that they know they can enter the lobby again.
        Then, they are removed from the state until they join the lobby again on their own.
        Reconnecting players are automatically added to the lobby.
        """

        self.context().get_disconnected_players().clear()

        for player in self.context().get_ingame_players():
            player.send_message(RESET())
            player.reset()
        self.context().get_ingame_players().clear()

        for player in self.context().get_reconnecting_players():
            logger.info(f"{str(player)} enters lobby")
            player.send_message(RESET())
            player.reset()
            self.context().get_ingame_players().append(player)
            player.send_message(ENTER_LOBBY())
        self._send_player_states()
        self.context().get_reconnecting_players().clear()

        for player in self.context().get_queued_players():
            logger.info(f"{str(player)} enters lobby")
            self.context().get_ingame_players().append(player)
            player.send_message(ENTER_LOBBY())
        self._send_player_states()
        self.context().get_queued_players().clear()

        self.context().get_song_list().clear()
        self.context().reset_next_song_index()

        # Import here to prevent circular imports
        from .waiting import Waiting
        self.context().transition(Waiting(self.context()))

    def _send_to_ingame_players(self, message: dict, timed = False):
        """Sends the given message to all players currently ingame, optionally send as a timed
            message for ping compensation"""
        for player in self.context().get_ingame_players():
            if timed:
                player.send_timed_message(message)
            else:
                player.send_message(message)

    def _check_for_continue(self) -> bool:
        """
        Checks whether all players want to continue and calls `_continue()` if they do.

        Returns `True` if and only if all players want to continue.
        """
        if len(self.context().get_ingame_players()) > 0 \
            and all(p.get_continue() for p in self.context().get_ingame_players()):
            logger.info("Continue...")
            self._reset_continuing_players()
            self._continue()

    def _reset_continuing_players(self):
        """Sets all players to not wanting to continue"""
        for player in self.context().get_ingame_players():
            player.set_continue(False)
        self._send_player_states()

    def _remove_player_from_game(self, player: Player):
        """
        Removes the given player who is ingame from the game.

        Overridden by the `Waiting` and `Ending` states as it does not require to keep the state of
        disconnecting players.
        """

        logger.info("Removing player from current game but save its state for potential reconnect")
        self.context().get_ingame_players().remove(player)
        self.context().get_disconnected_players().append(player)

        # No remaining players?
        if len(self.context().get_ingame_players()) == 0:
            logger.warning(
                "All players disconnected from current game, returning to waiting state...")
            self._reset()
            return

        self._send_player_states()
        # For corner case where player leaves and all others have downloaded all songs in init
        # downloading
        progress = min(p.get_progress() for p in self.context().get_ingame_players())
        self._send_to_ingame_players(SET_GLOBAL_PROGRESS(progress))

        if self._state_can_continue():
            if self._check_for_continue():
                return  # If continuing is possible, do so instead of state transitions or similar
                        # by following calls
        self._handle_player_disconnect()
        self._progress_update(progress)

    def _send_player_states(self):
        """Collects and sends the states of all players (name, score, continue, ...) to all players
            currently ingame"""
        states: List[PlayerState] = []
        for p in self.context().get_ingame_players():
            states.append(PlayerState(p.get_name(), p.get_id(), p.get_score(), p.get_continue(),
                                      ConnectionState.CONNECTED))
        for p in self.context().get_reconnecting_players():
            states.append(PlayerState(p.get_name(), p.get_id(), p.get_score(), p.get_continue(),
                                      ConnectionState.RECONNECTING))
        for p in self.context().get_disconnected_players():
            states.append(PlayerState(p.get_name(), p.get_id(), p.get_score(), p.get_continue(),
                                      ConnectionState.DISCONNECTED))

        self._send_to_ingame_players(SET_PLAYER_STATES(states))

    def _try_rehabilitation(self) -> bool:
        """Tries to rehabilitate reconnecting players into a running game and returns `True` if at
            least one player was rehabilitated"""
        if len(self.context().get_reconnecting_players()) > 0:
            logger.info("Rehabilitating reconnecting player(s) into running game...")
            for player in self.context().get_reconnecting_players():
                logger.info(f"Rehabilitating player {str(player)}")
                self.context().get_ingame_players().append(player)
                self._rehabilitate(player)
            self.context().get_reconnecting_players().clear()

            self._send_player_states()
            progress = min(p.get_progress() for p in self.context().get_ingame_players())
            self._send_to_ingame_players(SET_GLOBAL_PROGRESS(progress))
            self._progress_update(progress)

            return True
        return False

    ### Abstract methods for handling incoming messages from a player ###

    def player_song_index_reply(self, player: Player, status: SongStatus, index: int,
                                length: float):
        # pylint: disable=unused-argument
        """Player replies to song index request"""
        logger.warning(f"Ignored unexpected song index reply from {str(player)}")

    def player_assign_points(self, player: Player, target_player_id: int, points: int):
        # pylint: disable=unused-argument
        """Player assigns points to some player"""
        logger.warning(f"Ignored unexpected assign points from {str(player)}")

    ### Abstract methods to handle incoming messages from a player ###
    ### after the `GameState` super class already did some handling ###

    def _player_continue(self):
        """
        Player sets their continue wish being in the given state.

        This is used to toggle actions like time limits etc.
        """

    ### Non-abstract methods for handling incoming messages from a player ###

    def player_join(self, player: Player):
        """Player tries to join a lobby"""
        logger.warning(f"Player {str(player)} wanted to join while game is already running, added "
                       + "to the next game queue")
        self.context().get_queued_players().append(player)
        player.send_message(WAIT_FOR_NEXT_GAME())

    def player_progress_update(self, player: Player, progress: float):
        """Player gives their current download progress"""
        if player in self.context().get_ingame_players():
            # Global progress is minimal progress among all players
            # Determine minimal progress of other players; Sufficiently high default value if there
            # are no other players
            old_progress_others = min(
                (p.get_progress() for p in self.context().get_ingame_players() if p != player),
                default = 2**20)
            # Determine progress before the current update
            old_progress = min(old_progress_others, player.get_progress())

            player.set_progress(progress)
            new_progress = min(old_progress_others, progress)
            # Only broadcast global progress if it changed
            if new_progress != old_progress:
                self._send_to_ingame_players(SET_GLOBAL_PROGRESS(new_progress))

            # Let specific state handle progress update if necessary
            self._progress_update(new_progress)
        elif player in self.context().get_reconnecting_players():
            # Silently update progress of reconnecting players who already are downloading.
            player.set_progress(progress)
        else:
            pass # Players could still update in ending state, just ignore.

    @_current_players_only
    def player_continue(self, player: Player, continue_: bool, state: str):
        """Player sets their continue wish being in the given state"""
        if self.get_state_name() == state:
            assert self._state_can_continue()
            player.set_continue(continue_)
            self._send_player_states()
            if not self._check_for_continue():
                self._player_continue() # local update without state being transitioned anyways
        else:
            logger.warning(f"Ignored unexpected continue from {str(player)} for state {state} "
                            + f"while being in state {self.get_state_name()}")

    def player_disconnect(self, player: Player):
        """Player is disconnected from the game"""
        if player in self.context().get_ingame_players():
            logger.info(f"Removing {str(player)} from the current game...")
            self._remove_player_from_game(player)
        elif player in self.context().get_reconnecting_players():
            logger.info(f"Removing {str(player)} from reconnecting players...")
            self.context().get_reconnecting_players().remove(player)
            self.context().get_disconnected_players().append(player)
            self._send_player_states()
        elif player in self.context().get_queued_players():
            logger.info(f"Removing {str(player)} from queued players...")
            self.context().get_queued_players().remove(player)
        else:
            logger.warning(f"Unexpected player disconnect from {str(player)} who is neither "
                            + "connected nor reconnecting to the game")

    def player_reconnect(self, player: Player, old_player: Player):
        """Reconnects `player` to replace previously disconnected `old_player`"""
        logger.info("Player was connected previously and is reconnecting")
        assert old_player in self.context().get_disconnected_players()

        self.context().get_disconnected_players().remove(old_player)
        player.reconnect(old_player)
        self.context().get_reconnecting_players().append(player)
        self._send_player_states()
        player.send_message(
            WAIT_FOR_REHABILITATION([s.to_dict() for s in self.context().get_song_list()]))
