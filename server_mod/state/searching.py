# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Optional
import time

from protocol.messages import REQUEST_SONG_INDEX
from server_mod.player import Player
from config import config
from misc import logger
from misc.song import SongStatus

from .game_state import GameState, _current_players_only, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext


class Searching(GameState): # pylint: disable=abstract-method
    """
    Searching state of the server.

    In this state, the game searches for the next song to play.
    To this end, the next song that is available to all players is searched for.

    ## Behaviour
    State `Searching` is entered if a song is to be played to the players.
    It requests the status of the next song in the list of all songs.

    If the download failed for at least one player, the status of the following song is requested
    recursively.
    If the download has not failed but some player still is downloading, the game remains in state
    `Searching`.
    The game transitions to state `Listening` if a song to play was found fast or to state `Pausing`
    if a song was found but the search took more than some timeout.
    `Pausing` is also entered if specifically requested (e.g. after initial downloading) or if a
    player reconnected.
    In the special case where failed downloads appear until no songs remain, state `Ending` is
    entered (except for 'no buzzers' mode where state 'Solving' is entered).

    Upon entering the state, the game tries to rehabilitate reconnecting players.

    Continuing players are used here to declare which players have the next song index prepared.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext', force_pause: bool = False):
        """
        Constructs a new instance for the given context.

        If `force_pause` is set to `True`, the state transitions to `Pausing` if it would transition
        to `Listening` otherwise.
        """

        super().__init__(context)
        logger.info("Searching for next song...")

        if self._try_rehabilitation():
            force_pause = True # Must pause if a player was rehabilitated
        self.__force_pause = force_pause
        self.__song_length: Optional[float] = None
        self.__begin_of_search = time.time()
        self.__request_song_index()

    def __request_song_index(self):
        """Request the song at the next index from all players"""
        self.__song_length = None
        if self.context().get_next_song_index() >= len(self.context().get_song_list()):
            # No songs remain
            # Import here to prevent circular imports
            if config.Gameplay.no_buzzers():
                from .solving import Solving
                self.context().transition(Solving(self.context(), None))
            else:
                from .ending import Ending
                self.context().transition(Ending(self.context()))
        else:
            logger.info(f"Requesting song at index {self.context().get_next_song_index()}")
            self._reset_continuing_players()
            self._send_to_ingame_players(
                REQUEST_SONG_INDEX(self.context().get_next_song_index()))

    def __check_for_continue_to_next_song(self):
        """Checks if all players have the next song index ready and advances the game state if they
            are"""
        if all(p.get_continue() for p in self.context().get_ingame_players()):
            self._reset_continuing_players()
            elapsed = time.time() - self.__begin_of_search
            logger.info(f"Found new song at index {self.context().get_next_song_index()} "
                        + f"after {elapsed : .3f}s")
            song_index = self.context().get_next_song_index()
            self.context().increment_next_song_index()
            if self.__force_pause or elapsed > config.Timeouts.search_length_to_pause():
                # Import here to prevent circular imports
                from .pausing import Pausing
                self.context().transition(Pausing(self.context(), song_index, self.__song_length))
            else:
                # Import here to prevent circular imports
                from .listening import Listening
                self.context().transition(Listening(self.context(), song_index, self.__song_length))

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Searching"

    # No continue possible
    # No continue possible
    # No polling necessary
    # No handling of progress updates required

    def _handle_player_disconnect(self):
        self.__check_for_continue_to_next_song() # Game could wait for disconnecting player

    def _rehabilitate(self, player: Player):
        pass # rehabilitation at start of the state, so new player will receive state update

    ### Implementation of abstract methods for handling incoming messages from a player ###

    @_current_players_only
    def player_song_index_reply(self, player: Player, status: SongStatus, index: int,
                                length: float):
        if self.context().get_next_song_index() == index:
            # ignore older messages from previous song indexes
            logger.info(f"Song index {index} status {status.name} for {str(player)}")

            if status == SongStatus.READY:
                player.set_continue(True)
                if self.__song_length is None:
                    self.__song_length = length
                elif self.__song_length != length:
                    logger.warning(f"Unequal song lengths {self.__song_length} and {length}")
                    self.__song_length = min(self.__song_length, length)
                self.__check_for_continue_to_next_song()

            elif status == SongStatus.WAITING:
                pass # player will respond when ready

            else:
                logger.warning(f"Skipping song at index {index}")
                self.context().increment_next_song_index()
                self.__request_song_index()

    # No point assigning expected
    # Updates in continuing players not relevant
