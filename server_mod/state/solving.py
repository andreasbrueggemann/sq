# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Optional
import random

from protocol.messages import ENTER_SOLVING, SET_POINT_ASSIGNMENT, ENTER_SPECIFIC_SONG_SOLVING
from server_mod.player import Player
from config import config
from misc import logger

from .game_state import GameState, _current_players_only, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext


class Solving(GameState): # pylint: disable=abstract-method
    """
    Solving state of the server.

    In this state, title, musicians and further information about a previously played song are
    revealed to the players.
    If somebody buzzered previously, this person can assign point to the players.

    ## Behaviour
    State `Solving` is entered after all players continue in state `Listening` (skip song) or after
    all players continue from state `Guessing`.
    If the transition comes from state `Guessing`, the player who buzzered can assign points to all
    players.
    If the assigning player disconnects, a random other player is chosen to assign points.
    After all players choose to continue, the state transitions to `Ending` if the score limit is
    reached or to `Searching` otherwise.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext', solver: Optional[Player]):
        """
        Constructs a new instance for the given context.

        The previously buzzering player is given as `solver` which is set to `None` if no one
        buzzered.
        """

        super().__init__(context)
        logger.info("Solving...")

        if config.Gameplay.no_buzzers():
            # Try rehabilitation, but only in 'no buzzers' mode as otherwise, song continues from
            # where it was in the listening state where the player potentially was not connected
            self._try_rehabilitation()

        self.__solver = solver
        self.__substitute: Optional[Player] = None
        """Player substituting the solver as assigner if the solver disconnected"""
        self.__added_points = {}
        """Mapping from player ids to score to add"""

        if self.__solver is not None and not self.__solver in self.context().get_ingame_players():
            # Solver lost connection
            logger.info("Need to find substitute to assign points")
            self.__assign_substitute_assigner()
        elif config.Gameplay.no_buzzers():
            # There is no solver due to 'no buzzers' mode, here start solving for a specific song
            logger.info("No-buzzers mode solving")
            self.__substitute = random.choice(self.context().get_ingame_players())
            next_index = self.context().pop_played_song_index()
            if next_index is None: # all solutions were given
                from .ending import Ending
                self.context().transition(Ending(self.context()))
            else:
                self._send_to_ingame_players(
                    ENTER_SPECIFIC_SONG_SOLVING(next_index, self.__substitute.get_id(), True))
        else:
            # never in 'no buzzers' mode
            self._send_to_ingame_players(
                ENTER_SOLVING(None if solver is None else solver.get_id(), False))

    def __assign_substitute_assigner(self):
        """Assigns a substitute assigner of a player buzzered but disconnected or if a previously
            chosen substitute also disconnected"""
        self.__substitute = random.choice(self.context().get_ingame_players())
        self._send_to_ingame_players(ENTER_SOLVING(self.__substitute.get_id(), True))

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Solving"

    @staticmethod
    def _state_can_continue() -> bool:
        return True

    def _continue(self):
        # Import here to prevent circular imports
        from .searching import Searching

        logger.info("Continuing from solving...")

        # Apply assigned points
        for player in self.context().get_all_players():
            if player.get_id() in self.__added_points:
                player.set_score(player.get_score() + self.__added_points[player.get_id()])
        self._send_player_states()

        if config.Gameplay.no_buzzers():
            self.context().transition(Solving(self.context(), None))
        else:
            # Check if score limit reached and transition
            highest_score = max(p.get_score() for p in self.context().get_all_players())
            if highest_score >= config.Gameplay.score_to_reach():
                # Import here to prevent circular imports
                from .ending import Ending
                self.context().transition(Ending(self.context()))
            else:
                self.context().transition(Searching(self.context()))

    # No polling necessary
    # No handling of progress updates required

    def _handle_player_disconnect(self):
        if self.__substitute is None:
            if self.__solver is not None and \
                    not self.__solver in self.context().get_ingame_players():
                self.__assign_substitute_assigner()
        else:
            if not self.__substitute in self.context().get_ingame_players():
                self.__assign_substitute_assigner()

    def _rehabilitate(self, player: Player):
        pass # rehabilitation at start of the state, so new player will receive state update

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected

    @_current_players_only
    def player_assign_points(self, player: Player, target_player_id: int, points: int):
        if self.__solver is None and not config.Gameplay.no_buzzers():
            logger.warning(f"{str(player)} assigned points to a player after song was skipped")
            return
        if self.__solver != player and (self.__substitute is None or self.__substitute != player):
            logger.warning(f"{str(player)} assigned points to a player but is not the player who "
                            + "shall assign points")
            return
        if points < 0 or points > config.Gameplay.maximum_score_per_song():
            logger.warning(f"{str(player)} assigned {points} points while only points in"
                           + f"[0,{config.Gameplay.maximum_score_per_song()}] are allowed")
            return

        logger.info("Points assigned")
        if target_player_id in self.__added_points:
            if points == 0:
                self.__added_points.pop(target_player_id)
            else:
                self.__added_points[target_player_id] = points
        else:
            if points != 0:
                self.__added_points[target_player_id] = points
        self._send_to_ingame_players(SET_POINT_ASSIGNMENT(self.__added_points))

        # Reset after each point assignments so that all players have to agree to a given
        # point assignment
        self._reset_continuing_players()

    # Updates in continuing players not relevant
