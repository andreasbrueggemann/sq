# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Optional
import random
import time

from protocol.messages import PLAY_SONG, START_TIME_LIMIT
from config import config
from misc import logger

from .game_state import GameState, _game_state_constructor
if TYPE_CHECKING:
    from .game_context import GameContext

class Listening(GameState): # pylint: disable=abstract-method
    """
    Listening to song state of the server.

    In this state, a given song is played to all players until a player buzzers.

    ## Behaviour
    State `Listening` is entered when a song to play was found to start playing it.
    The state reacts to player buzzer events and transitions to state `Guessing` when buzzer events
    appear.
    Note that depending on "BuzzerSlack", this transition may not appear immediately.
    Alternatively, state `Solving` may be entered immediately if all players want to continue or 
    after the listening_time_limit passes.
    """

    @_game_state_constructor
    def __init__(self, context: 'GameContext', song_index: int, song_length: float):
        """
        Constructs a new instance for the given context.

        The parameters declare the index of the song to play and its length.
        """

        super().__init__(context)
        logger.info(f"Starting to play song at index {song_index}...")
        logger.info(f"Song length is {song_length : .3f}s")

        start = self.context().get_song_list()[song_index].get_start_timestamp()
        if start is None:
            start = 0.0
        if start >= song_length:
            logger.warning("Timestamp larger than song length")
            start = 0.0
        logger.info(f"Song start is {start : .3f}s")

        remaining_len = song_length - start
        if config.Gameplay.random_start():
            timestamp = start + random.random() * config.Gameplay.latest_start() * remaining_len
        else:
            timestamp = start
        logger.info(f"Starting at {timestamp : .3f}s")

        for player in self.context().get_ingame_players():
            player.reset_buzzer_timer()

        self.__first_buzzer_time: Optional[float] = None
        self.__time_limit_start: Optional[float] = None
        self._send_to_ingame_players(PLAY_SONG(song_index, timestamp), timed = True)
        self.context().add_song_index_as_played(song_index)
        if config.Gameplay.listening_time_limit() is not None and \
            config.Gameplay.listening_time_limit_skip_threshold() == 0.0: # start time limit now
            logger.info(f"Starting time limit of {config.Gameplay.listening_time_limit()}s")
            self._send_to_ingame_players(START_TIME_LIMIT(config.Gameplay.listening_time_limit()),
                                         timed = True) # also timed so that PLAY_SONG arrives first
            self.__time_limit_start = time.time()

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Listening"

    @staticmethod
    def _state_can_continue() -> bool:
        return True

    def _continue(self):
        if config.Gameplay.no_buzzers():
            logger.info("Continuing...")
            # Import here to prevent circular imports
            from .guessing import Guessing
            self.context().transition(Guessing(self.context(), None))
        else:
            logger.info("Skipping song...")
            # Import here to prevent circular imports
            from .solving import Solving
            self.context().transition(Solving(self.context(), None))

    def poll(self):
        # Poll buzzer actions and time limit

        # Check if time limit is over (do not transition now as buzzers may be waiting due to slack)
        time_limit_over = False
        if self.__time_limit_start is not None:
            if time.time() - self.__time_limit_start > config.Gameplay.listening_time_limit():
                time_limit_over = True

        # Search all players who buzzered (after ping compensation)
        buzzering_players = \
            [player for player in self.context().get_ingame_players() if player.poll_buzzer()]

        if len(buzzering_players) > 0: # There are buzzer actions
            current_time = time.time()
            if self.__first_buzzer_time is None:
                logger.info("Registered first buzzer action")
                self.__first_buzzer_time = current_time

            if current_time - self.__first_buzzer_time >= config.Gameplay.buzzer_slack() or \
                time_limit_over:
                # buzzer slack time delta exceeded or time limit exceeded
                # Action
                logger.info("Executing buzzer action...")
                logger.info(f"Buzzering players: {', '.join(str(p) for p in buzzering_players)}")
                if config.Gameplay.buzzer_slack_compensation_mode():
                    min_score = min(p.get_score() for p in buzzering_players)
                    eligible_players = [p for p in buzzering_players if p.get_score() == min_score]
                    logger.info(f"Choose among: {', '.join(str(p) for p in eligible_players)}")
                else:
                    eligible_players = buzzering_players
                buzzering_player = random.choice(eligible_players)
                logger.info(f"Selected player {str(buzzering_player)} to answer")
                # Import here to prevent circular imports
                from .guessing import Guessing
                self.context().transition(Guessing(self.context(), buzzering_player))
                return # No further handling due to potentially exceeded time limit required

        if time_limit_over:
            if config.Gameplay.no_buzzers():
                logger.info("Continuing due to time limit...")
                # Import here to prevent circular imports
                from .guessing import Guessing
                self.context().transition(Guessing(self.context(), None))
            else:
                logger.info("Skipping song due to time limit...")
                # Import here to prevent circular imports
                from .solving import Solving
                self.context().transition(Solving(self.context(), None))

    # No handling of progress updates required
    # No special reaction to a player disconnecting required
    # Players are not rehabilitated in this state

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # No song index replies expected
    # No point assigning expected

    def _player_continue(self):
        # Activate time limit if required
        if self.__time_limit_start is None and config.Gameplay.listening_time_limit() is not None:
            # not already activated, but time limit is enabled

            total = len(self.context().get_ingame_players())
            continuing = len([p for p in self.context().get_ingame_players() if p.get_continue()])
            # Threshold reached or in case 1.0 all but one player continuing
            if continuing / total >= config.Gameplay.listening_time_limit_skip_threshold() or \
                        (config.Gameplay.listening_time_limit_skip_threshold() == 1.0 and \
                         continuing + 1 == total):
                logger.info(f"Starting time limit of {config.Gameplay.listening_time_limit()}s as "
                            + "enough players wish to continue")
                self._send_to_ingame_players(
                    START_TIME_LIMIT(config.Gameplay.listening_time_limit()))
                self.__time_limit_start = time.time()
