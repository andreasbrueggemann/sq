from typing import Optional, Tuple, List
import time
from statistics import median
from random import randrange

from protocol.connection_handler import ConnectionHandler
from protocol.messages import PING
from misc import logger
from config import config

class Player:
    """Player object that manages the connection and hold the state of a player"""

    def __init__(self, connection_handler: ConnectionHandler):
        """Constructs a new instance for the given connection handler"""

        self.__connection_handler = connection_handler
        self.__connected = True

        self.__name: Optional[str] = None
        self.__id = randrange(1 << 60)

        self.__continue = False
        """Whether the player wishes to continue"""
        self.__score: int = 0

        self.__progress = 0.0
        """Download progress of the player, equals number of processed songs (success + fail)"""
        self.__buzzer_time: Optional[float] = None
        """Time when a buzzer should be registered, used for ping compensation. `None` if player did
            not buzzer."""
        self.__timed_messages: List[Tuple[dict, float]] = []
        """Timed messages `(message, time)` that shall be sent at time `time`"""

        self.__last_ping_start_time: Optional[float] = None
        self.__pings: List[float] = []
        self.__avg_ping: Optional[int] = None

    def __str__(self):
        """String representation of the player"""
        if self.__name is None:
            return f"{self.__connection_handler.get_address()[0]}:" \
                    + f"{self.__connection_handler.get_address()[1]}"
        else:
            return f"{self.__connection_handler.get_address()[0]}:" \
                    + f"{self.__connection_handler.get_address()[1]} " \
                    + f"({self.__name}/{str(self.__id)[-4:]})"

    def reconnect(self, old_player: 'Player'):
        """Lets `old_player` reconnect by the player by transferring the relevant state to `self`"""
        self.__name = old_player.get_name()
        self.__score = old_player.get_score()
        self.__id = old_player.get_id()

    def reset(self):
        """Resets the game relevant state of the player for a new game"""
        self.__continue = False
        self.__score = 0
        self.__progress = 0.0
        self.__buzzer_time = None
        self.__timed_messages.clear()

    def receive_timed_buzzer(self):
        """Registers a buzzer event for the player, ping remains to be compensated"""
        logger.info(f"Received buzzer event from {str(self)}")
        waiting_time = max(0, config.Network.ping_upper_bound() - self.__avg_ping) / 2000
        self.__buzzer_time = time.time() + waiting_time

    def poll_buzzer(self):
        """Returns whether the player buzzered and ping compensation delay was passed"""
        return self.__buzzer_time is not None and self.__buzzer_time <= time.time()

    def reset_buzzer_timer(self):
        """Resets the buzzer timer indicating that the player did not buzzer"""
        self.__buzzer_time = None

    ###############
    ### Network ###
    ###############

    def send_message(self, message: dict):
        """Sends the given message to the player by appending it to the player's output queue"""
        self.__connection_handler.add_outgoing_message(message)

    def send_timed_message(self, message: dict):
        """Schedules the given message to be sent by `poll_timed_message()` after ping
            compensation"""
        if self.__avg_ping is None:
            logger.warning("{}: Sending times message despite of unknown ping")
            ping = 0 # Strongest compensation results to the player's disadvantage
        else:
            ping = self.__avg_ping
        waiting_time = max(0, config.Network.ping_upper_bound() - ping) / 2000
        self.__timed_messages.append((message, time.time() + waiting_time))

    def poll_timed_messages(self):
        """Sends all timed messages where the ping compensation delay has already passed"""
        now = time.time()
        for message, message_time in self.__timed_messages:
            if message_time <= now:
                self.send_message(message)
        self.__timed_messages = [(message, message_time)
                        for (message, message_time) in self.__timed_messages if message_time > now]

    def is_connected(self) -> bool:
        """Returns whether player still is connected"""
        return self.__connected

    def disconnect(self):
        """Disconnects the player from the game"""
        self.__connection_handler.close()
        self.__connected = False

    def ping(self):
        """Start player ping measurement"""
        if self.__last_ping_start_time is None:
            self.__last_ping_start_time = time.time()
            self.send_message(PING())
        else:
            logger.warning(f"{str(self)} pinged when last ping still running")

    def ping_reply(self):
        """Client replied to a ping"""
        if self.__last_ping_start_time is not None:
            elapsed = time.time() - self.__last_ping_start_time
            self.__pings.append(elapsed)
            self.__last_ping_start_time = None
            if len(self.__pings) == config.Network.ping_count():
                self.__avg_ping = int(median(self.__pings) * 1000)
                logger.info(f"{str(self)} has average ping of {self.__avg_ping}ms")
            else:
                self.ping()
        else:
            logger.warning(f"{str(self)} replied to ping unexpected")

    ###########################
    ### Getters and Setters ###
    ###########################

    def get_id(self) -> int:
        """Returns the player's id"""
        return self.__id

    def get_name(self) -> str:
        """Returns the player's name"""
        return self.__name

    def set_name(self, name: str):
        """Sets the player's name"""
        self.__name = name

    def get_continue(self) -> bool:
        """Returns whether the player wants to continue"""
        return self.__continue

    def set_continue(self, continue_: str):
        """Sets whether the player wants to continue"""
        self.__continue = continue_

    def get_score(self) -> int:
        """Returns the current score of the player"""
        return self.__score

    def set_score(self, score: int):
        """Sets the current score of the player"""
        self.__score = score

    def get_progress(self) -> float:
        """Returns the download progress of the player, equals number of processed songs
            (success + fail)"""
        return self.__progress

    def set_progress(self, progress: float):
        """Sets the download progress of the player, equals number of processed songs (success +
            fail)"""
        self.__progress = progress
