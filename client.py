"""
Starter for the SQ client.

Besides of running the launcher and subsequently the client itself, this starter
can also receive CLI arguments to bypass the launcher.
Argparse is not used due to the minimality of given CLI functionality.

## Arguments
* None : Open the launcher to input connection parameters
* `default` : Connect to default ip and port using default username (see config)
* `[index: int>=0]` : As `default`, but using the given client index for testing
* `[server ip: str] [server port: int] [username: str]` : Connect to the given server
    using the given username
"""

import sys
import traceback
from typing import Optional

# Disable pygame hello message
import os
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "1" # pylint: disable=wrong-import-position

from config import config
from misc import logger

from client_mod.client import Client
from client_mod.tk.launcher import Launcher
from client_mod.tk.selftest import Selftest
from client_mod.tk.proactive_dl import ProactiveDL
from client_mod.tk.crash_reporter import CrashReporter
from client_mod.tk.info_dialog import InfoDialog
from client_mod.errors import CustomMessageError

def cli_connect(server_ip: Optional[str], server_port: int, username: str):
    """
    Connects to the given server using the given arguments when running SQ over CLI.

    This method automatically catches connection errors and logs appropriate messages.
    Otherwise, errors are not catched so that they are displayed in the terminal.
    """

    if server_ip is None:
        logger.error("No server IP address given")
        return

    try:
        Client(server_ip, server_port, username)
    except ConnectionRefusedError:
        logger.error("Cannot reach given server")
    except ConnectionError as con_error:
        logger.error(f"Lost connection to the server: {type(con_error).__name__}, {str(con_error)}")
    except CustomMessageError as custom_message:
        logger.error(str(custom_message))
    except Exception: # pylint: disable=broad-except
        logger.error(traceback.format_exc())

def cli_config_init(index_: int = 0):
    """Initializes the config outputting potential errors over CLI"""
    try:
        conf_ = config.Config(config.Type.CLIENT, index_)
        logger.init_configuration()
        conf_.test()
    except CustomMessageError as custom_message:
        logger.error(str(custom_message))
        sys.exit()

def selftest():
    """Runs the selftest if SQ runs for the first time using the current version"""
    if Selftest.is_necessary():
        selftest_ = Selftest()
        if not selftest_.has_succeeded():
            sys.exit()

if __name__ == "__main__":
    if len(sys.argv) == 1: # No connection info given, use launcher
        # As this is the case for users not using CLI at all, catch exceptions and show them in GUI
        # instead of letting them be printed into the terminal.
        try:
            conf = config.Config(config.Type.CLIENT)
            logger.init_configuration()
            conf.test()
            selftest()
            while True:
                launcher = Launcher() # blocks until further input is given
                if launcher.is_submitted(): # Otherwise, window manually closed, just exit
                    if launcher.is_proactive_download_chosen():
                        ProactiveDL()
                        break
                    elif launcher.is_selftest_chosen():
                        Selftest()
                        # then, run launcher again
                    else:
                        args = launcher.get_connection_info()
                        del launcher
                        Client(*args)
                        break
                else:
                    break
        except ConnectionRefusedError:
            MESSAGE = "Connection to the server not possible. Make sure that you are connected to "\
                        + "the server and that the server is online."
            logger.error(MESSAGE)
            InfoDialog(MESSAGE)
        except ConnectionError as error:
            message = f"Lost connection to the server: {type(error).__name__}, {str(error)}"
            logger.error(message)
            InfoDialog(message)
        except CustomMessageError as error:
            logger.error(str(error))
            InfoDialog(str(error))
        except Exception: # pylint: disable=broad-except
            logger.error(traceback.format_exc())
            CrashReporter(traceback.format_exc())
    elif len(sys.argv) == 2 and sys.argv[1] == "default":
        cli_config_init()
        if config.Gameplay.username() is not None: # Ip and port always given by client_default.ini
            selftest()
            cli_connect(config.Network.ip_address(),
                        config.Network.port(),
                        config.Gameplay.username())
        else:
            raise ValueError("Cannot start client using option 'default' when no default "
                                + "username is given")
    elif len(sys.argv) == 2 and str.isdigit(sys.argv[1]):
        index = int(sys.argv[1])
        cli_config_init(index)
        if config.Gameplay.username() is not None: # Ip and port always given by client_default.ini
            selftest()
            cli_connect(config.Network.ip_address(),
                        config.Network.port(),
                        config.Gameplay.username())
        else:
            raise ValueError("Cannot start client using index option when no default "
                                + "username is given")
    elif len(sys.argv) == 4:
        cli_config_init()
        selftest()
        cli_connect(sys.argv[1], int(sys.argv[2]), sys.argv[3])
    else:
        sys.exit("Unexpected arguments.\nValid arguments are:\n"
            + "  - None : Open the launcher to input connection parameters\n"
            + "  - `default` : Connect to default ip and port using default username (see config)\n"
            + "  - `[index: int>=0]` : As `default`, but using the given client index for testing\n"
            + "  - `[server ip: str] [server port: int] [username: str]` : Connect to the given "
            + "server using the given username")
