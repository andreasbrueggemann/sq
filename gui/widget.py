from typing import Tuple
import pygame

# Position is not defined on init, do not init to `None` to prevent clutter
class Widget: # pylint: disable=attribute-defined-outside-init
    """
    Abstract superclass for GUI widgets that have a rectangular shape.

    Note that after construction, position and dimensions should immediately be set before further
    usage.
    """

    def set_position_and_dimensions(self, x: float, y: float, width: float, height: float,
                                    centered: bool = False):
        """
        Sets the position and dimension of the widget.

        Note that it is required to call this function before rendering the widget or calling any
        other methods that depend on its position or dimensions.
        """
        # pylint: disable=too-many-arguments

        self._x = x
        self._y = y
        self._width = width
        self._height = height
        if centered:
            self._x -= width // 2
            self._y -= height // 2
        self._set_position_and_dimensions_handler()

    def _set_position_and_dimensions_handler(self):
        """Handler for when `set_position_and_dimensions()` was called that subclasses can use as
            custom reaction"""

    def get_center(self) -> Tuple[float, float]:
        """Returns the center position of the widget"""
        return (self._x + self._width / 2, self._y + self._height / 2)

    def get_bounding_rectangle(self) -> Tuple[float, float, float, float]:
        """Returns the top left corner position and dimensions of the widget,
            (x, y, width, height)"""
        return (self._x, self._y, self._width, self._height)

    ### Abstract methods ###

    def render(self, surface: pygame.Surface):
        """Renders the widget to the given surface"""
        raise NotImplementedError()
