from .widget import Widget

class InteractionWidget(Widget): # also abstract => pylint: disable=abstract-method
    """Abstract superclass for GUI widgets that have a rectangular shape and mouse interaction"""

    def __is_inside(self, x: int, y: int) -> bool:
        """Returns whether the given position `(x, y)` is inside the widget's boundaries"""
        return self._x <= x <= self._x + self._width and self._y <= y <= self._y + self._height

    def mouse_down(self, x: int, y: int):
        """Handler for mouse down event on the given position, not has to be inside the widget"""
        if self.__is_inside(x, y):
            self._press_action(x, y)

    def mouse_up(self, x: int, y: int):
        """Handler for mouse up event on the given position, not has to be inside the widget"""
        self._release_action(x, y)

    def mouse_movement(self, x: int, y: int):
        """Handler for mouse movement event to the given position, not has to be inside the
            widget"""
        self._movement_action(x, y)

    ### Abstract methods (not required to be implemented, only implement those that are needed) ###

    def _press_action(self, x: int, y: int):
        """Action to be executed if the mouse button is pressed on the widget and given position"""

    def _release_action(self, x: int, y: int):
        """Action to be executed if the mouse button is released on the widget and given position"""

    def _movement_action(self, x: int, y: int):
        """Action to be executed if the mouse moves to the given position"""
