from typing import Callable
import pygame

from config import theme

from .interaction_widget import InteractionWidget

class Slider(InteractionWidget):
    """
    A vertical slider widget calling an optional callback if its value is changed.

    The slider's value is in the interval `[0; 1]`.
    """

    def __init__(self, value: float):
        """Constructs a new instance with the given value"""
        self.__value = value
        self.__pressed = False
        self.__callback: Callable[[float], None] = None

    def set_callback(self, callback: Callable[[float], None]):
        """Sets the callback which is to be called when the input value changes and gets this value
            as argument"""
        self.__callback = callback

    def get_value(self) -> float:
        """Returns the current value `0.0 <= x <= 1.0`"""
        return self.__value

    ### Implementation of abstract methods ###

    def render(self, surface: pygame.Surface):
        pygame.draw.rect(surface, theme.Slider.background_color(), self.get_bounding_rectangle())
        pygame.draw.rect(surface, theme.Slider.border_color(), self.get_bounding_rectangle(), 3)

        # use width as height of the knob
        # with given widget height, the top of the know can only move for height - knob-height which
        # is the widget width.
        # use 1 - value so that 1.0 is at the top and 0.0 is at the bottom
        knob_rectangle = (self._x, self._y + (1 - self.__value) * (self._height - self._width),
                            self._width, self._width)

        pygame.draw.rect(surface,
                        theme.Slider.color_pressed() if self.__pressed else theme.Slider.color(),
                        knob_rectangle)
        pygame.draw.rect(surface, theme.Slider.border_color(), knob_rectangle, 3)

    def _press_action(self, x: int, y: int):
        self.__pressed = True

        # y - self._y is y relative to the widget
        # mouse should correspond to middle of the knob, thus subtract knob height / 2
        # normalize to [0; 1] and invert as top is max value
        self.__value = 1 - (y - self._y - self._width / 2) / (self._height - self._width)
        # enforce bounds
        self.__value = max(self.__value, 0.0)
        self.__value = min(self.__value, 1.0)

        if self.__callback is not None:
            self.__callback(self.__value)

    def _release_action(self, x: int, y: int):
        self.__pressed = False

    def _movement_action(self, x: int, y: int):
        if self.__pressed:
            self._press_action(x, y)
