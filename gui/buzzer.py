import pygame

from config import theme

from .toggle_button import ToggleButton

class Buzzer(ToggleButton):
    """
    A buzzer button that can be pressed to call a custom callback and then cannot be pressed again.

    The callback will be called if the button is pressed and always have its argument set to `True`.
    The buzzer can be set to active to be highlighted without influencing its behaviour.
    """

    def __init__(self):
        """Constructs a new instance"""
        super().__init__()
        self.__active = False

    def set_active(self, active: bool):
        """Sets whether the buzzer is active and shall be highlighted"""
        self.__active = active

    ### Implementation of abstract methods ###

    def render(self, surface: pygame.Surface):
        pygame.draw.rect(surface,
                            theme.Buzzer.background_color_active() if self.__active
                                else theme.Buzzer.background_color(),
                            self.get_bounding_rectangle())
        pygame.draw.circle(surface,
                            theme.Buzzer.color_pressed() if self._pressed else theme.Buzzer.color(),
                            self.get_center(),
                            min(self._width, self._height) / 2 * 0.9)

    def _press_action(self, x: int, y: int):
        if not self._pressed:
            self._pressed = True
            if self._callback is not None:
                self._callback(True)
