from typing import List, Tuple
import sys
import pygame

from config import theme
from misc import logger

from .align import Align

class Label():
    """Label widget that displays text"""

    def __init__(self, text: str, font_size: int, color: Tuple[int, int, int],
                 align: Align = Align.CENTER):
        """
        Creates a new label displaying the given text with the given parameters.

        The text may contain linebreaks (`\\n`).
        Note that after construction, the position should immediately be set before further usage.
        """

        self.__x = 0
        self.__y = 0
        self.__text = text
        self.__text_lines: List[pygame.Surface] = []
        self.__text_line_boxes: List[pygame.Rect] = []
        self.__font_size = font_size
        self.__color = color
        self.__align = align
        self.__font = pygame.font.SysFont(theme.Text.font_name(), self.__font_size)

    def set_position(self, x: float, y: float):
        """
        Sets the position of the widget to `(x, y)` which will be height-wise centered.

        Note that it is required to call this function before rendering the label or calling any
        other methods that depend on its position.
        """

        self.__x = x
        self.__y = y
        self.prepare_text()

    def set_text(self, text: str):
        """
        Displays the given text which may contain linebreaks (`\\n`).
        """

        self.__text = text
        if hasattr(self, "_Label__x"):
            self.prepare_text()

    def prepare_text(self):
        """
        Prepares the text to be displayed according to a previously set position.
        """

        lines = self.__text.split("\n")
        height = len(lines) * (self.__font_size + theme.Text.line_break_spacing()) \
                    - theme.Text.line_break_spacing()
        # lines are in space between self.__y - height / 2 and self.__y + height / 2
        y = self.__y - height / 2 + self.__font_size / 2 # y of first line

        self.__text_lines.clear()
        self.__text_line_boxes.clear()

        for line in lines:
            txt = self.__font.render(line, True, self.__color)
            self.__text_lines.append(txt)
            if self.__align == Align.LEFT:
                box = txt.get_rect(midleft = (self.__x, y))
            elif self.__align == Align.CENTER:
                box = txt.get_rect(center = (self.__x, y))
            elif self.__align == Align.RIGHT:
                box = txt.get_rect(midright = (self.__x, y))
            else:
                logger.error("Given align not supported by Label")
                sys.exit()
            self.__text_line_boxes.append(box)
            y += self.__font_size + theme.Text.line_break_spacing()

    def get_width(self) -> int:
        """
        Returns the width of the label
        """

        return max(self.__font.size(r)[0] for r in self.__text.split("\n"))

    def render(self, surface: pygame.Surface):
        """
        Renders the label to the given surface
        """

        for (text, box) in zip(self.__text_lines, self.__text_line_boxes):
            surface.blit(text, box)
