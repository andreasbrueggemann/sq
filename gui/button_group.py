from typing import List, Callable, Optional
import pygame

from .interaction_widget import InteractionWidget
from .toggle_button import ToggleButton

class ButtonGroup(InteractionWidget):
    """
    A horizontally aligned group of equally sized toggle-buttons where exactly one button is active
    at each time after one button has been activated.

    A callback can be set that is called if a previously inactive button is activated using the
    index of given button as parameter.
    """

    def __init__(self, labels: List[str]):
        """
        Constructs a new instance with buttons containing the given texts.

        The number of buttons thus equals the number of strings given as parameter.
        """

        self.__callback: Optional[Callable[[int], None]] = None
        self.__buttons: List[ToggleButton] = []
        for i, label in enumerate(labels):
            button = ToggleButton()
            button.set_text(label)
            # default args s.t. current values are used:
            def callback(button_pressed, button = button, i = i):
                if button_pressed:
                    # Inactivate other buttons
                    for bg_button in self.__buttons:
                        if bg_button != button:
                            bg_button.set_pressed(False)
                    # Callback
                    if self.__callback is not None:
                        self.__callback(i)
                else:
                    # Permit by setting back to pressed, can only be inactivated by selecting other
                    button.set_pressed(True)
            button.set_callback(callback)
            self.__buttons.append(button)

    def set_callback(self, callback: Callable[[int], None]):
        """Registers a callback function"""
        self.__callback = callback

    def set_selected(self, selected: int):
        """Sets the selection such that the button with the given index is selected"""
        for i, button in enumerate(self.__buttons):
            if i == selected:
                button.set_pressed(True)
            else:
                button.set_pressed(False)

    ### Implementation of abstract methods ###

    def _set_position_and_dimensions_handler(self):
        button_width = self._width / len(self.__buttons)
        for i, button in enumerate(self.__buttons):
            button.set_position_and_dimensions(
                self._x + i * button_width, self._y, button_width, self._height)

    def render(self, surface: pygame.Surface):
        for button in self.__buttons:
            button.render(surface)

    def _press_action(self, x: int, y: int):
        for button in self.__buttons:
            button.mouse_down(x, y)

    def _release_action(self, x: int, y: int):
        for button in self.__buttons:
            button.mouse_up(x, y)
