from gui.button import Button

class ToggleButton(Button):
    """
    A toggle-button that can be pressed to be turned on and off calling a custom callback.

    The callback will be called if the button is pressed and returns whether it was turned on
    (`True`) or off (`False`).
    """

    def is_pressed(self) -> bool:
        """Returns whether the toggle-button is turned on"""
        return self._pressed

    def set_pressed(self, pressed: bool):
        """Manually sets whether the toggle-button is turned on or off"""
        self._pressed = pressed

    ### Implementation of abstract methods ###

    def _press_action(self, x: int, y: int):
        self._pressed = not self._pressed
        if self._callback is not None:
            self._callback(self._pressed)

    def _release_action(self, x: int, y: int):
        pass # Prevent button release action from being called
