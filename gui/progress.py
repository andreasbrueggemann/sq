from .multi_progress import MultiProgress

class Progress(MultiProgress):
    """
    Progress bar to display a progress in the interval `[0.0; 1.0]`.

    The bar is divided into some number of segments.
    Using a single segment means that no segmentation is applied.
    """

    # Different arguments than MultiProgress as no ahead progress required
    def update_progress(self, progress: float): # pylint: disable=arguments-differ
        """Updates the progress according to the given value"""
        super().update_progress(progress, progress)
