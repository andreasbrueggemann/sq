import pygame

from misc import logger
from config import theme

from .widget import Widget

class MultiProgress(Widget):
    """
    Multi-progress bar to display a progress in the interval `[0.0; 1.0]`.

    The bar is divided into some number of segments.
    Using a single segment means that no segmentation is applied.
    Instead of setting and displaying a single progress, an "ahead-progress" is also set which is at
    least as high as the standard progress.
    This "ahead-progress" will then be displayed as grey part of the progress bar running ahead of
    the progress itself.
    """

    def __init__(self, segments: int = 1):
        """Constructs a new instance with the given number of segments"""
        self.__segments = segments
        self.__progress = 0.0
        self.__ahead_progress = 0.0

    def set_segments(self, segments: int):
        """Sets the number of segments to segment the progress bar into"""
        self.__segments = segments

    def update_progress(self, progress: float, ahead_progress: float):
        """Updates the progress according to the given values"""
        if ahead_progress < progress:
            logger.warning("Ahead-progress of MultiProgress below progress")
        self.__progress = progress
        self.__ahead_progress = ahead_progress

    ### Implementation of abstract methods ###

    def render(self, surface: pygame.Surface):
        # Draw ahead progress
        pygame.draw.rect(surface, theme.Progress.ahead_color(),
                            (self._x, self._y, self._width * self.__ahead_progress, self._height))
        # Overdraw ahead progress by progress
        pygame.draw.rect(surface, theme.Progress.color(),
                            (self._x, self._y, self._width * self.__progress, self._height))

        # Border and segmentation
        pygame.draw.rect(surface, theme.Progress.border_color(), self.get_bounding_rectangle(), 3)
        segment_width = self._width / self.__segments
        for i in range(1, self.__segments):
            pygame.draw.line(surface, theme.Progress.border_color(),
                                (self._x + i * segment_width, self._y),
                                (self._x + i * segment_width, self._y + self._height))
