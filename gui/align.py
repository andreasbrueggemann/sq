from enum import Enum

class Align(Enum):
    """Text Align"""

    LEFT = 1
    CENTER = 2
    RIGHT = 3
