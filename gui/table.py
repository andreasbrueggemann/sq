from typing import List, Tuple
import sys
import pygame

from config import theme
from misc import logger

from .align import Align
from .label import Label

class TableCell():
    """Wrapper for data describing a single table cell"""

    def __init__(self, content: str, max_content: str, color: Tuple[int, int, int]):
        """
        Creates data wrapper for one table cell containing text `content`, using at least the space
        required to display `max_content` and the given color (RGB).
        """

        self.content = content
        self.max_content = max_content
        self.color = color

class Table():
    """Table widget that arranges multiple labels as a table"""

    def __init__(self, font_size: int, align: Align = Align.LEFT):
        """
        Creates a new table using the given font size and alignment.

        Center alignment is not supported.
        Note that after construction, the position should immediately be set before further usage.
        """

        self.__x = 0
        self.__y = 0
        self.__font_size = font_size
        assert align != Align.CENTER
        self.__align = align
        self.__cells: List[List[Label]] = []
        self.__reference_width: List[int] = []

    def set_position(self, x: float, y: float):
        """
        Sets the position of the widget such that it is centered around `(x, y)`.

        Note that it is required to call this function before rendering the table or calling any
        other methods that depend on its position.
        """

        self.__x = x
        self.__y = y
        self.__do_formatting()

    def set_content(self, content: List[List[TableCell]]):
        """
        Sets the table content where cell `(i, j)` will contain the entry `(i, j)` of `content`.
        """

        self.__cells: List[List[Label]] = []
        self.__reference_width = []
        font = pygame.font.SysFont(theme.Text.font_name(), self.__font_size)
        for c_row in content:
            row: List[Label] = []
            for i, c_cell in enumerate(c_row):
                row.append(Label(c_cell.content, self.__font_size, c_cell.color, self.__align))
                if len(self.__reference_width) > i:
                    self.__reference_width[i] = max(self.__reference_width[i],
                                                 font.size(c_cell.max_content)[0])
                else:
                    self.__reference_width.append(font.size(c_cell.max_content)[0])
            self.__cells.append(row)

        if hasattr(self, "_Table__x"):
            self.__do_formatting()

    def __do_formatting(self):
        """
        Applies text formatting according to a previously set position.
        """

        if len(self.__cells) == 0:
            return

        height = len(self.__cells) * (self.__font_size + theme.Text.line_break_spacing()) \
                    - theme.Text.line_break_spacing()
        # lines are in space between self.__y - height / 2 and self.__y + height / 2
        y = self.__y - height / 2 + self.__font_size / 2 # y of first line

        if self.__align == Align.LEFT:
            col_iter = range(len(self.__cells[0]))
        elif self.__align == Align.RIGHT:
            col_iter = reversed(range(len(self.__cells[0])))
        else:
            logger.error("Given align not supported by Table")
            sys.exit()
        x = self.__x
        # first iter over columns (ltr or rtl depending on align), then over rows
        for j in col_iter:
            max_width = self.__reference_width[j]
            for i, row in enumerate(self.__cells):
                cell = row[j]
                cell.set_position(x, y + i * (self.__font_size + theme.Text.line_break_spacing()))
                max_width = max(max_width, cell.get_width())
            if self.__align == Align.LEFT:
                x += max_width + theme.Table.column_spacing()
            else: # self.__align == Align.RIGHT
                x -= max_width + theme.Table.column_spacing()

    def render(self, surface: pygame.Surface):
        """Renders the label to the given surface"""

        for row in self.__cells:
            for cell in row:
                cell.render(surface)
