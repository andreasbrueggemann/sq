from typing import Callable, Optional
import pygame

from config import theme

from .interaction_widget import InteractionWidget

class Button(InteractionWidget):
    """
    A simple button that can be pressed to call a custom callback.

    The callback will be called if the button is pressed and always have its argument set to `True`.
    """

    def __init__(self):
        """Constructs a new instance"""
        self.__text: Optional[pygame.Surface] = None
        self.__text_box: Optional[pygame.Rect] = None
        self._pressed = False
        self._callback: Optional[Callable[[bool], None]] = None

    def set_text(self, text: str):
        """Sets the optional text to be displayed on the button"""
        font = pygame.font.SysFont(theme.Button.font_name(), theme.Button.font_size())
        self.__text = font.render(text, True, theme.Button.text_color())
        if hasattr(self, "_x"):
            self.__text_box = self.__text.get_rect(center = self.get_center())

    def set_callback(self, callback: Callable[[bool], None]):
        """Registers a callback function"""
        self._callback = callback

    ### Implementation of abstract methods ###

    def _set_position_and_dimensions_handler(self):
        if self.__text is not None:
            self.__text_box = self.__text.get_rect(center = self.get_center())

    def render(self, surface: pygame.Surface):
        pygame.draw.rect(surface,
                            theme.Button.color_pressed() if self._pressed else theme.Button.color(),
                            self.get_bounding_rectangle())
        if self.__text is not None:
            surface.blit(self.__text, self.__text_box)

    def _press_action(self, x: int, y: int):
        self._pressed = True
        if self._callback is not None:
            self._callback(True)

    def _release_action(self, x: int, y: int):
        self._pressed = False
