"""
Starter for the SQ server.

## Arguments
* None : Run the server using the default ip and port (see config)
* `[ip: str] [port: int]` : Run the server using the given ip address and port
"""

import sys
import traceback

from config import config
from misc import logger

from server_mod.server import Server

if __name__ == "__main__":
    conf = config.Config(config.Type.SERVER)
    logger.init_configuration()
    conf.test()

    try:
        if len(sys.argv) == 1: # Use default from config
            Server(config.Network.ip_address(), config.Network.port())
        elif len(sys.argv) == 3:
            Server(sys.argv[1], int(sys.argv[2]))
        else:
            sys.exit("Unexpected arguments.\n Valid arguments are:\n"
                + "  - None : Use the default ip and port (see config)\n"
                + "  - `[ip: str] [port: int]` : Use the given ip address and port")
    except Exception: # pylint: disable=broad-except
        logger.error(traceback.format_exc())
