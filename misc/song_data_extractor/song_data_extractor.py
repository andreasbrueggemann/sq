from typing import Optional

from config import config
from .unsupported_url_error import UnsupportedUrlError

class SongDataExtractor:
    """
    Extractor for song data like a unique id and starting timestamp.

    Do not construct an extractor directly but call `get_instance()` with the corresponding song's
    URL instead. Then, an appropriate extractor for the song's URL will be chosen automatically if
    possible.
    """

    @staticmethod
    def get_instance(song_url: str) -> 'SongDataExtractor':
        """
        Returns a `SongDataExtractor` matching the given song URL or raises an `UnsupportedUrlError`
        if the URL is not supported by SQ or not allowed by the used config.

        Only use this method to construct new instances of `SongDataExtractor` and its subclasses.
        """

        # pylint: disable=cyclic-import
        from .youtube import Youtube # import here to prevent circular import
        if SongDataExtractor.__is_extractor_name_enabled(Youtube.get_name()):
            extractor = Youtube.try_get_instance(song_url)
            if extractor is not None:
                return extractor
        raise UnsupportedUrlError(song_url)

    @staticmethod
    def __is_extractor_name_enabled(extractor_name: str) -> bool:
        """Returns if the extractor with the given name is enabled by the used config"""
        return config.Database.enabled_extractors() is None or \
                extractor_name in config.Database.enabled_extractors()

    ### Abstract methods ###

    @staticmethod
    def try_get_instance(song_url: str) -> Optional['SongDataExtractor']:
        """
        Returns a `SongDataExtractor` for a specific subclass or `None` if the subclass does not
        match the given song URL or is not allowed by the used config.

        Only use this method to construct new instances of subclasses of `SongDataExtractor`.
        """
        raise NotImplementedError()

    @staticmethod
    def get_name() -> str:
        """Returns the extractor's name"""
        raise NotImplementedError()

    def get_start_timestamp(self) -> int:
        """Returns the start timestamp of the corresponding song (in seconds)"""
        raise NotImplementedError()

    def get_unique_id(self) -> str:
        """
        Returns a unique id of the corresponding song.

        This id is chosen such that it can be part of a filename by e.g. providing
        case-insensitivity.
        """
        raise NotImplementedError()
