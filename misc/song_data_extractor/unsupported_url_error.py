class UnsupportedUrlError(Exception):
    """Exception raised if an URL is not supported by SQ or not allowed by the used config"""

    def __init__(self, url: str):
        """Constructs a new instance for the given unsupported URL"""
        super().__init__(f"URL {url} is not supported by SQ or not allowed by the used config")
