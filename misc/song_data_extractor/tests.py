import unittest
import unittest.mock
import base64

from config import config
from .song_data_extractor import SongDataExtractor
from .unsupported_url_error import UnsupportedUrlError

class TestExtractors(unittest.TestCase):
    """Test case for all available extractors"""

    def test_youtube(self):
        """Test youtube extractor"""
        config.Config(config.Type.CLIENT)

        def test_valid_url(url: str, expected_video_id: str, expected_timestamp: int):
            """Tests if the results for the given valid youtube url match the given video_id and
                timestamp"""
            expected_unique_id = base64.b32encode(expected_video_id.encode('utf-8')).decode('utf-8')
            extractor = SongDataExtractor.get_instance(url)
            self.assertEqual(expected_unique_id, extractor.get_unique_id())
            self.assertEqual(expected_timestamp, extractor.get_start_timestamp())

        def test_invalid_timestamp(url: str, expected_video_id: str, timestamp: str):
            """Tests if the results for the given valid youtube url match the given video_id while
                the timestamp is malformed resulting in 0 being returned"""
            with self.assertLogs() as used_logger:
                expected_unique_id = base64.b32encode(expected_video_id.encode('utf-8')) \
                    .decode('utf-8')
                extractor = SongDataExtractor.get_instance(url)
                self.assertEqual(expected_unique_id, extractor.get_unique_id())
                self.assertEqual(0, extractor.get_start_timestamp())
                self.assertEqual(used_logger.output,
                            [f"WARNING:root:Unknown timestamp structure {timestamp} in url {url}"])

        def test_invalid_url(url: str):
            """Tests if the given invalid URL yields an exception"""
            with self.assertRaises(UnsupportedUrlError):
                SongDataExtractor.get_instance(url)

        test_valid_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ", "dQw4w9WgXcQ", 0)
        test_valid_url("https://youtube.com/watch?v=dQw4w9WgXcQ", "dQw4w9WgXcQ", 0)
        test_valid_url("https://www.youtube.com/watch?abc=1&v=dQw4w9WgXcQ&xyz=2", "dQw4w9WgXcQ", 0)
        test_valid_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=14s", "dQw4w9WgXcQ", 14)
        test_valid_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=1m", "dQw4w9WgXcQ", 60)
        test_valid_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=1.5m", "dQw4w9WgXcQ", 90)
        test_valid_url("https://youtu.be/dQw4w9WgXcQ", "dQw4w9WgXcQ", 0)
        test_valid_url("https://youtu.be/dQw4w9WgXcQ?t=14", "dQw4w9WgXcQ", 14)
        test_valid_url(
            "https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=0&fs=1&iv_load_policy=3&start=10",
            "dQw4w9WgXcQ", 10)
        test_valid_url("http://www.youtube.com/v/dQw4w9WgXcQ?version=3", "dQw4w9WgXcQ", 0)
        test_valid_url("https://youtu.be/dQw4w9WgXcQ?t=", "dQw4w9WgXcQ", 0) # Ignore empty timestamp
        test_valid_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley",
                        "dQw4w9WgXcQ", 0)
        test_valid_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=ABC--r8gz8ihiwfhg874hjfdgh"
                        + "f&index=42&ab_channel=RickAstley", "dQw4w9WgXcQ", 0)

        test_invalid_timestamp("https://youtu.be/dQw4w9WgXcQ?t=14x", "dQw4w9WgXcQ", "14x")
        test_invalid_timestamp("https://youtu.be/dQw4w9WgXcQ?t=3h", "dQw4w9WgXcQ", "3h")
        test_invalid_timestamp("https://youtu.be/dQw4w9WgXcQ?t=a", "dQw4w9WgXcQ", "a")

        test_invalid_url("https://www.youtu.be/dQw4w9WgXcQ?t=14")
        test_invalid_url("https://example.com/dQw4w9WgXcQ?t=a")
        test_invalid_url("https://www.youtube.com/watch?t=3")
        test_invalid_url("https://www.youtube.com/v/?t=3")
        test_invalid_url("https://www.youtube.com/embed/?t=3")
        test_invalid_url("www.youtube.com/watch?v=dQw4w9WgXcQ")
