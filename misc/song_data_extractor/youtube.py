from urllib import parse
from typing import Optional
import base64

from .. import logger
from .song_data_extractor import SongDataExtractor

class Youtube(SongDataExtractor):
    """Song data extractor for songs from Youtube"""

    @staticmethod
    def try_get_instance(song_url: str) -> Optional['SongDataExtractor']:
        # pylint: disable=too-many-branches
        # Extract video id or fail, also extract start timestamp
        '''
        Supported patterns

        Examples:
        https://www.youtube.com/watch?v=dQw4w9WgXcQ
        https://youtube.com/watch?v=dQw4w9WgXcQ
        https://www.youtube.com/watch?abc=1&v=dQw4w9WgXcQ&xyz=2
        https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=14s
        https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=1m
        https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=1.5m
        https://youtu.be/dQw4w9WgXcQ
        https://youtu.be/dQw4w9WgXcQ?t=14
        https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=0&fs=1&iv_load_policy=3&start=10
        http://www.youtube.com/v/dQw4w9WgXcQ?version=3

        Parse:
        - Video id
            - (www.)youtube.com/watch then v=[video id] somewhere in query
            - (www.)youtube.com/embed/[video id]
            - (www.)youtube.com/v/[video id]
            - youtu.be/[video id]
        - Timestamp
            - t=[timestamp](s/m) somewhere in query
            - start=[timestamp](s/m) somewhere in query
            Implicit unit is s. Only support s and m here as higher timestamps only possible in
            videos which should not be downloaded in the first place.
            Seconds are truncated to an integer.
        '''

        url = parse.urlparse(song_url)
        query = parse.parse_qs(url.query)

        video_id = None
        if url.hostname in ["youtube.com", "www.youtube.com"]:
            if url.path == "/watch" and "v" in query and len(query["v"]) == 1 \
                    and len(query["v"][0]) > 0: # exclude empty v key in query
                video_id = query["v"][0]
            else:
                path_parts = url.path.split("/")
                if len(path_parts) == 3 and path_parts[1] == "embed" and len(path_parts[2]) > 0:
                    # path is /embed/[video id]
                    video_id = path_parts[2]
                elif len(path_parts) == 3 and path_parts[1] == "v" and len(path_parts[2]) > 0:
                    # path is /v/[video id]
                    video_id = path_parts[2]
        elif url.hostname == "youtu.be":
            # path is /[video id]
            if len(url.path) > 2: # exclude empty path or video id
                video_id = url.path[1:]

        if video_id is None:
            # No known youtube URL pattern
            return None

        timestamp = 0
        raw_timestamp = None
        if "t" in query and len(query["t"]) == 1 and len(query["t"][0]) > 0:
            raw_timestamp = query["t"][0]
        elif "start" in query and len(query["start"]) == 1 and len(query["start"][0]) > 0:
            raw_timestamp = query["start"][0]
        if raw_timestamp is not None:
            if raw_timestamp.endswith("s"):
                try:
                    timestamp = int(float(raw_timestamp[:-1]))
                except ValueError:
                    logger.warning(f"Unknown timestamp structure {raw_timestamp} in url {song_url}")
            elif raw_timestamp.endswith("m"):
                try:
                    timestamp = int(60 * float(raw_timestamp[:-1]))
                except ValueError:
                    logger.warning(f"Unknown timestamp structure {raw_timestamp} in url {song_url}")
            else:
                try:
                    timestamp = int(float(raw_timestamp))
                except ValueError:
                    logger.warning(f"Unknown timestamp structure {raw_timestamp} in url {song_url}")

        return Youtube(video_id, timestamp)

    def __init__(self, video_id: str, timestamp: int):
        """
        Constructs a new instance for the given youtube video id and startingtimestamp (in seconds,
        0 if not given by the URL).

        This constructor should not be called by other functions than the class' method
        `_try_get_instance()` itself.
        """

        self.__video_id = video_id
        """Youtube video id of the corresponding song"""
        self.__timestamp = timestamp
        """Starting timestamp of the corresponding song in seconds"""

    @staticmethod
    def get_name() -> str:
        return "youtube"

    def get_start_timestamp(self) -> int:
        return self.__timestamp

    def get_unique_id(self) -> str:
        # Use base32 encoding to achieve guaranteed valid and unique filename
        return base64.b32encode(self.__video_id.encode('utf-8')).decode('utf-8')
