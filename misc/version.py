"""Versions of the game and protocol"""

GAME_VERSION = "Beta 1.2.0"
"""Name of the current game version"""
PROTOCOL_VERSION = 3
"""Number of the current network protocol version"""
PERMACACHE_VERSION = 2
"""Number of the current permacache file format version"""
