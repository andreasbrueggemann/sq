from enum import IntEnum

from .song_data_extractor.song_data_extractor import SongDataExtractor
from .song_data_extractor.unsupported_url_error import UnsupportedUrlError

class SongStatus(IntEnum):
    """Status of the download of a song"""

    WAITING = 0
    """Song not downloaded yet"""
    READY = 1
    """Song downloaded successfully"""
    FAILED = 2
    """Download of the song failed"""
    REJECTED = 3
    """Song was rejected due to not supported or allowed URL"""

class Song:
    """
    Representation of a single song containing its URL, download status, file path if available
    and information to show when solving.
    """

    def __init__(self, title: str, artist: str, url: str, user: str):
        """
        Creates a new instance for the given title, artist(s), video URL and uploading user
        where the download remains to be executed.
        The download status instantly is set to `SongStatus.REJECTED` if the URL is not supported or
        allowed.
        """

        self.title = title
        self.artist = artist
        self.url = url
        self.user = user
        self.filename = None
        self.status = SongStatus.WAITING
        try:
            self.extractor = SongDataExtractor.get_instance(self.url)
        except UnsupportedUrlError:
            self.status = SongStatus.REJECTED

    def set_ready(self, filename):
        """Sets the song status to ready after downloading its audio file to the given location"""
        self.filename = filename
        self.status = SongStatus.READY

    def set_failed(self):
        """Sets the song status to failed after its download failed"""
        self.status = SongStatus.FAILED

    def get_start_timestamp(self) -> int:
        """
        Returns the start timestamp of the song in seconds which is read from the URL.

        Only call this if the song was not rejected.
        If the URL does not specify a timestamp, `0` is returned.
        """

        assert self.status != SongStatus.REJECTED
        return self.extractor.get_start_timestamp()

    def get_unique_id(self) -> str:
        """
        Returns a unique id of the song.

        Only call this if the song was not rejected.
        The id is chosen such that it can be part of a filename by e.g. providing
        case-insensitivity.
        """

        assert self.status != SongStatus.REJECTED
        return self.extractor.get_unique_id()

    def get_log_entry(self) -> str:
        """
        Returns a string representation of the song to be used in log files.

        Output is of form `title ; artist ; url` where `;` gets escaped by `\\` and `\\` by `\\\\`,
        """

        def escape(raw_string: str):
            return raw_string.replace("\\", "\\\\").replace(";", "\\;")

        return f"{escape(self.title)} ; {escape(self.artist)} ; {escape(self.url)}"

    def copy(self) -> 'Song':
        """Returns a copy of the song object"""
        output = Song(self.title, self.artist, self.url, self.user)
        output.filename = self.filename
        output.status = self.status
        return output

    def to_dict(self):
        """Returns a dict representation of the song without its downloading state"""
        return {
            "title": self.title,
            "artist": self.artist,
            "url": self.url,
            "user": self.user
        }

    @staticmethod
    def from_dict(song_dict: dict):
        """Returns a song constructed from the given dict representation of a song"""
        return Song(song_dict["title"], song_dict["artist"], song_dict["url"], song_dict["user"])
