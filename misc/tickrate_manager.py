from typing import Optional
import time

from . import logger

class TickrateManager:
    """
    Managing class for the client's or server's tickrate.

    This class can be used to compute times to idle such that the client or server
    reaches a specified tickrate.
    Before each tick, method `start_tick()` has to be called and after each tick, method
    `get_next_idle_time()` has to be called which then returns the time to idle such that the time
    elapsed between the calls of both methods together with the idle time result in the desired
    tickrate.
    Calling `get_next_idle_time()` before calling `start_tick()` is allowed and returns `0.0`.
    """

    def __init__(self, tickrate: int):
        """Initializes a new TickrateManager for the specified tickrate"""
        self.__tick_length = 1 / tickrate
        """Desired length of a single tick in seconds"""
        self.__last_pre_tick_time: Optional[float] = None
        """Timestamp of the last time before a tick was executed"""

    def get_next_idle_time(self) -> float:
        """Returns the next time to idle in order to match the specified tickrate"""
        now = time.time()
        if self.__last_pre_tick_time is None:
            # Immediately execute first tick
            return 0.0

        # Calculate the time required to process the last tick to then calculate
        # the timeout/waiting time so that the already passed time is padded to
        # fill the time that a single tick should take according to config.
        delta = now - self.__last_pre_tick_time

        if delta > self.__tick_length:
            logger.ticks_skipped(int(delta / self.__tick_length), delta)
            return 0.0

        return self.__tick_length - delta

    def start_tick(self):
        """Method to be called before a tick starts"""
        self.__last_pre_tick_time = time.time()
