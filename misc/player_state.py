from enum import IntEnum

class ConnectionState(IntEnum):
    """Connection State of a player"""

    CONNECTED = 0
    """Player is connected to the current game"""
    DISCONNECTED = 1
    """Player is disconnected from the server"""
    RECONNECTING = 2
    """Player reconnected to the server after being disconnected
    but is not reconnected to the current game yet"""

class PlayerState:
    """State of a player that compactly summarizes their name, id, connection status, score etc."""

    def __init__(self, name: str, id_: int, score: int, cont: bool, connection: ConnectionState):
        """Constructs a new player_state for the given parameters."""
        # pylint: disable=too-many-arguments
        self.name = name
        self.id = id_
        self.score = score
        self.cont = cont
        self.connection = connection

    def encode(self) -> dict:
        """Returns a dict encoding of the player state"""
        return {
            "name": self.name,
            "id": self.id,
            "score": self.score,
            "cont": self.cont,
            "connection": self.connection
        }

    @staticmethod
    def decode(encoding: dict) -> 'PlayerState':
        """Decodes a new player state given a dict encoding it"""
        return PlayerState(encoding["name"],
                           int(encoding["id"]),
                           int(encoding["score"]),
                           bool(encoding["cont"]),
                           ConnectionState(encoding["connection"]))
