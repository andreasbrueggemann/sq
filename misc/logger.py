"""
Basic logging functionality.

After initializing config and before testing it, call `init_configuration()` to read the logging
configuration from the configuration files.
Otherwise, logging default settings will be used.
"""

# pylint: disable=cyclic-import

from typing import List
import logging
from logging import StreamHandler, Formatter
from logging.handlers import RotatingFileHandler

from config import config

LOGLEVEL_CONNECTIONCHANGE = 25 # between warning and info
logging.addLevelName(LOGLEVEL_CONNECTIONCHANGE, "CONNECTIONCHANGE")

class SqFormatter(Formatter):
    """Custom logging formatter using different formatting for different loglevels"""

    @staticmethod
    def coloring(message: str, color_code: int) -> str:
        """Returns the given message surrounded by ANSI escape sequences to use the color of the
            given color code or just the message without colors if coloring is disabled"""
        if config.Logging.coloring():
            return f"\033[{color_code}m{message}\033[0m"
        else:
            return message

    # pylint: disable=protected-access
    def format(self, record: logging.LogRecord):
        if record.levelno == logging.ERROR:
            self._style._fmt = f"{SqFormatter.coloring('ERROR', 31)}: %(message)s" # red
        elif record.levelno == logging.WARNING:
            self._style._fmt = f"  {SqFormatter.coloring('WARNING', 33)}: %(message)s" # yellow
        elif record.levelno == LOGLEVEL_CONNECTIONCHANGE:
            self._style._fmt = f"  {SqFormatter.coloring('CONNECTIONCHANGE', 94)}: %(message)s"
            # bright blue
        elif record.levelno == logging.INFO:
            self._style._fmt = f"    {SqFormatter.coloring('info', 36)}: %(message)s" # cyan
        elif record.levelno == logging.DEBUG:
            self._style._fmt = f"      {SqFormatter.coloring('debug', 37)}: %(message)s" # white
        else:
            self._style._fmt = f"{SqFormatter.coloring('?%(levelname)s', 95)}: %(message)s"
            # bright magenta
        return super().format(record)

def init_configuration():
    """
    Initializes the logging depending on the set config.

    This should be done after initializing configuration and BEFORE testing it.
    If a config parameter is an illegal value, a callback is used as the logging handler needs to
    work to report such problem later.

    If not initialized, standard logging settings will be used.
    """

    level = config.Logging.level()
    target = config.Logging.target()
    if level.lower() not in ["error", "warning", "info", "debug"]:
        level = "debug" # As we need something to work with to report this problem later
    if target not in ["terminal", "file", "both"]:
        target = "both" # As we need something to work with to report this problem later

    # Convert loglevel to logging representation
    level = logging.getLevelName(level.upper())
    assert isinstance(level, int)

    handlers: List[logging.Handler] = []
    if target in ["terminal", "both"]:
        handler = StreamHandler()
        handler.setFormatter(SqFormatter())
        handlers.append(handler)
    if target in ["file", "both"]:
        try:
            handler = RotatingFileHandler(config.Logging.path(),
                                            mode = "a",
                                            maxBytes = config.Logging.max_file_size(),
                                            backupCount = config.Logging.backups(),
                                            encoding = "utf-8")
            handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s: %(message)s",
                                                    datefmt = "%Y-%d-%m %H:%M:%S"))
            handlers.append(handler)
        except FileNotFoundError: # As config test runs afterwards
            error("Invalid log file path")
    logging.basicConfig(handlers = handlers, level = level)

def __truncate_message(message: str):
    """Truncates the given protocol message to a maximal length given by the config by potentially
        cutting its end"""
    if len(message) > config.Logging.protocol_message_length_limit():
        return message[:(config.Logging.protocol_message_length_limit() - 3)] + "..."
    else:
        return message

def connect(ip_address: str, port: int):
    """A new client with given ip and port connects to the server"""
    logging.log(LOGLEVEL_CONNECTIONCHANGE, "New connection from %s:%i", ip_address, port)

def disconnect(ip_address: str, port: int):
    """A client with given ip and port disconnects from the server"""
    logging.log(LOGLEVEL_CONNECTIONCHANGE, "%s:%i disconnected", ip_address, port)

def error(message: str):
    """An error occurred"""
    logging.error(message)

def warning(message: str):
    """Give a warning"""
    logging.warning(message)

def info(message: str):
    """Print information"""
    logging.info(message)

def ticks_skipped(ticks: int, delta: float):
    """The given number of ticks was skipped as one tick took the given time delta long (seconds)"""
    if delta >= config.Logging.tick_length_warning_threshold():
        warning(f"{ticks} ticks skipped (last tick took {delta:.3f} seconds)")
    elif config.Logging.ticks_skipped():
        logging.debug("%i ticks skipped", ticks)

def incoming(ip_address: str, port: int, message: dict, length: int):
    """Message of the given length was received from the given ip and port"""
    if config.Logging.incoming():
        logging.debug("<-- %s:%i, len: %i, content: %s", ip_address, port, length,
                        __truncate_message(str(message)))

def outgoing(ip_address: str, port: int, message: dict, length: int):
    """Message of the given length was sent to the given ip and port"""
    if config.Logging.outgoing():
        logging.debug("--> %s:%i, len: %i, content: %s", ip_address, port, length,
                        __truncate_message(str(message)))

def event(ip_address: str, port: int, message: str):
    """Socket event fired (data available to read or socket writable)"""
    if config.Logging.socket_events():
        logging.debug("Event for %s:%i: %s", ip_address, port, message)
