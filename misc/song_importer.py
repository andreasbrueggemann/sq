"""Functionality to import lists of songs from a database"""

import requests

from config import config

def fetch_songs(number_songs: int) -> dict:
    """
    Fetches the given number of songs from the database.
    The number being zero represents fetching all abailable songs.

    Raises a `ConnectionError` if connecting to the database was not possible.
    """

    if config.Database.post():
        response = requests.post(
            config.Database.url(),
            data = {
                "username": config.Database.username(),
                "password": config.Database.password(),
                "limit": number_songs
            },
            timeout = 20.0
        )
    else:
        response = requests.get(
            config.Database.url(),
            auth = (config.Database.username(), config.Database.password()),
            params = {
                "limit": number_songs
            },
            timeout = 20.0
        )

    if response.status_code != 200:
        raise ConnectionError("Connection to database not possible, status code "
                                + f"{response.status_code}")
    return response.json()
