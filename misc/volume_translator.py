"""Custom translator for volume slider input to volume allowing other translations than the
    normally expected y=x one"""

import pygame

from config import config

def set_volume(raw_value: float):
    """Sets the pygame music volume to `raw_value**volume_translation_order`"""
    pygame.mixer.music.set_volume(raw_value ** config.GUI.volume_translation_order())
