"""
Logger to write messages to a file.

Each message is appended in its own line to an existing file or the file is created if not existing.
Optionally, messages that already are in the file can be ignored.
"""

def log(message: str, filename: str, no_duplicates: bool = False):
    """
    Writes the given message into a new line of the given file.

    The file is created if not already existing.
    If `no_duplicates` is set to `True`, the function does not change
    the file if the given message already exists in the file.
    """

    if no_duplicates:
        try:
            with open(filename, "r", encoding="utf-8") as log_file:
                entries = log_file.readlines()
            if message.strip() in (e.strip() for e in entries):
                return
        except FileNotFoundError:
            pass # in this case, no duplicate

    with open(filename, "a", encoding="utf-8") as log_file:
        log_file.write(message)
        log_file.write("\n")
