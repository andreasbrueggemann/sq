from __future__ import unicode_literals
from typing import Optional, List, Tuple
from pathlib import Path
import threading
import os
import time
import shutil
import subprocess
import yt_dlp

from misc import logger, file_logger
from misc.song import Song, SongStatus
from config import config
from client_mod.errors import DownloadInterruptionError, UnexpectedYtDlpBehaviourError

class YDLPLogger(): # pylint: disable=missing-function-docstring
    """Logger class for yt_dlp"""

    def debug(self, _):
        pass

    def info(self, _):
        pass

    def warning(self, msg):
        logger.warning(msg)

    def error(self, msg):
        logger.warning(msg) # as ydlp error is being catched

SONG_STATUS_SUCCESS = "success"
"""String to write into song status files if the song download succeeded"""
SONG_STATUS_FAIL = "fail"
"""String to write into song status files if the song download failed"""
TEMP_LOBBY_MUSIC_FILE = "temp_lobby_music.ogg"
"""Name of the temp lobby music file (inside download folder) which is a copy of the original file
    which is loaded using pygame so that the original file can be overwritten without any
    problems"""

class Downloader:
    """
    Downloader daemon using yt_dlp to fetch audio files from youtube urls.
    Provides downloading functionality via yt_dlp.
    The permanent cache may be used to skip downloads if the required files are already present
    locally.

    All prepared files are written to the `temp` folder together with a status file indicating if
    the download was successful.
    A status file indicating a failing download does not have a corresponding audio file.
    This case represents that downloading a song has already been tried but was not successful in
    constrast to a situation where there was no previous try downloading the song.

    The downloader uses a permanent cache as follows:
    Before downloading a song, it checks if the cache already contains the required file.
    If this is the case, the local file is copied to the `temp` folder instead of downloading it.
    Note that this is still done if the permanent cache is disabled.
    If a song is freshly downloaded, it is also copied to the cache if and only if
    `write_to_permacache` and the config are appropriately set.
    """

    def __init__(self, write_to_permacache = None, selftest_mode = False):
        """
        Constructs a new instance.

        The optional flag `write_to_permacache` can be used to decide whether downloaded songs shall
        be written to the permanent cache.
        If it is left out, the config is used for this decision.
        Thus, the flag can be used to overwrite the config which for instance is used for proactive
        downloads.

        The optional flag `selftest_mode` can be used to activate the selftest mode where no songs
        can be read from the permacache or written to and localtemp is ignored. In this mode, song
        volumes are always normalized.
        """

        self.__queue: Optional[List[Song]] = None
        """List of songs to download"""
        self.__current_index = 0
        """Index of currently handled song in queue"""
        self.__daemon: Optional[threading.Thread] = None
        """Daemon worker thread of the downloader"""
        self.__daemon_exception: Optional[Exception] = None
        """Exception thrown by the daemon thread to be forwarded to main thread and be thrown
            there"""
        self.__lock = threading.Lock()
        """Thread lock to prevent simultaneous access from different threads"""
        self.__current_song_progress = 0.0
        """Progress of downloading the current song, between `0.0` and `1.0`"""

        if write_to_permacache is None:
            self.__write_to_permacache = config.Download.permacache_downloads()
            """Whether downloaded files are to be written to the permanent cache"""
        else:
            self.__write_to_permacache = write_to_permacache

        self.__selftest_mode = selftest_mode
        """If `True`, no songs can be read from the permacache and localtemp is ignored and all
            songs are normalized"""
        if self.__selftest_mode:
            self.__write_to_permacache = False

        self.__time_downloading = 0.0
        """Time spent downloading songs using youtube dl"""
        self.__time_normalizing = 0.0
        """Time spend normalizing audio volume"""
        self.__is_normalizing = False
        """Whether the downloader currently is normalizing a song"""
        self.__downloads_failed = 0
        """Number of song downloads that already failed"""
        self.__permacache_hits = 0
        """Number of songs that were read from the permacache instead of being downloaded"""

        self.__overwrite_lobby_music = config.Gameplay.overwrite_lobby_music()
        """Flag indicating that next download success shall overwrite the lobby music"""

    def init_queue(self, queue: List[Song], resume: bool):
        """
        Inits the song queue to the given list and starts downloading by running the download
        daemon.

        If `resume` is set to true, the downloader resumes a previous download by not clearing
        existing files in the `temp` folder.
        In this case, downloading files which already have a status file inside the `temp` folder
        is skipped.

        If a download is already running, it will be terminated before starting the new one.
        Mind that this may block for some time.
        """

        self.__check_for_daemon_exception()

        # If downloader is still running, join its thread and then spawn a new thread
        # stop() uses the lock itself
        self.stop()

        with self.__lock:
            # Resets for each new download process
            self.__queue = queue
            self.__current_index = 0
            self.__current_song_progress = 0.0
            self.__time_downloading = 0.0
            self.__time_normalizing = 0.0
            self.__is_normalizing = False
            self.__downloads_failed = 0
            self.__permacache_hits = 0
            self.__overwrite_lobby_music = config.Gameplay.overwrite_lobby_music()

            if not resume:
                # Purge download folder
                for filename in os.listdir(config.Download.download_path()):
                    if filename not in [".gitignore", TEMP_LOBBY_MUSIC_FILE]:
                        os.remove(f"{config.Download.download_path()}/{filename}")

            self.__daemon = threading.Thread(target = self.__download_daemon, daemon = True)
            # daemon so that thread is killed if game is
            self.__daemon.start()

    def stop(self):
        """Stops the downloader daemon which may block for some time"""
        self.__check_for_daemon_exception()
        if self.__daemon is not None:
            with self.__lock:
                self.__daemon.running = False # Signal to the thread that it is to be stopped
                logger.info("Waiting for downloader daemon to stop")
            # Release lock for waiting to join as the thread to terminate may also require to
            # acquire the lock
            self.__daemon.join()
            logger.info("Joined")

    def __check_for_daemon_exception(self):
        """Checks if a daemon thread crashed and yields the corresponding exception if it is the
            case"""
        with self.__lock:
            if self.__daemon_exception is not None:
                raise self.__daemon_exception

    def is_downloading(self) -> bool:
        """Returns whether the download daemon is running"""
        self.__check_for_daemon_exception()
        with self.__lock:
            if self.__daemon is None:
                return False
            else:
                return self.__daemon.is_alive()

    def get_queue_length(self) -> int:
        """Returns the length of the given queue of songs to download"""
        self.__check_for_daemon_exception()
        with self.__lock:
            return len(self.__queue)

    def get_download_progress(self) -> float:
        """Returns the current progress of downloading which is the number of already finished songs
            (float for progress when downloading a single one)"""
        self.__check_for_daemon_exception()
        with self.__lock:
            return self.__current_index + self.__current_song_progress

    def is_normalizing(self) -> bool:
        """Returns whether the downloader currently is normalizing a song"""
        self.__check_for_daemon_exception()
        with self.__lock:
            return self.__is_normalizing

    def get_time_downloading(self) -> float:
        """
        Returns the time that the downloader has downloaded songs using youtube dl in seconds.

        This time is only updated if a download finished or failed.
        """

        self.__check_for_daemon_exception()
        with self.__lock:
            return self.__time_downloading

    def get_time_normalizing(self) -> float:
        """
        Returns the time that the downloader has normalized audio volume in seconds.

        This time is only updated if a download finished or failed.
        """

        self.__check_for_daemon_exception()
        with self.__lock:
            return self.__time_normalizing

    def get_number_of_failed_downloads(self) -> int:
        """Returns the number of failed downloads"""
        self.__check_for_daemon_exception()
        with self.__lock:
            return self.__downloads_failed

    def get_number_of_permacache_hits(self) -> int:
        """Returns the number of songs read from the permacache"""
        self.__check_for_daemon_exception()
        with self.__lock:
            return self.__permacache_hits

    def fetch_at_index(self, index: int) -> Song:
        """Returns the song at the given index which may be already downloaded, may be failed or may
            not already be handled"""
        self.__check_for_daemon_exception()
        with self.__lock:
            assert (self.__queue is not None and index < len(self.__queue))
            # Copy as original may be accessed by downloader thread to avoid race conditions
            return self.__queue[index].copy()

    def __progress_hook(self, progress: dict):
        """Progress callback for yt_dlp"""
        if progress["status"] == "downloading":
            if "total_bytes" in progress and progress["total_bytes"] is not None \
                and int(progress["total_bytes"]) != 0:
                total_bytes = int(progress["total_bytes"])
            elif "total_bytes_estimate" in progress and progress["total_bytes_estimate"] \
                is not None and int(progress["total_bytes_estimate"]) != 0:
                total_bytes = int(progress["total_bytes_estimate"])
            else:
                logger.warning("Cannot estimate download file size")
                total_bytes = 1 << 25
            progress = int(progress["downloaded_bytes"]) / total_bytes
            # pylint: disable=condition-evals-to-constant
            if progress < self.__current_song_progress and False: # TODO Beta 1.3 remove this?
                                                                  # was a hotfix...
                logger.error("Youtube download progress moved backwards")
                # TODO Beta 1.3 (above) Change into warning later? Also currently doubled
                raise UnexpectedYtDlpBehaviourError("Youtube download progress moved backwards")
            with self.__lock:
                self.__current_song_progress = progress

        # Terminate currently running download if thread was stopped manually
        # by crashing it, see
        # https://github.com/ytdl-org/youtube-dl/issues/16175
        own_thread = threading.current_thread()
        if not getattr(own_thread, "running", True):
            raise DownloadInterruptionError()

    def __get_ydlp_opts(self, index: int):
        """Returns yt_dlp options for the given song index"""

        options = {
            "format": f"{config.Download.quality()}audio/{config.Download.quality()}",
            "postprocessors": [{
                "key": "FFmpegExtractAudio",
                "preferredcodec": "vorbis",
                "preferredquality": "192"
            }],
            "logger": YDLPLogger(),
            "progress_hooks": [self.__progress_hook],
            "outtmpl": f"{config.Download.download_path()}/song_{index}.%(ext)s",
            "cachedir": False,
            "noplaylist": True
        }

        if not config.Download.path_ffmpeg():
            options["ffmpeg_location"] = config.Files.ffmpeg()

        return options

    @staticmethod
    def __get_audio_file_path(index: int) -> str:
        """
        Returns the file path of the audio file for the song at the given index from the current
        quiz.

        Mind that changing this also requires to change `outtmpl` in `__get_ydlp_opts()`.
        """

        return f"{config.Download.download_path()}/song_{index}.ogg"

    @staticmethod
    def __get_status_file_path(index: int) -> str:
        """Returns the file path of the status file for the audio download at the given index from
            the current quiz"""
        return f"{config.Download.download_path()}/song_{index}.status"

    @staticmethod
    def __get_local_temp_audio_file_path(index: int) -> str:
        """Returns the file path of the audio file for the song at the given index from the local
            temp folder which is used for debugging purposes"""

        return f"{config.Debug.local_temp_path()}/song_{index}.ogg"

    @staticmethod
    def __get_permacache_audio_file_path(unique_id: str) -> str:
        """Returns the file path of the audio file for the given unique song id in the permacache"""
        return f"{config.Download.permacache_path()}/{unique_id}.ogg"

    @staticmethod
    def __get_ffmpeg_subprocess_params() -> Tuple[str, bool]:
        """Returns the command and `arg_shell` flag to use when running ffmpeg using
            `subprocess.run()`"""
        if config.Download.path_ffmpeg():
            command = "ffmpeg"
        else:
            command = config.Files.ffmpeg()

        if os.name == "nt":
            # Windows requires to set this to `False` or the game crashes.
            arg_shell = False
        else:
            arg_shell = True

        return (command, arg_shell)

    @staticmethod
    def __get_song_volume(file: str) -> float:
        """Returns the maximum volume of the song in the given file"""
        # https://superuser.com/questions/323119/how-can-i-normalize-audio-using-ffmpeg
        command, arg_shell = Downloader.__get_ffmpeg_subprocess_params()
        out = os.devnull

        # Output goes to stderr for some reason
        result = subprocess.run(
                f'{command} -i {file} -af "volumedetect" -vn -sn -dn -f null {out}',
                capture_output=True, check=True, shell=arg_shell) \
            .stderr.decode("utf-8")
        lines = result.split("\n")
        max_volume = None
        for line in lines:
            line = line.strip()
            if "max_volume:" in line:
                max_volume = float(line.split("max_volume:")[1].strip().split(" ")[0])
                break
        assert max_volume is not None
        return max_volume

    def __song_postprocessing(self, file: str):
        """Postprocessing required for the song written to the given file"""

        if not config.Download.normalize_audio_volume() and not self.__selftest_mode:
            return

        start_time = time.time()
        with self.__lock:
            self.__is_normalizing = True
        # https://superuser.com/questions/323119/how-can-i-normalize-audio-using-ffmpeg
        command, arg_shell = Downloader.__get_ffmpeg_subprocess_params()

        max_volume = Downloader.__get_song_volume(file)
        logger.info(f"Song has max volume {max_volume} dB")

        if max_volume < -0.01:
            # Normalize
            logger.info("Normalizing...")
            # Make source and target file distinct:
            # Copy xyz/abc.def to xyz/abc_old.def and then use xyz/abc_old.def as source and
            # xyz/abc.def as target for normalization
            file_parts = file.split(".")
            old = ".".join(file_parts[:-1]) + "_old." + file_parts[len(file_parts) - 1]
            shutil.copyfile(file, old)
            subprocess.run(
                f'{command} -y -i {old} -af "volume={0 - max_volume}dB" {file}',
                check=True, capture_output=True, shell=arg_shell)
            os.remove(old)

            max_volume = Downloader.__get_song_volume(file)
            logger.info(f"Song has max volume {max_volume} dB")

        with self.__lock:
            self.__time_normalizing += time.time() - start_time
            self.__is_normalizing = False

    def __handle_dl_success(self, index: int):
        """
        Handler for when downloading the song at the given index succeeded.

        This writes the resulting status file, modifies the corresponding song data structure and
        adjusts the internal state.
        """

        with open(Downloader.__get_status_file_path(index), "w", encoding="utf-8") as status_file:
            status_file.write(SONG_STATUS_SUCCESS)

        with self.__lock:
            self.__queue[index].set_ready(Downloader.__get_audio_file_path(index))
            logger.info(f"Download for song {index} succeeded")
            self.__current_song_progress = 0.0
            self.__current_index = index + 1
            if self.__overwrite_lobby_music:
                self.__overwrite_lobby_music = False
                shutil.copyfile(Downloader.__get_audio_file_path(index), config.Files.lobby_music())

    def __handle_dl_fail(self, index: int, song: Song):
        """
        Handler for when downloading the song at the given index failed and thus is skipped.

        This writes the resulting status file, modifies the corresponding song data structure, logs
        the missing song and adjusts the internal state.
        """

        with open(Downloader.__get_status_file_path(index), "w", encoding="utf-8") as status_file:
            status_file.write(SONG_STATUS_FAIL)
        with self.__lock:
            self.__queue[index].set_failed()
            logger.warning(f"Download for song {index} failed ({song.title} - {song.artist})")
            file_logger.log(song.get_log_entry(),
                            config.Files.failed_downloads(),
                            no_duplicates = True)
            self.__current_song_progress = 0.0
            self.__current_index = index + 1
            self.__downloads_failed += 1

    def __download_daemon(self):
        """Daemon thread target for downloading"""
        try:
            own_thread = threading.current_thread()
            assert self.__queue is not None
            for index, song in enumerate(self.__queue):
                if not getattr(own_thread, "running", True):
                    break

                # Check if song download already succeeded previously, e.g., before reconnecting
                if Path(Downloader.__get_status_file_path(index)).is_file():
                    with open(Downloader.__get_status_file_path(index), "r", encoding="utf-8") \
                            as status_file:
                        status = status_file.read()

                    with self.__lock:
                        if status == SONG_STATUS_SUCCESS:
                            self.__queue[index].set_ready(Downloader.__get_audio_file_path(index))
                        else:
                            self.__queue[index].set_failed()
                        self.__current_song_progress = 0.0
                        self.__current_index = index + 1

                    continue

                if not getattr(own_thread, "running", True):
                    break

                # Catch cases where song data cannot be extracted due to not supported URL format
                # or not allowed URL.
                if song.status == SongStatus.REJECTED:
                    logger.error(
                        f"Server sent url {song.url} which is not supported or allowed. "
                        + "This could be due to malicious server behaviour. Skipping the song...")
                    self.__handle_dl_fail(index, song)
                    continue

                if config.Debug.local_temp() and not self.__selftest_mode:
                    self.__simulate_download_song(song, index)
                else: # Standard download
                    self.__download_song(song, index)
        except Exception as error: # pylint: disable=broad-except
            with self.__lock:
                self.__daemon_exception = error
        finally:
            logger.info("Downloader daemon ended")

    def __simulate_download_song(self, song: Song, index: int):
        """
        Emulates downloading the given song by sleeping and copying ile from local folder for
        debugging purposes.

        This approach does not utilize the permacache.
        """
        time.sleep(config.Debug.local_temp_download_delay())
        with self.__lock:
            self.__time_downloading += config.Debug.local_temp_download_delay()
        if Path(Downloader.__get_local_temp_audio_file_path(index)).is_file():
            # Non-existent files represent download error
            shutil.copyfile(Downloader.__get_local_temp_audio_file_path(index),
                            Downloader.__get_audio_file_path(index))
            self.__song_postprocessing(Downloader.__get_audio_file_path(index))
            self.__handle_dl_success(index)
        else:
            self.__handle_dl_fail(index, song)

    def __download_song(self, song: Song, index: int):
        """Downloads the given song or reads it from the permacache if applicable"""
        own_thread = threading.current_thread()

        # Lookup if song exists in permacache
        if Path(Downloader.__get_permacache_audio_file_path(song.get_unique_id())) \
                .is_file() and not self.__selftest_mode:
            logger.info(f"Permacache hit for song {index}")
            shutil.copyfile(
                Downloader.__get_permacache_audio_file_path(song.get_unique_id()),
                Downloader.__get_audio_file_path(index))
            with self.__lock:
                self.__permacache_hits += 1
            self.__handle_dl_success(index)
            return

        # Download song
        opts = self.__get_ydlp_opts(index)
        with yt_dlp.YoutubeDL(opts) as ydlp:
            download_start_time = time.time()
            success = False
            for _ in range(config.Download.attempts_per_song()):
                if not getattr(own_thread, "running", True):
                    break
                try:
                    ydlp.download([song.url])
                    success = True
                    break
                except DownloadInterruptionError:
                    break
                # Otherwise, try again (up to as many times as specified in config)
                except UnexpectedYtDlpBehaviourError as error:
                    logger.error(str(error))
                except Exception as error: # pylint: disable=broad-except
                    # Ignore all other download fails which may occur
                    logger.error(str(type(error)) + " " + str(error))
            with self.__lock:
                self.__time_downloading += time.time() - download_start_time

            if not getattr(own_thread, "running", True):
                return

            if success:
                self.__song_postprocessing(Downloader.__get_audio_file_path(index))
                self.__handle_dl_success(index)

                # Add to cache
                if self.__write_to_permacache:
                    shutil.copyfile(
                        Downloader.__get_audio_file_path(index),
                        Downloader.__get_permacache_audio_file_path(song.get_unique_id()))
            else:
                self.__handle_dl_fail(index, song)
