class DownloadInterruptionError(Exception):
    """
    Technical exception used to interrupt running downloads by yt_dlp.

    It is fired by the progress_hook of a running yt_dlp execution to interrupt it which
    appears to be the valid solution to stop yt_dlp (issue originally from youtube-dl):
    https://github.com/ytdl-org/youtube-dl/issues/16175
    """

    def __init__(self):
        """Constructs a new instance"""
        super().__init__("The running download was interrupted inside one of its progress hooks")

class UnexpectedYtDlpBehaviourError(Exception):
    """Exception for unexpected yt_dlp behaviour"""

    def __init__(self, behaviour: str):
        """Constructs a new instance for the given behaviour description"""
        super().__init__(f"Unexpected yt_dlp behaviour: {behaviour}")

class CustomMessageError(Exception):
    """Exception containing a custom message that shall be displayed"""
