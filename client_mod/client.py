import selectors
import pygame
from client_mod.errors import CustomMessageError

from protocol import network_helper
from protocol.messages import * # pylint: disable=wildcard-import, unused-wildcard-import
from protocol.connection_handler import ConnectionHandler
from misc import logger
from misc.version import PROTOCOL_VERSION, GAME_VERSION
from misc.song import Song
from misc.tickrate_manager import TickrateManager
from config import config, auto_config

from .state.game_context import GameContext

class Client:
    """SQ Client containing the GUI's rendering loop merged with network processing"""

    def __init__(self, server_ip: str, server_port: int, username: str):
        """Constructs and runs a new instance connecting to the server with the given ip address an
            port using the given username."""

        self.__selector = selectors.DefaultSelector()
        logger.info(f"Connecting to {server_ip}:{server_port}...")
        try:
            self.__socket = network_helper.get_socket_for_ip_address(server_ip)
        except ValueError as error:
            raise CustomMessageError(f"Malformed server ip address {server_ip}") from error
        self.__socket.connect((server_ip, server_port))
        self.__socket.setblocking(False)
        self.__connection_handler = ConnectionHandler(self.__selector, self.__socket,
                                                        (server_ip, server_port))
        self.__selector.register(self.__socket, selectors.EVENT_READ,
                                    data = self.__connection_handler)

        pygame.init()
        self.__surface = pygame.display.set_mode(
            (auto_config.game_window_width(), auto_config.game_window_height()), pygame.RESIZABLE)
        pygame.display.set_caption(f"SQ ver. {GAME_VERSION}")

        self.__game_context = GameContext(self.__connection_handler)
        self.__game_context.state().join(username)
        self.__main_loop()

    def __main_loop(self):
        """Main loop of the SQ client"""
        try: # pylint: disable=too-many-nested-blocks
            running = True
            tickrate_manager = TickrateManager(config.Timeouts.base_tickrate())

            while running:
                # Wait for next message for at most the idle time for the specified tickrate
                events = self.__selector.select(tickrate_manager.get_next_idle_time())

                # Begin to process next game tick
                tickrate_manager.start_tick()

                # Handle all socket events
                for key, mask in events:
                    handler: ConnectionHandler = key.data
                    if mask & selectors.EVENT_READ:
                        messages = handler.get_incoming_messages()
                        for message in messages:
                            closed = self.__handle_message(message)
                            if closed:
                                running = False
                    if mask & selectors.EVENT_WRITE:
                        handler.write()
                if not self.__selector.get_map():
                    break

                # Update GUI
                self.__surface, quitting = self.__game_context.state()\
                                                .handle_interaction(self.__surface)
                self.__game_context.state().render(self.__surface)
                pygame.display.update()
                if quitting:
                    pygame.quit()
                    running = False

                # State polling
                self.__game_context.state().poll()

            handler.close()
        except KeyboardInterrupt:
            logger.info("Stopping client...")
        finally:
            self.__selector.close()
            self.__game_context.save_autoconfig_state_to_file()

    def __handle_message(self, message: dict) -> bool:
        """Handles the given received message and returns whether the connection was closed"""
        # pylint: disable=too-many-branches,too-many-statements

        closed = False
        command = message["command"]

        if command == C_PING:
            # immediately reply, does not involve the state
            self.__game_context.send(PING_REPLY())
        elif command == C_REFUSE_CONNECTION:
            logger.error("Server refused connection")
            reason = message["reason"]
            if reason == REFUSE_CONNECTION_REASON_PROTOCOL_VERSION:
                logger.error("Reason: Invalid protocol version, server runs on "
                            + f"{message['content']}, you have {PROTOCOL_VERSION}")
            else:
                logger.error(f"Unknown reason: {reason}")
            closed = True
        elif command == C_SET_PLAYER_ID:
            self.__game_context.state().set_id(message["content"])
        elif command == C_PROVIDE_CONFIG:
            self.__game_context.state().provide_config(message["lookahead"],
                                                       message["no_buzzers"],
                                                       message["maximum_score_per_song"])
        elif command == C_WAIT_FOR_REHABILITATION:
            self.__game_context.state().wait_for_rehabilitation(
                            [Song.from_dict(s) for s in message["songs"]])
        elif command == C_ENTER_LOBBY:
            self.__game_context.state().enter_lobby()
        elif command == C_WAIT_FOR_NEXT_GAME:
            self.__game_context.state().enter_queue()
        elif command == C_SET_PLAYER_STATES:
            self.__game_context.state().set_player_states(
                [PlayerState.decode(e) for e in message["content"]])
        elif command == C_SET_SONG_LIST:
            self.__game_context.state().set_song_list(
                            [Song.from_dict(s) for s in message["content"]])
        elif command == C_SET_GLOBAL_PROGRESS:
            self.__game_context.state().set_global_progress(message["progress"])
        elif command == C_REQUEST_SONG_INDEX:
            self.__game_context.state().request_song_index(message["content"])
        elif command == C_ENTER_PAUSE:
            self.__game_context.state().enter_pause()
        elif command == C_PLAY_SONG:
            self.__game_context.state().play_song_index(message["content"], message["timestamp"])
        elif command == C_ENTER_GUESSING:
            self.__game_context.state().guess(message["player_name"], message["player_id"])
        elif command == C_ENTER_NEUTRAL_GUESSING:
            self.__game_context.state().neutral_guess()
        elif command == C_ENTER_SOLVING:
            self.__game_context.state().solve(message["assigner_id"], message["substitute"])
        elif command == C_ENTER_SPECIFIC_SONG_SOLVING:
            self.__game_context.state().solve_specific_song(message["index"],
                                                            message["assigner_id"],
                                                            message["substitute"])
        elif command == C_SET_POINT_ASSIGNMENT:
            # Converting dict to json changes keys (player ids) from int to str. Revert this here.
            content = {}
            for key, points in message["content"].items():
                content[int(key)] = points
            self.__game_context.state().set_point_assignment(content)
        elif command == C_END:
            self.__game_context.state().end()
        elif command == C_REHABILITATE_INTO_INIT_DOWNLOADING:
            self.__game_context.state().rehabilitate_into_init_downloading()
        elif command == C_RESET:
            self.__game_context.state().reset()
        elif command == C_START_TIME_LIMIT:
            self.__game_context.state().start_time_limit(message["content"])
        elif command == C_KEEPALIVE:
            logger.info("Received keepalive message")
        else:
            raise ValueError(f"Unknown protocol command {command}")

        return closed
