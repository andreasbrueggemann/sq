# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, List, Optional, Dict, Tuple
import pygame

from misc import logger, file_logger, volume_translator
from misc.song import SongStatus
from protocol.messages import ASSIGN_POINTS
from config import config, theme

from gui.align import Align
from gui.label import Label
from gui.toggle_button import ToggleButton
from gui.button_group import ButtonGroup
from gui.interaction_widget import InteractionWidget

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Solving(GameState):
    """
    Solving state of the client.

    In this state, the previously played song is displayed to the players who can assign points
    between them if somebody buzzered previously.
    There also is an option to solve for a song at an arbitrary index.

    ## Interaction
    The player can assign points to all players if they buzzered or are the substitute for somebody
    who buzzered.
    The player can declare if they want to continue or not.
    Their continue status is reset if the point assignment changes.
    """

    def __init__(self, context: 'GameContext', assigner_id: Optional[int], substitute: bool,
                 specific_song_index: Optional[int] = None):
        """
        Constructs a new instance for the given context where `assigner_id` is the id of the player
        who shall assign points.
        This normally is the player who buzzered which could change if this player disconnects.
        In this case, the flag `substitute` is set to `True`.
        If `assigner_id` is `None`, the song was skipped.

        `specific_song_index` can be set to not solve the last played song, but the specific song
        given at this index.
        """

        # Enter the state
        super().__init__(context)
        logger.info("Solving...")
        self.__point_assignment: Dict[int, int] = {}

        if specific_song_index is None:
            song = self.context().get_current_song()
        else:
            song = self.context().get_downloader().fetch_at_index(specific_song_index)

        self.__l_solution = Label(f"{song.title}\n{song.artist}\n(Upload: {song.user})",
                                    theme.Text.big_size(), theme.Text.color())

        self.__b_playlist = ToggleButton()
        self.__b_playlist.set_text("Add to playlist")
        def playlist_callback(button_on: bool):
            if button_on:
                file_logger.log(song.get_log_entry(), config.Files.playlist(),
                                no_duplicates = not config.Gameplay.allow_playlist_duplicates())
            else:
                self.__b_playlist.set_pressed(True)
        self.__b_playlist.set_callback(playlist_callback)

        # Call solve for same functionality as if already in state but substitute is chosen
        self.solve(assigner_id, substitute)
        if specific_song_index is not None:
            # Have to reload song and jump to specific timestamp to repeat from there
            if song.status == SongStatus.READY:
                pygame.mixer.music.load(song.filename)
                # According to pygame documentation, volume is reset when loading song...
                volume_translator.set_volume(self.context().get_volume())
                pygame.mixer.music.play()
                if self.context().get_timestamp_for_index(specific_song_index) is None:
                    timestamp = song.get_start_timestamp()
                else:
                    timestamp = self.context().get_timestamp_for_index(specific_song_index)
                try:
                    pygame.mixer.music.set_pos(timestamp)
                except pygame.error:
                    logger.error(f"Tried to skip to timestamp {timestamp} in song, failed")
            else:
                # might happen if player disconnected early and never downloaded this song
                # => do not halt solving in this case
                logger.warning("Song has not been downloaded (yet), no song playing")
        elif not self.__skipped:
            # Was paused before in Guessing state, also no specific song index, so just continue
            pygame.mixer.music.unpause()

        # solve() already resizes

    def __get_points_for_player_id(self, player_id: int) -> int:
        """Returns the assigned points for the given player id using the current point assignment"""
        if player_id in self.__point_assignment:
            return self.__point_assignment[player_id]
        else:
            return 0

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Solving"

    def leave_state(self):
        pygame.mixer.music.stop()
        pygame.mixer.music.unload()

    def can_continue(self) -> Tuple[bool, str, bool, str]:
        return (True, "Continue", False, "C")

    @staticmethod
    def _has_scoreboard() -> bool:
        return True

    @staticmethod
    def _has_lookahead_progress() -> bool:
        return True

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_solution.render(surface)
        self.__b_playlist.render(surface)
        if not self.__skipped:
            for player_l in self.__ls_players:
                player_l.render(surface)
            for you_l in self.__ls_you:
                you_l.render(surface)
            for point_l in self.__ls_points:
                point_l.render(surface)
            if self.__is_assigner:
                for point_bg in self.__bgs_points:
                    point_bg.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_solution.set_position(width / 2, height / 4)
        self.__b_playlist.set_position_and_dimensions(50, height - 120, 170, 90)
        if not self.__skipped:
            row_height = height / 2 / len(self.__ls_players)
            for i, player_l in enumerate(self.__ls_players):
                player_l.set_position(
                    width / 2.5 - 100, height / 2 + i * row_height + theme.Text.small_size() / 2)
                self.__ls_you[i].set_position(
                    width / 2.5 - 50, height / 2 + i * row_height + theme.Text.small_size() / 2)
                self.__ls_points[i].set_position(
                    width / 2.5, height / 2 + i * row_height + theme.Text.small_size() / 2)
                if self.__is_assigner:
                    self.__bgs_points[i].set_position_and_dimensions(
                        width / 2, height / 2 + i * row_height + theme.Text.small_size() / 2 - 25,
                        50 * (self.context().get_maximum_score_per_song() + 1), 50)

    def _get_interaction_widgets(self) -> List[InteractionWidget]:
        output = [self.__b_playlist]
        if not self.__skipped and self.__is_assigner:
            output += self.__bgs_points
        return output

    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state

    def solve(self, assigner_id: int, substitute: bool):
        self.__skipped = assigner_id is None
        self.__is_assigner = assigner_id == self.context().get_player_id()
        self.__assigner_id = assigner_id
        self.__substitute = substitute

        # Construct GUI elements here as they may change for e.g. different assigners
        if not self.__skipped:
            self.__ls_players: List[Label] = []
            self.__ls_you: List[Label] = []
            self.__ls_points: List[Label] = []
            for player in self.context().get_player_states():
                self.__ls_players.append(
                    Label(player.name,
                            theme.Text.small_size(),
                            theme.Text.highlight_color() if player.id == self.__assigner_id
                                            and not self.__substitute else theme.Text.color(),
                            Align.RIGHT))
                self.__ls_you.append(
                    Label(" (You)" if player.id == self.context().get_player_id() else "",
                          theme.Text.small_size(),
                          theme.Text.highlight_color()))
                self.__ls_points.append(Label(str(self.__get_points_for_player_id(player.id)),
                                                theme.Text.small_size(), theme.Text.color()))

            if self.__is_assigner:
                self.__bgs_points: List[ButtonGroup] = []
                for player in self.context().get_player_states():
                    points_bg = ButtonGroup([str(i) for i in range(
                                                self.context().get_maximum_score_per_song() + 1)])
                    points_bg.set_selected(0)
                    def callback(points, player_id = player.id):
                        # default args s.t. current values are used
                        self.context().send(ASSIGN_POINTS(player_id, points))
                    points_bg.set_callback(callback)
                    points_bg.set_selected(self.__get_points_for_player_id(player.id))
                    self.__bgs_points.append(points_bg)

        self.resize(self.context().get_window_width(), self.context().get_window_height())

    def set_point_assignment(self, assignment: Dict[int, int]):
        self.__point_assignment = assignment
        if not self.__skipped:
            for i, player in enumerate(self.context().get_player_states()):
                self.__ls_points[i].set_text(str(self.__get_points_for_player_id(player.id)))

    # No custom reset handling required
    # No time limit used in this state
    # Continuing players not relevant in this state
