# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Tuple, List, Optional
import time
import pygame

from misc import logger, volume_translator
from config import theme
from protocol.messages import BUZZER

from gui.interaction_widget import InteractionWidget
from gui.buzzer import Buzzer
from gui.label import Label
from gui.progress import Progress

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Listening(GameState):
    """
    Listening state of the client.

    In this state, the client listens to a song and may decide to buzzer or skip.

    ## Interaction
    A song is played.
    The client can buzzer or decide to skip the song.
    The buzzer is only displayed if not in 'no buzzer' mode.
    It is shown how many players currently wish to skip and the remaining time if enabled.
    """

    def __init__(self, context: 'GameContext', index: int, timestamp: float):
        """Constructs a new instance for the given context that plays the song at the given index
            from the given timestamp (seconds) on"""
        super().__init__(context)
        logger.info(f"Playing song at index {index}")

        # Listening only entered if song is ready
        self.context().set_current_song(self.context().get_downloader().fetch_at_index(index))
        pygame.mixer.music.load(self.context().get_current_song().filename)
        # save for reuse in 'no buzzers' mode:
        self.context().set_timestamp_for_index(index, timestamp)
        # According to pygame documentation, volume is reset when loading song...
        volume_translator.set_volume(self.context().get_volume())
        pygame.mixer.music.play()
        try:
            pygame.mixer.music.set_pos(timestamp)
        except pygame.error:
            logger.error(f"Tried to skip to timestamp {timestamp} in song, failed")

        self.__l_title = Label("Guess the song!", theme.Text.big_size(), theme.Text.color())
        if self.context().is_no_buzzers():
            self.__buzzer = None
        else:
            self.__buzzer = Buzzer()
            self.__buzzer.set_callback(lambda _: self.context().send(BUZZER()))
        self.__p_time_limit: Optional[Progress] = None
        self.__time_limit: Optional[float] = None
        self.__time_limit_start: Optional[float] = None
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Listening"

    # No custom operations when leaving state necessary

    def can_continue(self) -> Tuple[bool, str, bool, str]:
        if self.context().is_no_buzzers():
            return (True, "Continue", False, "C")
        else:
            return (True, "Skip", False, "S")

    @staticmethod
    def _has_scoreboard() -> bool:
        return True

    @staticmethod
    def _has_lookahead_progress() -> bool:
        return True

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)
        if self.__buzzer is not None:
            self.__buzzer.render(surface)
        if self.__p_time_limit is not None:
            passed_time = time.time() - self.__time_limit_start
            remaining_time = max(0.0, self.__time_limit - passed_time)
            self.__p_time_limit.update_progress(remaining_time / self.__time_limit)
            self.__p_time_limit.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 8)
        if self.__buzzer is not None:
            self.__buzzer.set_position_and_dimensions(
                width / 2, height / 2, height / 2, height / 2, centered = True)
        if self.__p_time_limit is not None:
            self.__p_time_limit.set_position_and_dimensions(width / 2, height * 3 / 4 + 20,
                                                            height / 2, 20, centered = True)

    def _get_interaction_widgets(self) -> List[InteractionWidget]:
        if self.__buzzer is None:
            return []
        else:
            return [self.__buzzer]

    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required

    def start_time_limit(self, time_limit: float):
        self.__time_limit = time_limit
        self.__time_limit_start = time.time()
        self.__p_time_limit = Progress()
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    # Player states not relevant in this state
