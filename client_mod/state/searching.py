# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Optional
import pygame

from misc import logger
from misc.song import SongStatus
from config import theme
from protocol.messages import REPLY_SONG_INDEX

from gui.label import Label
from gui.multi_progress import MultiProgress

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Searching(GameState):
    """
    Searching state of the client.

    In this state, the client receives queries for song indexed and replies if they are available.

    ## Interaction
    The player is shown the download progress if they have to wait for the next song.
    """

    def __init__(self, context: 'GameContext', index: int):
        """Constructs a new instance for the given context where the song at the given index is
            requested"""
        super().__init__(context)
        self.__await_dl_finish_index: Optional[int] = None
        """Index that is requested and still being downloaded by the own downloader; `None` if
            such index does not exist"""
        self.request_song_index(index)
        logger.info("Enter searching phase...")

        self.__l_title = Label("Fetching next song...", theme.Text.big_size(), theme.Text.color())
        self.__p_progress = MultiProgress()
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Searching"

    # No custom operations when leaving state necessary
    # No continuing

    @staticmethod
    def _has_scoreboard() -> bool:
        return True

    @staticmethod
    def _has_lookahead_progress() -> bool:
        return True

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)

        global_progress = self.context().get_progress() - self.context().get_last_requested_index()
        global_progress = min(global_progress, 1.0)
        local_progress = self.context().get_downloader().get_download_progress() \
                            - self.context().get_last_requested_index()
        local_progress = min(local_progress, 1.0)
        self.__p_progress.update_progress(global_progress, local_progress)
        self.__p_progress.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 3)
        self.__p_progress.set_position_and_dimensions(
            width / 2, height * 2 / 3, width / 2, 80, centered = True)

    # No interaction widgets

    def _poll(self):
        # If server is waiting for this client's download, poll if it is finished (or failed) and
        # notify the server as soon as possible
        song = None
        if self.__await_dl_finish_index is not None:
            song = self.context().get_downloader().fetch_at_index(self.__await_dl_finish_index)

            if song.status != SongStatus.WAITING: # download not running any longer
                if song.status in [SongStatus.FAILED, SongStatus.REJECTED]:
                    self.context().send(REPLY_SONG_INDEX(song.status, self.__await_dl_finish_index,
                                                            0.0))
                else:
                    assert song.status == SongStatus.READY
                    audio = pygame.mixer.Sound(song.filename)
                    length = audio.get_length()
                    self.context().send(REPLY_SONG_INDEX(song.status, self.__await_dl_finish_index,
                                                            length))

                self.__await_dl_finish_index = None

    ### Implementation of abstract methods for handling incoming messages from a player ###

    def request_song_index(self, index: int):
        logger.info(f"Got request for song at index {index}")
        self.__await_dl_finish_index = None
        song = self.context().get_downloader().fetch_at_index(index)
        self.context().set_last_requested_index(index)
        if song.status == SongStatus.WAITING:
            self.__await_dl_finish_index = index
            self.context().send(REPLY_SONG_INDEX(song.status, index, 0.0))
        elif song.status in [SongStatus.FAILED, SongStatus.REJECTED]:
            self.context().send(REPLY_SONG_INDEX(song.status, index, 0.0))
        else:
            assert song.status == SongStatus.READY
            audio = pygame.mixer.Sound(song.filename)
            length = audio.get_length()
            self.context().send(REPLY_SONG_INDEX(song.status, index, length))

    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required
    # No time limit used in this state
    # Player states not relevant in this state
