# pylint: disable=cyclic-import

from typing import List, Optional, Dict
import time

from protocol.connection_handler import ConnectionHandler
from client_mod.downloader import Downloader
from misc.song import Song
from misc.player_state import ConnectionState, PlayerState
from config import config, auto_config, theme

from gui.table import TableCell

from .game_state import GameState
from .gui_overlay import GUIOverlay

class GameContext:
    """
    Context of a `GameState` that holds such state and keeps track of variables which are to be
    preserved when state transitions appear and manages some GUI elements used by multiple states.
    """

    def __init__(self, connection_handler: ConnectionHandler):
        """Constructs a new instance that enters the `Connecting` state and joins the game"""

        self.__connection_handler = connection_handler
        try:
            with open(config.Files.player_id(), "r", encoding="utf-8") as id_file:
                self.__player_id = int(id_file.read())
        except FileNotFoundError:
            self.__player_id = 0
        self.__player_name: Optional[str] = None
        self.__player_states: List[PlayerState] = []
        self.__downloader = Downloader()
        self.__downloading = False
        """
        Whether downloader was running when polling the last time.
        Need this as its own variable so that download progress can be sent one final time when
        the download finished
        """
        self.__progress = 0.0
        """Global download progress (in number of successfully or failed downloaded songs)"""
        self.__current_song: Optional[Song] = None
        self.__songs_to_timestamp: Dict[int, float] = {} # To remember starting timestamps (no-buzz)
        self.__window_width = auto_config.game_window_width()
        self.__window_height = auto_config.game_window_height()
        self.__lookahead: int = 1
        """Download lookahead"""
        self.__no_buzzers: bool = False
        """Whether 'no buzzer' mode is activated"""
        self.__maximum_score_per_song: int = 1
        """Maximum score that can be assigned to a player for one song"""
        self.__last_progress_sync = 0.0
        """Time when the latest transmission of the local download progress to the server was"""
        self.__last_requested_index = 0
        """Index of last requested song"""

        self.__gui_overlay = GUIOverlay(self)

        # Import here to prevent circular imports
        from .connecting import Connecting
        self.__state = Connecting(self)

    def reset(self):
        """Resets the internal state after a game finished"""
        self.__player_states.clear()
        self.__downloader.stop()
        self.__downloading = False
        self.__progress = 0.0
        self.__current_song = None
        self.__last_progress_sync = 0.0
        self.__last_requested_index = 0

    def state(self) -> GameState:
        """Returns the current state of the game"""
        return self.__state

    def transition(self, state: GameState):
        """Transitions to the given state"""
        self.__state = state

    def leave_state(self):
        """
        Leaves the current state.

        This is called immediately by the GameState superclass when it is constructed so that any
        prior state is properly left.
        This cannot be done in `transition()` as this appears after the new state has been created
        and might have done some own actions.
        """

        if hasattr(self, '_GameContext__state'):
            # Is the case when entering `Connecting` in the first place
            self.__state.leave_state()

    def send(self, message: dict):
        """Sends the given message to the server"""
        self.__connection_handler.add_outgoing_message(message)

    def get_continueboard_table_data(self):
        """
        Returns data for the board showing all players and who wishes to continue as a list of rows.

        Final format is as follows:

        Alice   | (You) | (continue) 
        Bob     |       | 
        Charlie |       | (continue)
        """

        _, continue_str, _, _ = self.__state.can_continue()
        data: List[List[TableCell]] = []
        for state in self.__player_states:
            you_marker = "(You)" if state.id == self.__player_id else ""
            status_marker = f'({continue_str})' if state.cont else ""

            data.append([TableCell(state.name, "Ikea Blahaj", theme.Text.color()),
                        TableCell(you_marker, "(You)", theme.Text.highlight_color()),
                        TableCell(status_marker, "(Continue)", theme.Text.continue_status_color())])
        return data

    def get_scoreboard_table_data(self, ending_formatting: bool = False) -> List[List[TableCell]]:
        """
        Returns data for the scoreboard as a list of rows.

        Final scoreboard format is as follows:

        Alice   | (You) | (C)   | 5
        Bob     |       | (RC)  | 4
        Charlie |       |       | 3

        Set `ending_formatting` to true to not apply washout etc. to colors.
        """

        _, _, _, continue_str = self.__state.can_continue()
        data: List[List[TableCell]] = []
        for state in self.__player_states:
            you_marker = "(You)" if state.id == self.__player_id else ""
            if state.connection == ConnectionState.DISCONNECTED:
                status_marker = "(DC)"
                status_color = theme.Text.disconnect_color() # no washout
            elif state.connection == ConnectionState.RECONNECTING:
                status_marker = "(RC)"
                status_color = theme.Text.reconnect_color() # no washout
            else:
                status_marker = f'({continue_str})' if state.cont else ""
                status_color = theme.apply_rgb_washout(theme.Text.continue_status_color(),
                                                       theme.Scoreboard.washout())

            data.append([TableCell(state.name, "Ikea Blahaj",
                                   theme.Text.color() if ending_formatting else
                                   theme.Scoreboard.color()),
                        TableCell(you_marker, "(You)",
                                  theme.Text.highlight_color() if ending_formatting else
                                  theme.apply_rgb_washout(theme.Text.highlight_color(),
                                                          theme.Scoreboard.washout())),
                        TableCell(status_marker, "(RC)", status_color),
                        TableCell(":", ":", theme.Text.color() if ending_formatting else
                                            theme.Scoreboard.color()),
                        TableCell(str(state.score), "999",
                                  theme.Text.color() if ending_formatting else
                                  theme.Scoreboard.color())])
        return data

    def save_autoconfig_state_to_file(self):
        """Saves the internal state which uses the auto configuration `AutoConfig`"""
        auto_config.write_current_state(self.__window_width,
                                        self.__window_height,
                                        self.__gui_overlay.get_volume())

    ###########################
    ### Getters and Setters ###
    ###########################

    def get_player_id(self) -> int:
        """Returns the player id"""
        return self.__player_id

    def set_player_id(self, player_id: int):
        """Sets the player id to the given value"""
        self.__player_id = player_id

        # Write to file for potential reconnects
        with open(config.Files.player_id(), "w", encoding="utf-8") as id_file:
            id_file.write(str(player_id))

    def get_player_name(self) -> Optional[str]:
        """Returns the name of the player if set"""
        return self.__player_name

    def set_player_name(self, name: str):
        """Sets the player name to the given string"""
        self.__player_name = name

    def set_player_states(self, player_states: List[PlayerState]):
        """Player states are updated (e.g., somebody continues, joins, gets a new score, ...)"""
        self.__player_states = player_states
        self.__gui_overlay.set_scoreboard_data(self.get_scoreboard_table_data())

    def get_player_states(self) -> List[PlayerState]:
        """Returns all player states (e.g., somebody continues, joins, gets a new score, ...)"""
        return self.__player_states

    def get_downloader(self) -> Downloader:
        """Returns the song downloader"""
        return self.__downloader

    def is_downloading(self) -> bool:
        """Returns whether downloader was running last time when polled"""
        return self.__downloading

    def set_downloading(self, downloading: bool):
        """Sets whether the downloader is still running"""
        self.__downloading = downloading

    def get_last_progress_sync(self) -> float:
        """Returns the time when the latest transmission of the local download progress
            to the server was"""
        return self.__last_progress_sync

    def update_last_progress_sync(self):
        """Sets the time when the latest transmission of the local download progress to the
            server was to the current time"""
        self.__last_progress_sync = time.time()

    def get_progress(self) -> float:
        """Returns the global download progress"""
        return self.__progress

    def set_progress(self, progress: float):
        """Sets the global download progress"""
        self.__progress = progress

    def get_window_width(self) -> int:
        """Returns the current window width"""
        return self.__window_width

    def get_window_height(self) -> int:
        """Returns the current window height"""
        return self.__window_height

    def set_window_dimensions(self, width, height):
        """Sets the window dimensions"""
        self.__window_width = width
        self.__window_height = height

    def get_lookahead(self) -> int:
        """Returns the download lookahead"""
        return self.__lookahead

    def set_lookahead(self, lookahead: int):
        """Sets the download lookahead"""
        self.__lookahead = lookahead

    def is_no_buzzers(self) -> int:
        """Returns whether 'no buzzer' mode is active"""
        return self.__no_buzzers

    def set_no_buzzers(self, no_buzzers: bool):
        """Sets whether 'no buzzer' mode is active"""
        self.__no_buzzers = no_buzzers

    def get_maximum_score_per_song(self) -> int:
        """Returns the maximum score that can be assigned to a player for one song"""
        return self.__maximum_score_per_song

    def set_maximum_score_per_song(self, maximum_score_per_song: int):
        """Sets the maximum score that can be assigned to a player for one song"""
        self.__maximum_score_per_song = maximum_score_per_song

    def get_last_requested_index(self) -> int:
        """Returns the index of the last requested song"""
        return self.__last_requested_index

    def set_last_requested_index(self, index: int):
        """Sets the index of the last requested song to the given value"""
        self.__last_requested_index = index

    def get_current_song(self) -> Optional[Song]:
        """Returns the current song if existing"""
        return self.__current_song

    def set_current_song(self, song: Optional[Song]):
        """Sets the current song to the given one"""
        self.__current_song = song

    def get_volume(self) -> float:
        """Returns the current volume `0.0 <= x <= 1.0`"""
        return self.__gui_overlay.get_volume()

    def get_gui_overlay(self) -> GUIOverlay:
        """Returns the GUI overlay containing widgets shown across multiple states"""
        return self.__gui_overlay

    def set_timestamp_for_index(self, index: int, timestamp: float):
        """Saves the starting timestamp at index i for later reuse in 'no buzzers' mode"""
        self.__songs_to_timestamp[index] = timestamp

    def get_timestamp_for_index(self, index: int) -> Optional[float]:
        """Returns a previously used starting timestamp for the song at the given index or `None`
            if unknown"""
        if index in self.__songs_to_timestamp:
            return self.__songs_to_timestamp[index]
        else:
            return None
