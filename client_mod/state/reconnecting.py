# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, List
import pygame

from misc import logger
from misc.song import Song
from config import theme

from gui.label import Label

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Reconnecting(GameState):
    """
    Reconnecting state of the client.

    The player remains in this state until they are rehabilitated into the running game.

    ## Interaction
    None
    """

    def __init__(self, context: 'GameContext', songs: List[Song]):
        """Constructs a new instance for the given context using the given song list"""
        super().__init__(context)
        logger.info("Reconnecting...")
        logger.info("Proceed to download songs...")
        self.context().get_downloader().init_queue(songs, True)
        # Following is reuired as download may already be complete leading to downloader remaining
        # inactive. In this case, downloading is manually set to `True` here so that the (final)
        # download progress is sent to the server once before downloading is set to `False`
        # automatically.
        self.context().set_downloading(True)

        self.__l_connect = Label("Wait to be rehabilitated into running game...",
                                    theme.Text.small_size(), theme.Text.color())
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Reconnecting"

    # No custom operations when leaving state necessary
    # No continuing
    # No scoreboard
    # No lookahead progress
    # No volume slider

    def _render(self, surface: pygame.Surface):
        self.__l_connect.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_connect.set_position(width / 2, height / 2)

    # No interaction widgets
    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required
    # No time limit used in this state
    # Player states not relevant in this state
