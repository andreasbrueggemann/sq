# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, List, Optional
import pygame

from misc import logger
from misc.song import Song
from config import theme

from gui.label import Label
from gui.multi_progress import MultiProgress

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class InitDownloading(GameState):
    """
    Initial downloading state of the client.

    In this state, the client waits until all clients have reached the lookahead with their
    downloads.
    Songs are not given e.g. if player is rehabilitated and thus already knows songs.

    ## Interaction
    The player is shown their and the global download progress.
    """

    def __init__(self, context: 'GameContext', songs: Optional[List[Song]]):
        """
        Constructs a new instance for the given context and list of songs.

        The list of songs can be `None` indicating that the player is rehabilitating and already
        got their song list.
        """

        super().__init__(context)
        logger.info("Starting initial download...")
        if songs is not None:
            self.context().get_downloader().init_queue(songs, False)

        self.__l_title = Label(
            "Downloading first batch of songs...", theme.Text.big_size(), theme.Text.color())
        self.__p_progress = MultiProgress(self.context().get_lookahead())
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "InitDownloading"

    def leave_state(self):
        pygame.mixer.music.stop() # Stop lobby music

    # No continuing

    @staticmethod
    def _has_scoreboard() -> bool:
        return True

    # No lookahead progress

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)
        own_progress = min(self.context().get_downloader().get_download_progress()
                                / self.context().get_lookahead(),
                            1.0)
        progress = min(self.context().get_progress() / self.context().get_lookahead(), 1.0)
        self.__p_progress.update_progress(progress, own_progress)
        self.__p_progress.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 3)
        self.__p_progress.set_position_and_dimensions(
            width / 2, height * 2 / 3, width / 2, 80, centered = True)

    # No interaction widgets
    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required
    # No time limit used in this state
    # Player states not relevant in this state
