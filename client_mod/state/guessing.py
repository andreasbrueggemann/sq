# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Tuple, Optional
import time
import pygame

from misc import logger
from config import theme

from gui.buzzer import Buzzer
from gui.label import Label
from gui.progress import Progress

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Guessing(GameState):
    """
    Guessing state of the client.

    In this state, the players can guess which song they previously listened to after some
    player buzzered.
    Also, all players can write down their guess if in 'no buzzer' mode.

    ## Interaction
    The players can declare if they want to continue or not.
    The remaining time is shown.
    """

    def __init__(self, context: 'GameContext',
                 player_name: Optional[str], player_id: Optional[int]):
        """
        Constructs a new instance for the given context after the player with given name and id
        buzzered.

        Player name and id will be `None` in 'no-buzzer'-mode.
        """
        super().__init__(context)
        # check no buzzers => name and id not None
        # and buzzers => name and id None
        assert (player_name is not None and player_id is not None) or self.context().is_no_buzzers()
        assert not self.context().is_no_buzzers() or (player_name is None and player_id is None)
        pygame.mixer.music.pause()
        self.__player_name = player_name
        self.__player_self = player_id == self.context().get_player_id()
        if self.context().is_no_buzzers():
            logger.info("Entering 'no buzzer' guessing phase")
            title = "Write down your guess..."
        else:
            logger.info(f"Player {player_name}{' (You)' if self.__player_self else ''} buzzered")
            title = f"{self.__player_name}{' (You)' if self.__player_self else ''} is guessing..."

        self.__l_title = Label(title, theme.Text.big_size(), theme.Text.color())
        if self.context().is_no_buzzers():
            self.__buzzer = None
        else:
            self.__buzzer = Buzzer()
            self.__buzzer.set_active(self.__player_self)
        self.__p_time_limit: Optional[Progress] = None
        self.__time_limit: Optional[float] = None
        self.__time_limit_start: Optional[float] = None
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Guessing"

    # No custom operations when leaving state necessary

    def can_continue(self) -> Tuple[bool, str, bool, str]:
        if self.context().is_no_buzzers():
            return (True, "Continue", False, "C")
        else:
            return (True, "Solve", False, "S")

    @staticmethod
    def _has_scoreboard() -> bool:
        return True

    @staticmethod
    def _has_lookahead_progress() -> bool:
        return True

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)
        if self.__buzzer is not None:
            self.__buzzer.render(surface)
        if self.__p_time_limit is not None:
            passed_time = time.time() - self.__time_limit_start
            remaining_time = max(0.0, self.__time_limit - passed_time)
            self.__p_time_limit.update_progress(remaining_time / self.__time_limit)
            self.__p_time_limit.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 8)
        if self.__buzzer is not None:
            self.__buzzer.set_position_and_dimensions(
            width / 2, height / 2, height / 2, height / 2, centered = True)
        if self.__p_time_limit is not None:
            self.__p_time_limit.set_position_and_dimensions(width / 2, height * 3 / 4 + 20,
                                                            height / 2, 20, centered = True)

    # No interaction widgets
    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required

    def start_time_limit(self, time_limit: float):
        self.__time_limit = time_limit
        self.__time_limit_start = time.time()
        self.__p_time_limit = Progress()
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    # Player states not relevant in this state
