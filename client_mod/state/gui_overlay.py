from typing import TYPE_CHECKING, List
import time
import pygame

from config import config, theme, auto_config
from misc import volume_translator

from gui.align import Align
from gui.interaction_widget import InteractionWidget
from gui.label import Label
from gui.multi_progress import MultiProgress
from gui.slider import Slider
from gui.table import Table, TableCell

if TYPE_CHECKING:
    from .game_context import GameContext

class GUIOverlay:
    """
    GUI overlay containing elements that can be shown by multiple states.

    This is used by the game context to manage GUI widgets globally so that they do not have to be
    initialized again by each state when transitioning.
    """

    def __init__(self, context: 'GameContext'):
        """Constructs a new instance for the given context"""
        self.__context = context
        self.__last_fps_update: float = 0.0
        """Time when FPS got updates last"""
        self.__frame_counter: int = 0
        """Frames since `__last_fps_update`"""

        self.__t_scoreboard = Table(theme.Text.small_size(), Align.RIGHT)
        self.__p_lookahead_progress = MultiProgress()
        self.__s_volume = Slider(auto_config.volume())
        self.__s_volume.set_callback(volume_translator.set_volume)
        if config.GUI.show_fps():
            self.__l_fps = Label("", theme.Text.small_size(), theme.Text.color())
        if config.GUI.show_download_stats():
            self.__l_download_stats = Label("", theme.Text.small_size(), theme.Text.color())

    def render(self, surface: pygame.Surface, scoreboard: bool, lookahead_progress: bool,
                volume: bool):
        """
        Renders the global GUI overlay elements to the given surface.

        The given arguments decide whether specific overlay elements shall be displayed.
        """

        # Scoreboard
        if scoreboard:
            self.__t_scoreboard.render(surface)

        # Lookahead progress
        if lookahead_progress:
            # Number of segments may update with changing last requested index => compute every time
            # Only queue length - last requested index - 1 songs remain after last requested index
            segments = min(self.__context.get_lookahead(),
                            self.__context.get_downloader().get_queue_length()
                                - self.__context.get_last_requested_index() - 1)
            if segments > 0: # Do not render progress bar when already in last song
                self.__p_lookahead_progress.set_segments(segments)
                global_progress = self.__context.get_progress() \
                                    - self.__context.get_last_requested_index() - 1
                global_progress = min(global_progress / segments, 1.0)
                local_progress = self.__context.get_downloader().get_download_progress() \
                                    - self.__context.get_last_requested_index() - 1
                local_progress = min(local_progress / segments, 1.0)
                self.__p_lookahead_progress.update_progress(global_progress, local_progress)
                self.__p_lookahead_progress.render(surface)

        # Volume slider
        if volume:
            self.__s_volume.render(surface)

        # FPS
        if config.GUI.show_fps():
            self.__frame_counter += 1
            current_time = time.time()
            if current_time >= self.__last_fps_update + 1.0: # ~ 1 second update interval
                delta = current_time - self.__last_fps_update
                self.__l_fps.set_text(str(round(self.__frame_counter / delta)))
                self.__frame_counter = 0
                self.__last_fps_update = current_time

            self.__l_fps.render(surface)

        # Download stats
        if config.GUI.show_download_stats():
            downloader = self.__context.get_downloader()
            self.__l_download_stats.set_text(
                f"[DOWNLOAD] Progress: {int(downloader.get_download_progress())} "
                + f"(failed: {downloader.get_number_of_failed_downloads()}, "
                + f"from permacache: {downloader.get_number_of_permacache_hits()}), "
                + f"DL time: {GUIOverlay.__format_time(downloader.get_time_downloading())},"
                + " Normalizing time: "
                + f"{GUIOverlay.__format_time(downloader.get_time_normalizing())}")
            self.__l_download_stats.render(surface)

    def resize(self, width: int, height: int):
        """Resizes the overlay elements to the given GUI dimensions"""
        self.__t_scoreboard.set_position(width - 50, height / 2)
        self.__p_lookahead_progress.set_position_and_dimensions(5, height - 25, width - 10, 20)
        self.__s_volume.set_position_and_dimensions(0, 0.2 * height, 30, 0.7 * height)
        if config.GUI.show_fps():
            self.__l_fps.set_position(width - 50, 20)
        if config.GUI.show_download_stats():
            self.__l_download_stats.set_position(width / 2, height - 15)

    def get_interaction_widgets(self, volume: bool) -> List[InteractionWidget]:
        """Returns a list of global GUI overlay elements that have mouse interaction depending
        on the argument defining whether a volume slider is displayed"""
        if volume:
            return [self.__s_volume]
        else:
            return []

    def get_volume(self) -> float:
        """Returns the current volume slider value `0.0 <= x <= 1.0`"""
        return self.__s_volume.get_value()

    def set_scoreboard_data(self, data: List[List[TableCell]]):
        """Sets the content of the scoreboard"""
        self.__t_scoreboard.set_content(data)

    @staticmethod
    def __format_time(time_: float) -> str:
        """Returns a string representing the given time (h, min, s)"""
        seconds = int(time_)
        minutes = seconds // 60
        hours = minutes // 60
        minutes %= 60
        seconds %= 60
        if hours > 0:
            return f"{hours}h{minutes}min{seconds}s"
        elif minutes > 0:
            return f"{minutes}min{seconds}s"
        else:
            return f"{seconds}s"
