# pylint: disable=cyclic-import

from typing import TYPE_CHECKING
import pygame

from misc import logger
from config import theme

from gui.label import Label

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Connecting(GameState):
    """
    Connecting state of the client.

    This state is initially entered and then left as soon as the server instructs the player to
    enter a lobby or wait for the next game.

    ## Interaction
    None
    """

    def __init__(self, context: 'GameContext'):
        """Constructs a new instance for the given context"""
        super().__init__(context)
        logger.info("Connecting...")

        self.__l_connect = Label(
            "Connecting to server...", theme.Text.big_size(), theme.Text.color())
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Connecting"

    # No custom operations when leaving state necessary
    # No continuing
    # No scoreboard
    # No lookahead progress
    # No volume slider

    def _render(self, surface: pygame.Surface):
        self.__l_connect.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_connect.set_position(width / 2, height / 2)

    # No interaction widgets
    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required
    # No time limit used in this state
    # Player states not relevant in this state
