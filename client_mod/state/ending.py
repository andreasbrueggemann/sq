# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, List
import pygame

from misc import logger
from misc.version import PROTOCOL_VERSION
from config import theme
from protocol.messages import JOIN_GAME

from gui.align import Align
from gui.button import Button
from gui.label import Label
from gui.interaction_widget import InteractionWidget
from gui.table import Table

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Ending(GameState):
    """
    Ending state of the client.

    In this state, the final scoreboard is shown to the player who may re-enter the lobby
    for the next game.

    ## Interaction
    After the server resets, the player may use a button to re-enter the lobby.
    """

    def __init__(self, context: 'GameContext'):
        """Constructs a new instance for the given context"""
        super().__init__(context)
        logger.info("Ending...")
        self.__reset = False
        """Whether reset was received and player may enter lobby for next game"""

        self.__l_title = Label("The end", theme.Text.big_size(), theme.Text.color())
        self.__t_scoreboard = Table(theme.Text.big_size(), Align.LEFT)
        self.__t_scoreboard.set_content(
            self.context().get_scoreboard_table_data(ending_formatting=True))

        # Already prepare continue button but delay displaying it
        self.__b_continue = Button()
        self.__b_continue.set_text("Continue")
        def callback(_):
            self.__reset = False # Do not allow to continue again before server replied
            self.context().reset()
            self.context().send(JOIN_GAME(self.context().get_player_name(), PROTOCOL_VERSION, 0))
        self.__b_continue.set_callback(callback)

        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Ending"

    # No custom operations when leaving state necessary
    # No continuing
    # No scoreboard
    # No lookahead progress
    # No volume slider

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)
        self.__t_scoreboard.render(surface)
        if self.__reset:
            self.__b_continue.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 8)
        self.__t_scoreboard.set_position(width / 5, height / 2)

        # Already prepare continue button but delay displaying it
        self.__b_continue.set_position_and_dimensions(width - 120, height - 120, 100, 80)

    def _get_interaction_widgets(self) -> List[InteractionWidget]:
        if self.__reset:
            return [self.__b_continue]
        else:
            return []

    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No time limit used in this state

    def reset(self):
        # Only register reset and execute it when player enters lobby
        self.__reset = True

    # Player states not relevant in this state
