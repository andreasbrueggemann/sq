# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Dict, List, Tuple
import time
import pygame

from misc.version import PROTOCOL_VERSION
from misc.song import Song
from misc.player_state import PlayerState, ConnectionState
from config import config, theme
from protocol.messages import JOIN_GAME, UPDATE_LOCAL_PROGRESS, SET_CONTINUE

from gui.interaction_widget import InteractionWidget
from gui.toggle_button import ToggleButton
from gui.progress import Progress

if TYPE_CHECKING:
    from .game_context import GameContext

# pylint: disable=pointless-string-statement
"""
### Default state transition diagram ###

                         ______________   enter_queue   ______________________
                         | Connecting | --------------> | WaitingForNextGame |
                         |____________|                 |____________________|
                              |                                 |
   +--------------------------+                                 |
   |                          |                                 | enter_lobby
   | wait_for_rehabilitation  | enter_lobby                     |
   v                          v                                 |
________________         ___________  <-------------------------+
| Reconnecting |         | Waiting |
|______________|         |_________|
   |                          |
   | rehabilitate_into_       | set_song_list
   | init_downloading         v
   +------------------>  ___________________
   |                     | InitDownloading |
   |                     |_________________|
   | request_song_index       |
   |                          | request_song_index
   |                          v
   +---------------+-->  _____________
                   |     | Searching | ------------+ request_song_index
                   |     |___________| <-----------+
                   |          |
                   |          + ------------------+------------------+
                   |          |                   |                  |
                   |          | enter_pause       |                  |
                   |          v                   |                  |
                   |     ___________              |                  |
                   |     | Pausing |              | play_song_index  |
                   |     |_________|              |                  |
                   |          |                   |                  |
                   |          | play_song_index   |                  |
                   |          v                   |                  |
                   |     _____________  <---------+                  |
                   |     | Listening |                               |
                   |     |___________|                               |
request_song_index |          |                                      |
                   |          +---------+                            |
                   |          |         |                            |
                   |          | guess   |                            | end
                   |          v         | solve                      |
                   |     ____________   |                            |
                   |     | Guessing |   |                            |
                   |     |__________|   |                            |
                   |          |         |                            |
                   |          | solve   |                            |
                   |          v         |                            |
                   |     ___________  <-+                            |
                   |     | Solving |-----------+ solve               |
                   |     |_________|<----------+                     |
                   |          |                                      |
                   +----------+                                      |
                              |                                      |
                              | end                                  |
                              v                                      |
                         __________  <-------------------------------+
                         | Ending |
                         |________|


### State transition diagram for 'no_buzzers' mode ###


                                 ______________   enter_queue   ______________________
                                 | Connecting | --------------> | WaitingForNextGame |
                                 |____________|                 |____________________|
                                      |                                 |
   +----------------------------------+                                 |
   |                                  |                                 | enter_lobby
   | wait_for_rehabilitation          | enter_lobby                     |
   v                                  v                                 |
________________                 ___________  <-------------------------+
| Reconnecting |                 | Waiting |
|______________|                 |_________|
   |                                  |
   | rehabilitate_into_               | set_song_list
   | init_downloading                 v
   +------------------>          ___________________
   |                             | InitDownloading |
   |                             |_________________|
   | request_song_index               |
   |                                  | request_song_index
   |                                  v
   +----------------->-----+-->  _____________
   |                       |     | Searching | ------------+ request_song_index
   |                       |     |___________| <-----------+
   |                       |          |
   |                       |          + ------------------+------------------+
   |                       |          |                   |                  |
   |                       |          | enter_pause       |                  |
   |                       |          v                   |                  |
   |                       |     ___________              |                  |
   |                       |     | Pausing |              | play_song_index  |
   |                       |     |_________|              |                  |
   |                       |          |                   |                  |
   |                       |          | play_song_index   |                  |
   |    request_song_index |          v                   |                  |
   |                       |     _____________  <---------+                  |
   |                       |     | Listening |                               |
   |                       |     |___________|                               | solve_specific_song
   |                       |          |                                      |
   | solve_specific_song   |          | neutral_guess                        |
   |                       |          v                                      |
   |                       |     ____________                                |
   |                       |     | Guessing |                                |
   |                       |     |__________|                                |
   |                       |          |                                      |
   |                       +----------+                                      |
   |                                                                         |
   |                                                                         |
   +------------------>  ___________ <---------------------------------------+
                         | Solving |-----------+ solve_specific_song, solve
                         |_________|<----------+
                              |
                              | end
                              v
                         __________
                         | Ending |
                         |________|
"""

class GameState:
    """Superclass of all states of the client handling the GUI"""

    def __init__(self, context: 'GameContext'):
        """
        Constructs a new instance for the given context.

        This constructor must be called by the constructors of all subclasses.
        """

        context.leave_state()
        self.__context = context

        # Continue (handle here instead of game context as there exist multiple variants)
        can_continue, continue_str, _, _ = self.can_continue()
        if can_continue:
            self.__continue_b_input = ToggleButton()
            self.__continue_b_input.set_text(continue_str)
            self.__continue_b_input.set_callback(lambda on : self.context()
                                            .send(SET_CONTINUE(on, self.get_state_name())))
            self.__continue_p_state = Progress()

    ### Abstract methods (some with default behaviour) ###

    @staticmethod
    def get_state_name() -> str:
        """Returns the name of the state"""
        raise NotImplementedError()

    def leave_state(self):
        """Handler for leaving the state"""

    def can_continue(self) -> Tuple[bool, str, bool, str]:
        """
        Returns whether the state shall render a continue functionality consisting of button and
        progress.
        If so, the second return is the string to be shown on the continue button and the
        third return is whether continue button and progress are to be displayed separated or not.
        The fourth return is the code shown in the scoreboard for players who wish to continue
        """

        return (False, "", False, "")

    @staticmethod
    def _has_scoreboard() -> bool:
        """Returns whether the state shall render a scoreboard"""
        return False

    @staticmethod
    def _has_lookahead_progress() -> bool:
        """Returns whether the state shall render a lookahead progress for the current progress
            of the download"""
        return False

    @staticmethod
    def _has_volume_slider() -> bool:
        """Returns whether the state shall render a volume slider"""
        return False

    def _render(self, surface: pygame.Surface):
        """Renders the specific parts of the current state's GUI on the given surface"""
        raise NotImplementedError()

    def _resize(self, width: int, height: int):
        """Resizes the specific parts of the current state's GUI to the given dimensions"""
        raise NotImplementedError()

    def _get_interaction_widgets(self) -> List[InteractionWidget]:
        """Returns a list of GUI widgets that have mouse interaction and are specific to the
            current state"""
        return []

    def _poll(self):
        """Poll the gamestate, e.g., for timed events, for actions specific to the current state"""

    ### Further methods ###

    def context(self) -> 'GameContext':
        """Returns the context of the state"""
        return self.__context

    def join(self, username: str):
        """Joins the game using the given username"""
        self.context().set_player_name(username)
        self.context().send(JOIN_GAME(username, PROTOCOL_VERSION, self.context().get_player_id()))

    def render(self, surface: pygame.Surface):
        """Renders the GUI on the given surface"""
        surface.fill(theme.GameWindow.color())
        self._render(surface)
        self.context().get_gui_overlay().render(surface, self._has_scoreboard(),
                                        self._has_lookahead_progress(), self._has_volume_slider())

        # Continue
        can_continue, _, _, _ = self.can_continue()
        if can_continue:
            self.__continue_b_input.render(surface)
            self.__continue_p_state.render(surface)

    def resize(self, width: int, height: int):
        """Resizes the GUI to the given dimensions"""
        self._resize(width, height)
        self.context().get_gui_overlay().resize(width, height)

        # Continue
        can_continue, _, continue_separate, _ = self.can_continue()
        if can_continue:
            if continue_separate:
                self.__continue_b_input.set_position_and_dimensions(
                    width * 3 / 4, height * 7 / 8, width / 3, 80, centered = True)
                self.__continue_p_state.set_position_and_dimensions(
                    width / 4, height * 7 / 8, width / 3, 80, centered = True)
            else:
                self.__continue_b_input.set_position_and_dimensions(
                    width - 120, height - 120, 100, 80)
                self.__continue_p_state.set_position_and_dimensions(
                    width - 120, height - 40, 100, 10)

    def __get_interaction_widgets(self) -> List[InteractionWidget]:
        """Returns a list of GUI widgets that have mouse interaction"""
        interaction_widgets = self._get_interaction_widgets() \
                            + self.context().get_gui_overlay().get_interaction_widgets(
                                                                        self._has_volume_slider())

        # Continue
        can_continue, _, _, _ = self.can_continue()
        if can_continue:
            interaction_widgets.append(self.__continue_b_input)

        return interaction_widgets

    def handle_interaction(self, surface: pygame.Surface) -> Tuple[pygame.Surface, bool]:
        """Handles GUI interaction and returns a potentially updated surface and whether the GUI
            was closed"""
        closed = False
        (m_x, m_y) = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                closed = True
            elif event.type == pygame.VIDEORESIZE:
                surface = pygame.display.set_mode((event.w, event.h), pygame.RESIZABLE)
                self.context().set_window_dimensions(event.w, event.h)
                self.resize(event.w, event.h)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1 or config.GUI.allow_all_buttons():
                    for interaction_widget in self.__get_interaction_widgets():
                        interaction_widget.mouse_down(m_x, m_y)
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1 or config.GUI.allow_all_buttons():
                    for interaction_widget in self.__get_interaction_widgets():
                        interaction_widget.mouse_up(m_x, m_y)
            elif event.type == pygame.MOUSEMOTION:
                for interaction_widget in self.__get_interaction_widgets():
                    interaction_widget.mouse_movement(m_x, m_y)

        return surface, closed

    def poll(self):
        """Poll the gamestate, e.g., for timed events"""
        if self.context().get_downloader().is_downloading():
            self.context().set_downloading(True)
            elapsed = time.time() - self.context().get_last_progress_sync()
            sync = elapsed > config.Timeouts.progress_sync_interval()
            if sync:
                self.context().update_last_progress_sync()
                self.context().send(
                    UPDATE_LOCAL_PROGRESS(self.context().get_downloader().get_download_progress()))
        elif self.context().is_downloading(): # Send final progress
            self.context().set_downloading(False)
            self.context().send(
                UPDATE_LOCAL_PROGRESS(self.context().get_downloader().get_download_progress()))

        self._poll()

    ### Abstract methods to handle incoming messages from the server, some with default behaviour###

    def request_song_index(self, index: int):
        """Song at the given index is requested"""
        # This is overwritten by the `Searching` state.
        # The default implementation here is necessary to let the first request in one state cycle
        # enter the `Searching` state.
        # The specific state then does further handling.
        # Import here to prevent circular imports
        from client_mod.state.searching import Searching
        self.context().transition(Searching(self.context(), index))

    def solve(self, assigner_id: int, substitute: bool):
        """Current song is solved with the given player id assigning points which could be a
            substitute if the buzzering player disconnected"""
        # This is overwritten by the `Solving` state as solving can be started multiple times for
        # the same song.
        # This is the case if the original point assigner disconnects and a substitute is chosen.
        # In this case, no new state shall be constructed as old state is to be kept.
        # Import here to prevent circular imports
        from .solving import Solving
        self.context().transition(Solving(self.context(), assigner_id, substitute))

    def set_point_assignment(self, assignment: Dict[int, int]):
        """Point assignment mapping player ids to their assigned points is set"""

    def reset(self):
        """Game is reset"""
        self.context().reset()

    def start_time_limit(self, time_limit: float):
        """Time limit has been started with the given remaining time"""

    ### Abstract methods to handle incoming messages from the server ###
    ### after the `GameState` super class already did some handling ###

    def _update_player_states(self):
        """Handle an update of the player states"""

    ### Non-abstract methods for handling incoming messages from the server ###

    def set_id(self, player_id: int):
        """ID of player is set"""
        self.context().set_player_id(player_id)

    def provide_config(self, lookahead: int, no_buzzers: bool, maximum_score_per_song: int):
        """Relevant server side config is provided"""
        self.context().set_lookahead(lookahead)
        self.context().set_no_buzzers(no_buzzers)
        self.context().set_maximum_score_per_song(maximum_score_per_song)

    def wait_for_rehabilitation(self, songs: List[Song]):
        """Player is told to wait for rehabilitation"""
        # Import here to prevent circular imports
        from .reconnecting import Reconnecting
        self.context().transition(Reconnecting(self.context(), songs))

    def enter_lobby(self):
        """Player is told to enter lobby"""
        # Import here to prevent circular imports
        from .waiting import Waiting
        self.context().transition(Waiting(self.context()))

    def enter_queue(self):
        """Player is told to enter the queue to wait for the next game"""
        # Import here to prevent circular imports
        from .waiting_for_next_game import WaitingForNextGame
        self.context().transition(WaitingForNextGame(self.context()))

    def set_player_states(self, player_states: List[PlayerState]):
        """Player states are updated (e.g., somebody continues, joins, gets a new score, ...)"""
        self.context().set_player_states(player_states)
        self._update_player_states()

        # Manage continue
        can_continue, _, _, _ = self.can_continue()
        if can_continue:
            active_players = sum(s.connection == ConnectionState.CONNECTED for s in player_states)
            cont_players = sum(
                s.connection == ConnectionState.CONNECTED and s.cont for s in player_states)
            if active_players > 0:
                self.__continue_p_state.update_progress(cont_players / active_players)
            else:
                self.__continue_p_state.update_progress(0.0)
            # Update own continue, needed if for example
            # point assignments reset continue state of all players
            for s in player_states:
                if s.id == self.context().get_player_id():
                    self.__continue_b_input.set_pressed(s.cont)
                    break

    def set_song_list(self, songs: List[Song]):
        """Song list is set and player has to start downloading"""
        # Import here to prevent circular imports
        from .init_downloading import InitDownloading
        self.context().transition(InitDownloading(self.context(), songs))

    def set_global_progress(self, progress: float):
        """Global download progress is set to the given value"""
        self.context().set_progress(progress)

    def enter_pause(self):
        """Pause is entered"""
        # Import here to prevent circular imports
        from .pausing import Pausing
        self.context().transition(Pausing(self.context()))

    def play_song_index(self, index: int, timestamp: float):
        """Song at the given index is played starting with the given timestamp"""
        # Import here to prevent circular imports
        from .listening import Listening
        self.context().transition(Listening(self.context(), index, timestamp))

    def guess(self, player_name: str, player_id: int):
        """Guessing is entered by the given player buzzering"""
        # Import here to prevent circular imports
        from .guessing import Guessing
        self.context().transition(Guessing(self.context(), player_name, player_id))

    def neutral_guess(self):
        """Guessing is entered in 'no buzzer' mode"""
        # Import here to prevent circular imports
        from .guessing import Guessing
        self.context().transition(Guessing(self.context(), None, None))

    def solve_specific_song(self, song_index: int, assigner_id: int, substitute: bool):
        """Specific song at the given index is solved with the given player id assigning points
            which could be a substitute if the buzzering player disconnected"""
        # Import here to prevent circular imports
        from .solving import Solving
        self.context().transition(Solving(self.context(), assigner_id, substitute,
                                          specific_song_index = song_index))

    def end(self):
        """Game is ended"""
        # Import here to prevent circular imports
        from .ending import Ending
        self.context().transition(Ending(self.context()))

    def rehabilitate_into_init_downloading(self):
        """Player is rehabilitated into initial downloading"""
        # Import here to prevent circular imports
        from .init_downloading import InitDownloading
        self.context().transition(InitDownloading(self.context(), None))
