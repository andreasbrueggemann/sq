# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Tuple
import pygame

from misc import logger
from config import theme

from gui.label import Label
from gui.table import Table

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class Pausing(GameState):
    """
    Pausing state of the client.

    In this state, all players wait until all agree to continue.

    ## Interactions
    The player can declare if they are ready or not.
    The list of other players and whether they wish to update is shown continuously.
    """

    def __init__(self, context: 'GameContext'):
        """Constructs a new instance for the given context"""
        super().__init__(context)
        logger.info("Pausing game...")

        self.__l_title = Label("Ready to continue?", theme.Text.big_size(), theme.Text.color())
        self.__t_players = Table(theme.Text.small_size())
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "Pausing"

    # No custom operations when leaving state necessary

    def can_continue(self) -> Tuple[bool, str, bool, str]:
        return (True, "Ready", True, "R")

    @staticmethod
    def _has_scoreboard() -> bool:
        return True

    @staticmethod
    def _has_lookahead_progress() -> bool:
        return True

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)
        self.__t_players.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 8)
        self.__t_players.set_position(width / 8, height / 2)
        self._update_player_states()

    # No interaction widgets
    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required
    # No time limit used in this state

    def _update_player_states(self):
        self.__t_players.set_content(self.context().get_continueboard_table_data())
