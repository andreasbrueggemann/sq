# pylint: disable=cyclic-import

from typing import TYPE_CHECKING, Tuple
from pathlib import Path
import shutil
import pygame

from misc import logger, volume_translator
from config import config, theme
from client_mod.downloader import TEMP_LOBBY_MUSIC_FILE

from gui.label import Label

from .game_state import GameState
if TYPE_CHECKING:
    from .game_context import GameContext

class WaitingForNextGame(GameState):
    """
    Waiting for next game state of the client.

    In this state, the client is in a queue for the next game while a game is still running and is
    notified to enter a lobby as soon as the last game resets.

    ## Interaction
    None
    """

    def __init__(self, context: 'GameContext'):
        """Constructs a new instance for the given context"""
        super().__init__(context)
        logger.info("Waiting for next game...")

        if Path(config.Files.lobby_music()).is_file():
            pygame.mixer.music.unload()
            # Copy to temp file as downloader may overwrite original file while being streamed
            # otherwise
            temp_file = f"{config.Download.download_path()}/{TEMP_LOBBY_MUSIC_FILE}"
            shutil.copyfile(config.Files.lobby_music(), temp_file)
            pygame.mixer.music.load(temp_file)
            volume_translator.set_volume(self.context().get_volume())
            pygame.mixer.music.play(loops = -1) # play forever

        self.__l_title = Label("Game is running! Waiting for the next game...",
                               theme.Text.big_size(), theme.Text.color())
        self.resize(self.context().get_window_width(), self.context().get_window_height())

    ### Implementation of abstract methods ###

    @staticmethod
    def get_state_name() -> str:
        return "WaitingForNextGame"

    # No custom operations when leaving state necessary

    def can_continue(self) -> Tuple[bool, str, bool, str]:
        return (False, "", False, "")

    # No scoreboard
    # No lookahead progress

    @staticmethod
    def _has_volume_slider() -> bool:
        return True

    def _render(self, surface: pygame.Surface):
        self.__l_title.render(surface)

    def _resize(self, width: int, height: int):
        self.__l_title.set_position(width / 2, height / 8)

    # No interaction widgets
    # No polling required

    ### Implementation of abstract methods for handling incoming messages from a player ###

    # Song index request not handled by this state
    # No custom solve handling
    # No setting of point assignment expected in this state
    # No custom reset handling required
    # No time limit used in this state
    # No player states relevant in this state
