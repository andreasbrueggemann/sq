from typing import Tuple, Optional, Callable
import tkinter as tk
from tkinter import ttk

from config import theme
from misc.version import GAME_VERSION

class TkWindow:
    """
    Base class for windows using Tk, that are launcher, crash reporter and similar.
    """

    def __init__(self, window_title: str, content_title: str, window_geometry: Tuple[int, int]):
        """Constructs a new instance for the given window title and title shown in the content
            and the given window geometry (width, height)"""
        self._window = tk.Tk()
        self._window.title(f"SQ ver. {GAME_VERSION} - {window_title}")
        self._window.geometry(theme.size_to_string(*window_geometry))

        # Make that exceptions inside callbacks are propagated to main thread
        def exception_catch_disabler(self, exception, *args):
            raise exception
        self._window.report_callback_exception = exception_catch_disabler

        title = tk.Label(
            self._window,
            text = content_title,
            bg = theme.Tk.title_background_color(),
            fg = theme.Tk.title_font_color(),
            font = (theme.Tk.title_font_name(), theme.Tk.title_font_size())
        )
        title.pack(
            ipadx = theme.Tk.i_pad_x(),
            ipady = theme.Tk.i_pad_y(),
            fill = "both",
            expand = True
        )

        self.__content = ttk.Frame(self._window)
        self.__content.pack(
            padx = theme.Tk.pad_x(),
            pady = theme.Tk.pad_y(),
            fill = "x",
            expand = True
        )

    def _add_background_task(self, task: Callable[[], None]):
        """Adds a background task that is repeatet in some fixed time interval"""
        def next_run():
            task()
            self._window.after(100, next_run)
        self._window.after(100, next_run)

    def _get_content_frame(self) -> ttk.Frame:
        """Returns the content frame of the window"""
        return self.__content

    def _add_label(self, text: str):
        """Adds a label to the content frame"""
        label = tk.Label(
            self._get_content_frame(),
            text = text,
        )
        label.pack(
            ipadx = theme.Tk.i_pad_x(),
            ipady = theme.Tk.i_pad_y(),
            fill = "x",
        )

    def _add_text_display_area(self, text: str):
        """
        Adds an area displaying the given text to the content frame.

        In contrast to `_add_label`, the text area automatically breaks the text to fit its width.
        """

        textarea = tk.Text(
            self._get_content_frame(),
            height = 18
        )
        textarea.pack(
            ipadx = theme.Tk.i_pad_x(),
            ipady = theme.Tk.i_pad_y(),
            fill = "both",
            expand = True
        )
        textarea.insert(tk.INSERT, text)
        textarea["state"] = "disabled"

    def _add_labeled_progress(self, text: str, spinner_mode: bool = False) -> ttk.Progressbar:
        """
        Adds a progressbar and corresponding label to the content frame and returns the progress bar
        for further handling.

        If `spinner_mode` is set to `True`, the progressbar does only indicate a running process
        instead of giving actual progress information.
        """

        label = ttk.Label(
            self._get_content_frame(),
            text = text
        )
        label.pack(fill = "x")
        progressbar = ttk.Progressbar(
            self._get_content_frame(),
            orient = tk.HORIZONTAL,
            length = 100,
            mode = "indeterminate" if spinner_mode else "determinate"
        )
        progressbar.pack(
            ipadx = theme.Tk.i_pad_x(),
            ipady = theme.Tk.i_pad_y(),
            fill = "x"
        )
        return progressbar

    def _add_labeled_input(self, label_text: str, input_preset_text: Optional[str]) -> ttk.Entry:
        """Adds a text input and corresponding label to the content frame and returns the input for
            further handling"""
        label = ttk.Label(
            self._get_content_frame(),
            text = label_text,
        )
        label.pack(fill = "x")
        entry = ttk.Entry(self._get_content_frame())
        if input_preset_text is not None:
            entry.insert(0, input_preset_text)
        entry.pack(
            ipadx = theme.Tk.i_pad_x(),
            ipady = theme.Tk.i_pad_y(),
            fill = "x"
        )
        return entry

    def _add_separator(self, padding: int):
        """Adds a separator to the content frame using the given y padding"""
        separator = ttk.Separator(
            self._get_content_frame(),
            orient = "horizontal"
        )
        separator.pack(
            fill = "x",
            pady = padding
        )

    def _add_button(self, text: str, callback: Callable[[], None]) -> tk.Button:
        """Adds a button for the given text content and with the given callback to the content frame
            and returns it"""
        button = tk.Button(
            self._get_content_frame(),
            text = text,
            bg = theme.Tk.button_background_color(),
            fg = theme.Tk.button_font_color(),
            command = callback
        )
        button.pack(
            ipadx = theme.Tk.i_pad_x(),
            ipady = theme.Tk.i_pad_y(),
            fill = "x",
            pady = theme.Tk.pad_y()
        )
        return button
