from config import theme

from .tk_window import TkWindow

class CrashReporter(TkWindow):
    """GUI for showing crash reports after a game crashed"""

    def __init__(self, message: str):
        """Constructs a new instance for the given crash message and enters the GUI main loop which
            is only left when the window is closed"""
        super().__init__(
            "Crash Report",
            "Seems like SQ crashed...",
            theme.WindowSize.crash_reporter()
        )

        self._add_text_display_area(message)

        self._window.mainloop()
