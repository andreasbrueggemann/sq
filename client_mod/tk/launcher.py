from typing import Tuple

from config import config, theme

from .tk_window import TkWindow

class Launcher(TkWindow):
    """Launcher for SQ that offers a GUI to input server IP, port and username and allows to run
        the proactive download"""

    def __init__(self):
        """Constructs a new instance and enters the GUI main loop which is only left if the launcher
            is closed or a proceeding action is chosen"""
        super().__init__(
            "Launcher",
            "SQ launcher - Select a server:",
            theme.WindowSize.launcher()
        )

        address_e = self._add_labeled_input(
            "Server address:",
            config.Network.ip_address()
        )
        port_e = self._add_labeled_input(
            "Server port:",
            config.Network.port()
        )
        name_e = self._add_labeled_input(
            "Your name:",
            config.Gameplay.username()
        )

        def callback():
            self.__submit(address_e.get().strip(), int(port_e.get().strip()), name_e.get().strip())
        self._add_button(
            "Connect!",
            callback
        )

        self._add_separator(3)

        self._add_button(
            "Just download songs",
            self.__submit_download
        )
        self.__just_download = False
        self._add_button(
            "Run Selftest",
            self.__submit_selftest
        )
        self.__run_selftest = False

        self.__submitted = False

        self._window.mainloop()

    def __submit(self, address: str, port: int, name: str):
        """Handler for when the player wishes to connect to given server and port using the
            given name"""
        self.__just_download = False
        self.__address = address
        self.__port = port
        self.__name = name
        if len(name) > 0 and len(address) > 0:
            self.__submitted = True
            self._window.destroy()

    def __submit_download(self):
        """Handler for when the player wishes to run a proactive download"""
        self.__just_download = True
        self.__submitted = True
        self._window.destroy()

    def __submit_selftest(self):
        """Handler for when the player wishes to manually run a selftest"""
        self.__run_selftest = True
        self.__submitted = True
        self._window.destroy()

    def is_submitted(self) -> bool:
        """Returns whether user inputs were submitted"""
        return self.__submitted

    def is_proactive_download_chosen(self) -> bool:
        """Returns whether the proactive download option was chosen after running the launcher"""
        return self.__just_download

    def is_selftest_chosen(self) -> bool:
        """Returns whether the manual selftest option was chosen after running the launcher"""
        return self.__run_selftest

    def get_connection_info(self) -> Tuple[str, int, str]:
        """Returns server IP, server port and username to use to join a game;
        only works after running the launcher if no proactive download was chosen"""
        return (self.__address, self.__port, self.__name)
