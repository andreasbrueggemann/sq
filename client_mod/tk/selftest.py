from typing import Optional
import threading
import os
import base64
from enum import Enum
import pygame

from config import config, theme
from misc.version import GAME_VERSION, PERMACACHE_VERSION
from misc.song import Song, SongStatus
from misc import logger

from ..downloader import Downloader
from ..errors import CustomMessageError
from .tk_window import TkWindow

class SelftestState(Enum):
    """Current testing state of the selftest, see steps as documented for class `Selftest`"""
    DOWNLOAD = 0
    """Downloading a song"""
    NORMALIZE = 1
    """Normalizing the downloaded song's volume"""
    PERMACACHE_UPDATE = 2
    """Updating the permacache"""
    PLAYBACK = 3
    """Downloaded song is played"""

PERMACACHE_VERSION_FILENAME = "version.ini"
"""Name of the permacache version file inside the permacache directory"""

class Selftest(TkWindow):
    """
    Selftest of SQ that tests downloading, volume normalization, audio and updates the permacache
    if necessary. Specific methods can be used to determine if the selftest should be started.

    The steps of the selftest are as follows:
    * A song is downloaded to test yt_dlp, the internet connection and the presence of ffmpeg
    * The song's volume is being normalized checking the CLI interface to ffmpeg
    * The permacache is updated if the file naming scheme changed
    * The downloaded song is played in the background to test the audio configuration
    """

    @staticmethod
    def is_necessary() -> bool:
        """
        Returns if a selftest is necessary.

        This is the case if another version of SQ is used compared to the previous one or if the
        permacache version changed.
        Note that also checking the permacache version is necessary as a user of one SQ version
        could try to import an old permacache folder of another version.
        """

        if Selftest.__get_last_used_version() != GAME_VERSION:
            logger.info("Selftest is necessary as last SQ version was "
                        + f"{Selftest.__get_last_used_version()} and current SQ version is "
                        + f"{GAME_VERSION}")
            return True
        if Selftest.__get_permacache_version() != PERMACACHE_VERSION:
            logger.info("Selftest is necessary as current permacache version "
                        + f"{Selftest.__get_permacache_version()} needs to be updated to "
                        + f"{PERMACACHE_VERSION}")
            return True
        return False

    @staticmethod
    def __get_last_used_version() -> str:
        """Returns the last known version that SQ was started with"""
        try:
            with open(config.Files.version(), "r", encoding="utf-8") as version_file:
                return version_file.read().strip()
        except FileNotFoundError:
            return "Beta 1.0.0"

    @staticmethod
    def __get_permacache_version() -> int:
        """Returns the version of the current permacache"""
        try:
            with open(f"{config.Download.permacache_path()}/{PERMACACHE_VERSION_FILENAME}",
                        "r", encoding="utf-8") as version_file:
                return int(version_file.read().strip())
        except FileNotFoundError:
            return 1

    @staticmethod
    def __update_last_used_version():
        """Updates the last known version that SQ was started with to the current version"""
        with open(config.Files.version(), "w", encoding="utf-8") as version_file:
            version_file.write(GAME_VERSION)

    @staticmethod
    def __update_permacache_version():
        """Updates the permacache version file to the current version"""
        with open(f"{config.Download.permacache_path()}/{PERMACACHE_VERSION_FILENAME}",
                        "w", encoding="utf-8") as version_file:
            version_file.write(str(PERMACACHE_VERSION))

    def __init__(self):
        """Runs the selftest GUI"""
        super().__init__(
            "Selftest",
            "SQ selftest",
            theme.WindowSize.selftest()
        )

        self.__download_p = self._add_labeled_progress("Testing download...")
        self.__normalization_p = self._add_labeled_progress(
            "Testing volume normalization...",
            spinner_mode = True
        )
        self.__permacache_p = self._add_labeled_progress("Updating permacache...")

        self._add_separator(10)

        self.__confirm_audio_b = self._add_button(
            "",
            self.__confirm
        )
        self.__confirm_audio_b["bg"] = "gray"
        self.__confirm_audio_b["state"] = "disabled"

        self.__permacache_updater: Optional[threading.Thread] = None
        self.__permacache_updater_exception: Optional[Exception] = None
        self.__downloader = Downloader(selftest_mode = True)
        self.__downloader.init_queue([Song("", "", config.Download.selftest_song(), "")], False)
        self.__state = SelftestState.DOWNLOAD

        self.__succeeded = False

        self._add_background_task(self.__update)
        self._window.mainloop()

    def __update(self):
        """Manages the background tasks' state and updates the GUI accordingly"""

        if self.__state == SelftestState.DOWNLOAD:
            self.__download_p["value"] = 100 * self.__downloader.get_download_progress()
            if self.__downloader.is_normalizing():
                self.__state = SelftestState.NORMALIZE
                self.__normalization_p.start(10)

        if self.__state == SelftestState.NORMALIZE:
            if not self.__downloader.is_normalizing():
                self.__normalization_p.stop()
                self.__normalization_p["value"] = 100

                # Test if download failed
                if not self.__downloader.is_downloading() and \
                        self.__downloader.fetch_at_index(0).status == SongStatus.FAILED:
                    self._window.destroy()
                    raise CustomMessageError("Downloading song from "
                                            + f"{config.Download.selftest_song()} failed, please "
                                            + "test manually if the URL works in your browser")

                # Start permacache update
                current_version = Selftest.__get_permacache_version()
                logger.info(f"Present permacache has version {current_version}, SQ requires version"
                        + f" {PERMACACHE_VERSION}")
                if current_version > PERMACACHE_VERSION:
                    self._window.destroy()
                    raise CustomMessageError("Cannot start old version with newer permacache")
                elif current_version < PERMACACHE_VERSION:
                    logger.info("Updating permacache...")
                    self.__permacache_updater = threading.Thread(
                        target = self.__permacache_update_thread,
                        daemon = False,
                        args = [current_version])
                    self.__permacache_updater.start()
                    self.__state = SelftestState.PERMACACHE_UPDATE
                else:
                    logger.info("Thus, no update necessary")
                    self.__permacache_p["value"] = 100
                    self.__start_playback()
                    self.__state = SelftestState.PLAYBACK

        if self.__state == SelftestState.PERMACACHE_UPDATE:
            # Crash if permacache updater crashes
            if self.__permacache_updater_exception is not None:
                raise self.__permacache_updater_exception
            # Proceed if updater thread finished
            if not self.__permacache_updater.is_alive():
                self.__start_playback()
                self.__state = SelftestState.PLAYBACK

    def __permacache_update_thread(self, old_version: int):
        """
        Thread to update the permacache.

        This is no daemon thread. Interrupting it may corrupt the entire permacache.
        """

        try:
            files = [file for file in os.listdir(config.Download.permacache_path()) \
                                    if file not in [PERMACACHE_VERSION_FILENAME, ".gitignore"]]
            for i, file in enumerate(files):
                Selftest.__update_permacache_file(file, old_version)
                self.__permacache_p["value"] = 100 * (i + 1) / len(files)

            self.__permacache_p["value"] = 100
            Selftest.__update_permacache_version()
        except Exception as error: # pylint: disable=broad-except
            self.__permacache_updater_exception = error
            # thread terminates

    @staticmethod
    def __update_permacache_file(file: str, old_version: int):
        """Updates a single permacache file to the new permacache version"""
        if old_version == 1 and PERMACACHE_VERSION == 2:
            # Change names from youtube video id to base32 encoding of video ids to fix problems
            # with case insensitive file systems
            if not file.endswith(".ogg"):
                logger.warning(f"Unexpected file {file} in permacache folder")
                return
            video_id = file[:-4]
            encoded_id = base64.b32encode(video_id.encode('utf-8')).decode('utf-8')
            os.rename(f"{config.Download.permacache_path()}/{file}",
                        f"{config.Download.permacache_path()}/{encoded_id}.ogg")
            return

        raise ValueError(f"Unknown permacache update procedure from {old_version} to "
                        + f"{PERMACACHE_VERSION}")

    def __start_playback(self):
        """Starts the song audio playback"""
        pygame.init()
        pygame.mixer.music.load(self.__downloader.fetch_at_index(0).filename)
        pygame.mixer.music.play(loops = -1) # Loop forever
        self.__confirm_audio_b["text"] = "Click if you hear a song now"
        self.__confirm_audio_b["state"] = "normal"
        self.__confirm_audio_b["bg"] = theme.Tk.button_background_color()
        self.__succeeded = True

    def __confirm(self):
        """Callback for the confirm audio button that ends the selftest"""
        logger.info("Selftest succeeded")
        Selftest.__update_last_used_version()
        pygame.mixer.music.stop()
        self._window.destroy()

    def has_succeeded(self) -> bool:
        """Returns if the selftest has succeeded"""
        return self.__succeeded
