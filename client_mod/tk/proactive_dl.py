from typing import List, Optional

from config import config, theme
from misc import logger, song_importer
from misc.song import Song

from ..downloader import Downloader
from ..errors import CustomMessageError
from .tk_window import TkWindow

class ProactiveDL(TkWindow):
    """
    Proactive downloader for SQ that offers a GUI to download songs without the context
    of a running game.

    The downloader fills the permacache by querying songs by itself to decrease download
    times in future game sessions.
    The user can enter a number of songs (or 0 representing all songs) to query from
    the database specified in their config.
    From the resulting list of songs, all songs that are not already present in the
    permacache are downloaded and added to the permacache.
    Thus, the number of downloaded songs may be less than the number of queried songs
    due to songs already being cached beforehand.
    """

    def __init__(self):
        """Constructs and runs a new instance"""
        super().__init__(
            "Downloader",
            "Proactively download songs",
            theme.WindowSize.proactive_dl()
        )

        self._add_label("Randomly sample n songs and start to download them.\n"
                        + "Set n=0 to sample all songs.")

        self.__number_of_songs_e = self._add_labeled_input(
            "n / Number of songs to sample:",
            "0"
        )

        self._add_button(
            "Start download",
            self.__start_download
        )
        self._add_button(
            "Stop download",
            self.__stop_download
        )

        self._add_separator(3)

        self.__work_p = self._add_labeled_progress(
            "Status:",
            spinner_mode = True
        )
        self.__progress_p = self._add_labeled_progress("Progress:")

        self.__downloader = Downloader(write_to_permacache = True)
        """Downloader daemon"""
        self.__songs: Optional[List[Song]] = None

        self._add_background_task(self.__progress_update)
        self._window.mainloop()

    def __start_download(self):
        """Start to download songs given the inputs from the GUI"""
        if not self.__downloader.is_downloading(): # Ignore otherwise
            if config.Database.url() is None or config.Database.username() is None \
                    or config.Database.password() is None:
                self._window.destroy()
                raise CustomMessageError("Missing database credentials in client.ini")

            number_songs = int(self.__number_of_songs_e.get().strip())

            logger.info("Fetching songs from database...")
            try:
                json_songs = song_importer.fetch_songs(number_songs)
                self.__songs = [Song.from_dict(song) for song in json_songs]
                logger.info(f"Requested {number_songs} songs, got {len(self.__songs)} songs")

                self.__downloader.init_queue(self.__songs, False)
                self.__work_p.start(10)
            except ConnectionError as error:
                self._window.destroy()
                raise CustomMessageError(str(error)) from error

    def __stop_download(self):
        """Stops the current download if running"""
        if self.__downloader.is_downloading():
            self.__downloader.stop()
            self.__work_p.stop()

    def __progress_update(self):
        """Updates the currently shown download progress"""
        if self.__songs is None:
            self.__progress_p["value"] = 0
        else:
            self.__progress_p["value"] = 100 * self.__downloader.get_download_progress() \
                                            / len(self.__songs)
        if not self.__downloader.is_downloading():
            self.__work_p.stop()
