from config import theme

from .tk_window import TkWindow

class InfoDialog(TkWindow):
    """GUI for showing an info dialog window"""

    def __init__(self, message: str):
        """Constructs a new instance for the given message and enters the GUI main loop which is
            only left when the window is closed"""
        super().__init__(
            "Info",
            "Info",
            theme.WindowSize.info()
        )

        self._add_text_display_area(message)

        self._window.mainloop()
