# Beta 1.2.0 (Protocol Version 3) --- 2023-11-18
Multiple gameplay updates and new gamemode
## Additions
- Added new 'No Buzzers' gamemode where nobody can buzzer, all players write their solutions down and points are only assigned at the end of the game (#45)
- buzzer_slack_compensation_mode option which, if buzzer-slack is enabled, selects player with lowest score who buzzered instead of random player (#44)
- Time limit for listening to a song until it is skipped if nobody buzzers (including option to start timer only if enough players wish to continue) (#42)
- Time limit for guessing a song after buzzering (including mode to automatically continue after that) (#25)
- Added queue for the next game for players who try to connect while a game is already running
## Changes
- Scoreboard now also displays who wants to continue/skip/... (#26)
- Reformatted scoreboard to use proper columns (#33) and coloring (part of #37)
- Reworked formatting of continuing player lists and players in score assigning, also included colors there (#37)
- Maximum number of points to assign for one song can now be freely configured by the server
- Server now cuts off too long names (#33)
- Minor cleanups in protocol etc.
- Removed Herobrine
## Bug Fixes
- Fixed bug where the server crashes in the Ending state if a player disconnects before the server resets
- Fixed bug where the server crashes if a reconnecting player disconnects before being rehabilitated
- Fixed that when the buzzering player disconnects and a substitute for assigning points is chosen, that substitute will not themselves be substituted if they also disconnect
- Fixed bug where InitDownloading is never left if the number of songs is lower than the lookahead
- Fixed bug where reconnecting of a client who already finished downloading glitches the progress bar
- Fixed memory leak where server-side netcode keeps a list of all connections even after closing

# Beta 1.1.4 (Protocol Version 2) --- 2023-08-07
Bug fix for newer yt_dlp versions
## Bug Fixes
- Disabled backwards progress handling in downloader as new yt_dlp causes it for no reason, effectively breaking the game

# Beta 1.1.3 (Protocol Version 2) --- 2023-07-23
Bug fix for newer yt_dlp versions
## Bug Fixes
- Fixed division by zero error when new yt_dlp progress dict contains 0 as estimated file size

# Beta 1.1.2 (Protocol Version 2) --- 2023-04-06
Migration from youtube_dl to yt_dlp
## Bug Fixes
- Downloading from youtube works again by switching to yt_dlp, youtube_dl has fixed issue but never pushed a new release...

# Beta 1.1.1 (Protocol Version 2) --- 2022-04-06
Rework of README.md and hotfixes
## Additions
- Proper readme
## Changes
- Skipped ticks are not logged any longer by default unless a certain threshold for the tick length is reached
- A more specific error is given if the config files are missing
## Bug Fixes
- Selftest does not let you continue to the launcher anymore if closed before finishing
- Download stats do now reset when starting a new game without restarting the client
- Lobby music now is changed each time when starting a new game even if the client is not restarted

# Beta 1.1.0 (Protocol Version 2) --- 2022-04-02
Major code-refactoring and multiple quality of life updates
## Additions
- Proper code documentation
- Tickrate limits for client and server to decrease CPU utilization, see config
- GUI can now show FPS, has to be enabled in config
- GUI can now show statistics regarding download, have to be enabled in config
- Client is now protected against downloads from unintended websites
- Added support for custom GUI themes
- Game window size and volume are now kept across multiple game sessions
- Proper client-side handling of failing connection to server
- When running the client using the launcher, crashes lead to the corresponding exception being shown in a designated GUI
- SQ now checks if file and folder dependencies exist on startup
- When SQ runs for the first time or with a new version, it runs a selftest and updates existing files
- Added music playing in the lobby and during initial downloading
- Added "volume translation" configuration allowing to set the volume slider to give more distinction in lower
    and less distinction in higher volumes
- When using windows, you can now run `windows_make_gui_only.bat` to get a `client.pyw` that does not open a terminal besides of
    the game when running. Remember that you need to run `windows_make_gui_only.bat` on every update to also update `client.pyw`.
    Note that using the window less version may open terminal windows for ffmpeg sometimes, so it is not really recommended yet.
## Changes
- Reworked config files (old config files likely require to be updated, use the reference files inside folder `config` as an example)
- Logging can also write into files now and got some improvements
- Removed Herobrine
## Bug Fixes
- Fixed problem that could arise from case-insensitive systems like Windows.
- Fixed missing ping compensation when starting to play songs
- Fixed bug in netcode that drastically increased server CPU utilization
- Potentially fixed rare bug where downloading a single song can cycle infinitely

# Beta 1.0 (Protocol Version 2) --- 2022-01-01
## Additions
- Client and server check protocol compatibility in handshake (#4)
- Download progress is displayed throughout the game (#14)
- Added proper disconnect handling
- Added support for reconnecting clients to a running game (#8)
- Added option to cache downloaded songs for future games (#22)
- Added option to proactively download songs for future games
- Add-to-playlist function to append current song to a textfile (#24)
- Added new feature for "BuzzerSlack" (#27)
## Changes
- GUI now is resizable (#17)
- Progress bars are now segmented into different steps and can display multiple progresses
- Some minor further GUI changes
- Config to disable volume normalization (#19)
- The server now re-enters waiting lobby state after the end of a game (#35)
- Failed downloads are now also written to a file (#23)
## Bug Fixes
- Fixed timeouts after long phases of no communication (#12)
- Fixed crashes from audio normalization when using Linux (#18)
