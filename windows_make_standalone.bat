if "%~1"=="" goto missingarg
if exist sq_builds\%1\ (
    echo "Output folder already existing"
    exit
)
pyinstaller client.py --noconfirm || goto error
mkdir sq_builds\%1\sq || goto error
move dist\client sq_builds\%1\sq || goto error
ren sq_builds\%1\sq\client dist || goto error
copy sq_builds\client.bat sq_builds\%1\sq || goto error
mkdir sq_builds\%1\sq\dist\bin || goto error
copy sq_builds\ffmpeg.exe sq_builds\%1\sq\dist\bin || goto error
copy sq_builds\client.ini sq_builds\%1\sq || goto error
mkdir sq_builds\%1\sq\dist\config || goto error
copy config\client_default.ini sq_builds\%1\sq\dist\config || goto error
copy config\theme_default.ini sq_builds\%1\sq\dist\config || goto error
mkdir sq_builds\%1\sq\dist\logs || goto error
mkdir sq_builds\%1\sq\dist\temp || goto error
mkdir sq_builds\%1\sq\song_database || goto error

goto end

:missingarg
echo "Missing argument: Output folder name"
exit

:error
echo "Build failed, check outputs. Maybe an input file was missing?"
exit

:end
echo "Build succeeded!"
