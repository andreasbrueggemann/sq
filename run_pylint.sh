#!/bin/sh
if [ "$1" = "all" ]
then
    pylint $(git ls-files '*.py') 
elif [ "$1" = "" ]
then
    pylint $(git ls-files '*.py') --disable=fixme,duplicate-code
else
    echo Illegal argument, allowed ones are [empty] or all
fi
